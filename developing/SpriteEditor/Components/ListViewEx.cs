﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace SpriteEditor.Components
{
	public class ListViewEx : ListView
	{
		public event EventHandler SubItemChanged;

		private ListViewItem.ListViewSubItem subItem;
		private TextBox itemTextBox;

		private void ItemTextBox_Leave(object sender, EventArgs e)
		{
			subItem.Text = this.itemTextBox.Text;
			this.itemTextBox.Leave -= new EventHandler(ItemTextBox_Leave);
			this.itemTextBox.Visible = false;

			this.OnSubItemChanged(sender, e);
		}

		private void ItemTextBox_KeyPress(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter) {
				subItem.Text = this.itemTextBox.Text;
			}

			if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Escape) {
				this.itemTextBox.KeyUp -= new KeyEventHandler(ItemTextBox_Leave);
				this.itemTextBox.Visible = false;

				this.OnSubItemChanged(sender, EventArgs.Empty);
			}
		}

		public ListViewEx()
			: base()
		{
			base.View = View.Details;
			base.FullRowSelect = true;
			base.HideSelection = false;

			//
			// TextBox
			//
			this.itemTextBox = new TextBox();
			this.itemTextBox.Name = "itemTextBox";
			this.itemTextBox.Visible = false;
			this.itemTextBox.Leave += new EventHandler(ItemTextBox_Leave);

			base.Controls.Add(this.itemTextBox);
		}

		protected virtual void OnSubItemChanged(object sender, EventArgs e)
		{
			if (this.SubItemChanged != null) {
				this.SubItemChanged(sender, e);
			}
		}

		#region Overrides
		protected override void OnDoubleClick(EventArgs e)
		{
			base.OnDoubleClick(e);

			Point p = base.PointToClient(Cursor.Position);
			int x = p.X;
			int y = p.Y;

			ListViewItem list = base.GetItemAt(x, y);
			this.subItem = list.GetSubItemAt(x, y);

			Rectangle rect = subItem.Bounds;

			this.itemTextBox.Text = subItem.Text;
			this.itemTextBox.Bounds = rect;
			this.itemTextBox.Visible = true;
			this.itemTextBox.BringToFront();
			this.itemTextBox.Leave += new EventHandler(this.ItemTextBox_Leave);
			this.itemTextBox.KeyUp += new KeyEventHandler(this.ItemTextBox_KeyPress);
			this.itemTextBox.Focus();
		}
		#endregion
	}
}
