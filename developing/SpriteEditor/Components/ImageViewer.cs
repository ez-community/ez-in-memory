﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Windows.Forms;
using System.Diagnostics;

using SpriteEditor.Core;

namespace SpriteEditor.Components
{
	public partial class ImageViewer : UserControl
	{
		public const int RESIZE_RECT_RANGE = 4;

		// Create new events on: create new rectangle & move rectangle
		public event EventHandler DrawedRect, MovedRect, ResizedRect;
		// Create new event on: Image zoom in/zoom out
		public event EventHandler ZoomInOut;

		private enum ImageViewerActions : int
		{
			SelectRect, Pan, DrawRect, MoveRect, PlayAnimation,
			ResizeRectTop, ResizeRectLeft, ResizeRectRight, ResizeRectBottom
		};

		private enum MouseEvents
		{
			MouseDown,  MouseMove
		};

		private SpriteFile sprite;
		private ImageViewerMode mode;
		private ImageViewerActions action;

		private Image spriteImage;
		private Image viewSpriteImage;
		private Image[] itemImages;
		private Image[] viewItemImages;

		private int selectedIndex;
		private int subItemSelectedIndex;
		private int zoom;							// 100% is 100
		private int scale;							// 100 % is 1 (percent / 100)

		private int baseX, baseY;					// 0, 0 for frames - left/top image
		private int mouseX, mouseY;					// Real mouse position on original image
		private Rectangle selectedRect;				// Real rectangle (not scaled)
		private Color selectedRectColor;

		public ImageViewer()
		{
			InitializeComponent();

			// Add MouseWheel event since cannot see in Events list in VS IDE
			this.MouseWheel += new System.Windows.Forms.MouseEventHandler(this.ImageViewer_MouseWheel);

			this.action = ImageViewerActions.SelectRect;
			this.scale = 1;   // Default is 100%
			this.selectedRectColor = Color.White;
		}

		#region Events
		/// <summary>
		/// Raise when user drawed a rectangle on image.
		/// </summary>
		protected virtual void OnDrawedRect(EventArgs e)
		{
			if (this.DrawedRect != null) {
				this.DrawedRect(this, e);
			}
		}

		/// <summary>
		/// Raise when user moved a rectangle on image.
		/// </summary>
		protected virtual void OnMovedRect(EventArgs e)
		{
			if (this.MovedRect != null) {
				this.MovedRect(this, e);
			}
		}

		protected virtual void OnResizedRect(EventArgs e)
		{
			if (this.ResizedRect != null) {
				this.ResizedRect(this, e);
			}
		}

		protected virtual void OnZoomInOut(EventArgs e)
		{
			if (this.ZoomInOut != null) {
				this.ZoomInOut(this, e);
			}
		}
		#endregion

		#region Private
		private void SetMouseXY(int x, int y)
		{
			this.mouseX = (x - this.baseX) / this.scale;
			this.mouseY = (y - this.baseY) / this.scale;
		}

		private Rectangle GetScaledRectangle()
		{
			int x = (this.selectedRect.X * this.scale) + this.baseX;
			int y = (this.selectedRect.Y * this.scale) + this.baseY;
			int w = this.selectedRect.Width * this.scale;
			int h = this.selectedRect.Height * this.scale;

			Rectangle rect = new Rectangle(x, y, w, h);
			return rect;
		}

		private void UpdateAction(MouseEventArgs e)
		{
			//Debug.WriteLine("action before: " + this.action);
			switch (this.action) {
				case ImageViewerActions.SelectRect:
					if (e.Button == MouseButtons.Right) {
						this.SetAction(ImageViewerActions.Pan);
					}
					break;

				case ImageViewerActions.Pan:
					if (e.Button == MouseButtons.Right) {
						this.SetAction(ImageViewerActions.DrawRect);
					}
					break;

				case ImageViewerActions.DrawRect:
					if (e.Button == MouseButtons.None) {
						this.SetMouseXY(e.X, e.Y);
						if (this.selectedRect.Contains(new Point(this.mouseX, this.mouseY))) {
							this.SetAction(ImageViewerActions.MoveRect);
						}
					} else if (e.Button == MouseButtons.Right) {
						this.SetAction(ImageViewerActions.Pan);
					}
					break;

				case ImageViewerActions.MoveRect:
					if (e.Button == MouseButtons.None) {
						this.SetMouseXY(e.X, e.Y);
						if (this.selectedRect.Contains(new Point(this.mouseX, this.mouseY))) {
							if (Math.Abs(this.mouseX - this.selectedRect.Left) < RESIZE_RECT_RANGE) {
								this.SetAction(ImageViewerActions.ResizeRectLeft);
							} else if (Math.Abs(this.mouseY - this.selectedRect.Top) <= RESIZE_RECT_RANGE) {
								this.SetAction(ImageViewerActions.ResizeRectTop);
							} else if (Math.Abs(this.mouseX - this.selectedRect.Right) < RESIZE_RECT_RANGE) {
								this.SetAction(ImageViewerActions.ResizeRectRight);
							} else if (Math.Abs(this.mouseY - this.selectedRect.Bottom) < RESIZE_RECT_RANGE) {
								this.SetAction(ImageViewerActions.ResizeRectBottom);
							}
						} else {
							this.SetAction(ImageViewerActions.Pan);
						}
					} else if (e.Button == MouseButtons.Right) {
						this.SetAction(ImageViewerActions.Pan);
					}
					break;

				case ImageViewerActions.ResizeRectLeft:
				case ImageViewerActions.ResizeRectTop:
				case ImageViewerActions.ResizeRectRight:
				case ImageViewerActions.ResizeRectBottom:
					if (e.Button == MouseButtons.None) {
						this.SetMouseXY(e.X, e.Y);
						int x1 = this.selectedRect.X;
						int y1 = this.selectedRect.Y;
						int x2 = x1 + this.selectedRect.Width;
						int y2 = y1 + this.selectedRect.Height;

						if (this.mouseX > x1 || this.mouseY > y1 || this.mouseX > x2 || this.mouseY > y2) {
							this.SetAction(ImageViewerActions.MoveRect);
						}
					} else if (e.Button == MouseButtons.Right) {
						this.SetAction(ImageViewerActions.Pan);
					}
					break;
			}
			//Debug.WriteLine("action after: " + this.action);
		}

		private void UpdateCursor()
		{
			if (this.action == ImageViewerActions.SelectRect) {
				this.Cursor = Cursors.Default;
			} else if (this.action == ImageViewerActions.Pan) {
				this.Cursor = Cursors.Hand;
			} else if (this.action == ImageViewerActions.DrawRect) {
				this.Cursor = Cursors.Cross;
			} else if (this.action == ImageViewerActions.MoveRect) {
				this.Cursor = Cursors.SizeAll;
			} else if (this.action == ImageViewerActions.ResizeRectLeft || this.action == ImageViewerActions.ResizeRectRight) {
				this.Cursor = Cursors.SizeWE;
			} else if (this.action == ImageViewerActions.ResizeRectTop || this.action == ImageViewerActions.ResizeRectBottom) {
				this.Cursor = Cursors.SizeNS;
			}
		}

		private void SetAction(ImageViewerActions action)
		{
			this.action = action;
			this.UpdateCursor();
			base.Invalidate();
		}

		private void UpdateItemRect()
		{
			if (this.sprite != null && this.selectedIndex > -1) {
				if (this.mode == ImageViewerMode.Module) {
					Module module = (Module)this.sprite.Modules[this.selectedIndex];
					module.SetRectangle(this.selectedRect);
				} else if (this.mode == ImageViewerMode.FModule) {
					FModule fmodule = (FModule)((Frame)this.sprite.Frames[this.selectedIndex]).FModules[this.subItemSelectedIndex];
					fmodule.X = this.selectedRect.X;
					fmodule.Y = this.selectedRect.Y;
				} else if (this.mode == ImageViewerMode.AFrame) {
					AFrame aframe = (AFrame)((Animation)this.sprite.Animations[this.selectedIndex]).AFrames[this.subItemSelectedIndex];
					aframe.X = this.selectedRect.X;
					aframe.Y = this.selectedRect.Y;
				}
			}
		}

		private void DrawCoordinateAxes(Graphics g)
		{
			Pen pen = new Pen(Color.Black);
			pen.Width = this.scale;

			Point p1 = new Point(this.baseX, base.Top);
			Point p2 = new Point(this.baseX, base.Top + base.Height);
			g.DrawLine(pen, p1, p2);

			Point p3 = new Point(0, this.baseY);
			Point p4 = new Point(base.Width, this.baseY);
			g.DrawLine(pen, p3, p4);
		}

		private void DrawRectangle(Rectangle rect, Graphics g)
		{
			if (this.selectedIndex > -1) {

				Pen pen = new Pen(this.selectedRectColor);
				pen.DashStyle = DashStyle.Dash;
				g.DrawRectangle(pen, rect);
			}
		}

		private void DrawImage(Graphics g, Image image, int x, int y)
		{
			if (image != null) {
				g.DrawImageUnscaled(image, this.baseX + (x * this.scale), this.baseY + (y * this.scale));
			}
		}
		#endregion

		#region Handle Events
		private void ImageViewer_SizeChanged(object sender, EventArgs e)
		{
			//if (this.viewImage != null) {
			//    this.baseX = (this.Width - this.viewImage.Width) >> 1;
			//    this.baseY = (this.Height - this.viewImage.Height) >> 1;
			//}
		}

		private void ImageViewer_MouseDown(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left) {
				this.SetMouseXY(e.X, e.Y);
				if (this.action == ImageViewerActions.DrawRect) {
					this.selectedRect = new Rectangle(this.mouseX, this.mouseY, 0, 0);
				}
			}
			this.UpdateAction(e);
		}

		private void ImageViewer_MouseMove(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left) {
				int lastMouseX = this.mouseX;
				int lastMouseY = this.mouseY;
				this.SetMouseXY(e.X, e.Y);

				int dx = (this.mouseX - lastMouseX);
				int dy = (this.mouseY - lastMouseY);

				if (this.action == ImageViewerActions.Pan) {
					this.baseX += dx * this.scale;	// dx
					this.baseY += dy * this.scale;	// dy
                    this.SetMouseXY(e.X, e.Y);
				} else if (this.action == ImageViewerActions.DrawRect) {
					this.selectedRect.Width += dx;
					this.selectedRect.Height += dy;
				} else if (this.action == ImageViewerActions.MoveRect) {
					this.selectedRect.X += dx;
					this.selectedRect.Y += dy;
				} else if (this.action == ImageViewerActions.ResizeRectLeft) {
					this.selectedRect.X += dx;
					this.selectedRect.Width += (-dx);
				} else if (this.action == ImageViewerActions.ResizeRectTop) {
					this.selectedRect.Y += dy;
					this.selectedRect.Height += (-dy);
				} else if (this.action == ImageViewerActions.ResizeRectRight) {
					this.selectedRect.Width += dx;
				} else if (this.action == ImageViewerActions.ResizeRectBottom) {
					this.selectedRect.Height += dy;
				}

				if (this.mode == ImageViewerMode.Animation) {
					this.StopAnimation();
				}

				base.Invalidate();
			}

			this.UpdateAction(e);
		}

		private void ImageViewer_MouseUp(object sender, MouseEventArgs e)
		{
			// Invoke OnMovedRect/OnDrawedRect when finish
			if (e.Button == MouseButtons.Left) {
				UpdateItemRect();
				if (this.action == ImageViewerActions.MoveRect) {
					this.OnMovedRect(e);
				} else if (this.action == ImageViewerActions.DrawRect) {
					this.OnDrawedRect(e);
				} else if (this.action > ImageViewerActions.MoveRect) {
					this.OnResizedRect(e);
				}

				if (this.mode == ImageViewerMode.Animation) {
					this.PlayAnimation();
				}

				base.Invalidate();
			}
		}

		private void ImageViewer_MouseWheel(object sender, MouseEventArgs e)
		{
			if (e.Delta > 0) {
				this.Zoom += 100;
			} else {
				if (this.Zoom > 100) {
					this.Zoom -= 100;
				}
			}
		}

		private void timer_Tick(object sender, EventArgs e)
		{
			this.subItemSelectedIndex++;
			if (this.subItemSelectedIndex > this.viewItemImages.Length - 1) {
			   this.subItemSelectedIndex = 0;
			}

			base.Invalidate();
		}
		#endregion

		#region Properties
		public SpriteFile Sprite
		{
			set
			{
				this.sprite = value;
				if (this.sprite != null && this.mode == ImageViewerMode.Module) {
					this.spriteImage = ImageLib.GetImage(this.sprite.ImageFileName);
					this.viewSpriteImage = ImageLib.Rescale(this.spriteImage, this.scale);

					if (this.scale == 1 && this.spriteImage != null) {
						this.baseX = (base.Width - this.spriteImage.Width) >> 1;
						this.baseY = (base.Height - this.spriteImage.Height) >> 1;
					}

					base.Invalidate();
				}
			}
		}

		public ImageViewerMode Mode
		{
			get
			{
				return this.mode;
			}

			set
			{
				this.mode = value;
				this.StopAnimation();

				if (this.mode == ImageViewerMode.Module) {
					if (this.spriteImage != null && this.spriteImage.PixelFormat != PixelFormat.Format32bppArgb) {
						this.selectedRectColor = Color.White;
					} else {
						this.selectedRectColor = Color.Magenta;
					}
				} else {
					this.selectedRectColor = Color.Magenta;
				}

				if (this.mode != ImageViewerMode.FModule && this.mode != ImageViewerMode.AFrame) {
					this.selectedIndex = -1;
					this.subItemSelectedIndex = -1;
					this.selectedRect = new Rectangle();
				}
			}
		}

		public int SelectedIndex
		{
			set
			{
				if (value > -1) {
					this.selectedIndex = value;

					if (this.mode == ImageViewerMode.Module) {
						Module module = (Module)this.sprite.Modules[this.selectedIndex];
						this.selectedRect = module.GetRectangle();
					} else if (this.mode == ImageViewerMode.Frame) {
						Frame frame = (Frame)this.sprite.Frames[this.selectedIndex];
						int length = frame.FModules.Count;
						this.itemImages = new Image[length];
						this.viewItemImages = new Image[length];

						for (int i = 0; i < length; i++) {
							FModule fmodule = (FModule)frame.FModules[i];
							Image image = this.sprite.GetFModuleImage(fmodule);
							this.itemImages[i] = image;
							this.viewItemImages[i] = ImageLib.Rescale(image, this.scale);
						}
					} else if (this.mode == ImageViewerMode.Animation) {
						Animation animation = (Animation)this.sprite.Animations[this.selectedIndex];
						ListAFrame aframes = (ListAFrame)animation.AFrames;
						int length = aframes.Count;
						this.itemImages = new Image[length];
						this.viewItemImages = new Image[length];

						for (int i = 0; i < length; i++) {
							AFrame aframe = (AFrame)aframes[i];
							Image image = this.sprite.GetAFrameImage(aframe);
							this.itemImages[i] = image;
							this.viewItemImages[i] = ImageLib.Rescale(image, this.scale);
						}

						this.PlayAnimation();
					}

					base.Invalidate();
				}
			}
		}

		public int SubItemSelectedIndex
		{
			set
			{
				if (value > -1 && this.selectedIndex > -1) {
					this.subItemSelectedIndex = value;

					if (this.mode == ImageViewerMode.FModule) {
						FModule fmodule = (FModule)((Frame)this.sprite.Frames[this.selectedIndex]).FModules[this.subItemSelectedIndex];
						this.selectedRect = fmodule.GetRectangle();
					} else if (this.mode == ImageViewerMode.AFrame) {
						AFrame aframe = (AFrame)((Animation)this.sprite.Animations[this.selectedIndex]).AFrames[this.subItemSelectedIndex];
						this.selectedRect = aframe.GetRectangle();
					}

					base.Invalidate();
				}
			}
		}

		public Palette Palette
		{
			set
			{
				if (this.spriteImage != null) {
					ColorPalette palette = this.spriteImage.Palette;
					for (int i = 0; i < palette.Entries.Length && i < value.Colors.Length; i++) {
						palette.Entries[i] = value[i];
					}

					this.spriteImage.Palette = palette;
					this.viewSpriteImage = ImageLib.Rescale(this.spriteImage, this.scale);
					base.Invalidate();
				}
			}
		}

		public int Zoom
		{
			set
			{
				if (this.zoom != value) {
					if (value < 100)
						value = 100;

					this.zoom = value;							// Zoom value are: 100, 200, 300...
					this.scale = value / 100;					// Scale value are: 1, 2, 3

					if (this.mode == ImageViewerMode.Module) {
						this.viewSpriteImage = ImageLib.Rescale(this.spriteImage, this.scale);
					} else if (this.itemImages != null) {
						for (int i = 0; i < this.itemImages.Length; i++) {
							Image image = ImageLib.Rescale(this.itemImages[i], this.scale);
							this.viewItemImages[i] = image;
						}
					}

					this.OnZoomInOut(EventArgs.Empty);			// Invoke ZoomIn/ZoomOut event
					this.Invalidate();
				}
			}

			get
			{
				return this.zoom;
			}
		}

		public int GetMouseX()
		{
			return this.mouseX;
		}

		public int GetMouseY()
		{
			return this.mouseY;
		}

//		public Color SelectedRectColor
//		{
//			set
//			{
//				this.selectedRectColor = value;
//			}
//		}
		#endregion

		#region Override
		protected override void OnPaint(PaintEventArgs e)
		{
			base.OnPaint(e);
			Graphics g = e.Graphics;

			//
			// Draw images
			//
			if (this.mode == ImageViewerMode.Module) {
				this.DrawImage(g, this.viewSpriteImage, 0, 0);
			} else if (this.viewItemImages != null && this.viewItemImages.Length > 0 && this.selectedIndex > -1) {
				if (this.mode == ImageViewerMode.Frame || this.mode == ImageViewerMode.FModule) {
					ListFModule fmodules = (ListFModule)((Frame)this.sprite.Frames[this.selectedIndex]).FModules;
					for (int i = 0; i < this.viewItemImages.Length; i++) {
						if (this.viewItemImages[i] != null) {
							FModule fmodule = (FModule)fmodules[i];
							this.DrawImage(g, this.viewItemImages[i], fmodule.X, fmodule.Y);
						}
					}
				} else if (this.mode == ImageViewerMode.Animation || this.mode == ImageViewerMode.AFrame) {
					ListAFrame aframes = (ListAFrame)((Animation)this.sprite.Animations[this.selectedIndex]).AFrames;

					if (aframes.Count > 0) {
						AFrame aframe = (AFrame)aframes[this.subItemSelectedIndex];
						this.DrawImage(g, this.viewItemImages[this.subItemSelectedIndex], aframe.X, aframe.Y);
					}
				}
			}

			//
			// Draw Coordinate axes
			//
			if (this.sprite != null) {
				this.DrawCoordinateAxes(g);
			}


			//
			// Draw selected rectangle
			//
			if (this.selectedRect != null && this.mode != ImageViewerMode.Animation && this.mode != ImageViewerMode.Frame) {
				Rectangle rect = this.GetScaledRectangle();
				this.DrawRectangle(rect, g);
			}
		}
		#endregion

		#region Public
		public void PlayAnimation()
		{
			const int TIMER_INTERVAL = 500;
			this.timer.Interval = TIMER_INTERVAL;
			this.timer.Enabled = true;

			this.action = ImageViewerActions.PlayAnimation;
			this.subItemSelectedIndex = 0;
		}

		public void StopAnimation()
		{
			this.timer.Stop();
			this.timer.Enabled = false;
			this.action = ImageViewerActions.SelectRect;
		}
		#endregion

		// TODO Nedd optimize
		private void ImageViewer_KeyDown(object sender, KeyEventArgs e)
		{
			if (((e.KeyCode == Keys.NumPad8 || e.KeyCode == Keys.NumPad2 ||
			      e.KeyCode == Keys.NumPad4 || e.KeyCode == Keys.NumPad6) &&
			      (this.mode != ImageViewerMode.Frame && this.mode != ImageViewerMode.Animation)) ||
			     (e.Control && e.KeyCode == Keys.B && this.mode == ImageViewerMode.Module)) {

			    if (e.Control && e.KeyCode == Keys.B && this.mode == ImageViewerMode.Module) {
					this.selectedRect = ImageLib.BestFix(this.selectedRect, (Bitmap)this.spriteImage);
					System.Diagnostics.Debug.WriteLine("This module is best fixed...");
				} else if (e.KeyCode == Keys.NumPad8) {
					this.selectedRect.Y--;
				} else if (e.KeyCode == Keys.NumPad2) {
					this.selectedRect.Y++;
				} else if (e.KeyCode == Keys.NumPad4) {
					this.selectedRect.X--;
				} else if (e.KeyCode == Keys.NumPad6) {
					this.selectedRect.X++;
				}

				this.UpdateItemRect();
				this.OnResizedRect(e);
				base.Invalidate();
			}
		}
	}
}
