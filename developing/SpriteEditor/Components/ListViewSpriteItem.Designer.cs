﻿namespace SpriteEditor.Components {
	partial class ListViewSpriteItem {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.bottomButton = new System.Windows.Forms.Button();
            this.topButton = new System.Windows.Forms.Button();
            this.downButton = new System.Windows.Forms.Button();
            this.upButton = new System.Windows.Forms.Button();
            this.cloneButton = new System.Windows.Forms.Button();
            this.deleteButton = new System.Windows.Forms.Button();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.insertButton = new System.Windows.Forms.Button();
            this.addButton = new System.Windows.Forms.Button();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.listViewEx = new SpriteEditor.Components.ListViewEx();
            this.SuspendLayout();
            // 
            // bottomButton
            // 
            this.bottomButton.Image = global::SpriteEditor.Properties.Resources.bottom;
            this.bottomButton.Location = new System.Drawing.Point(265, 3);
            this.bottomButton.Name = "bottomButton";
            this.bottomButton.Size = new System.Drawing.Size(32, 32);
            this.bottomButton.TabIndex = 17;
            this.toolTip.SetToolTip(this.bottomButton, "Move on bottom");
            this.bottomButton.UseVisualStyleBackColor = true;
            this.bottomButton.Click += new System.EventHandler(this.bottomButton_Click);
            // 
            // topButton
            // 
            this.topButton.Image = global::SpriteEditor.Properties.Resources.top;
            this.topButton.Location = new System.Drawing.Point(231, 3);
            this.topButton.Name = "topButton";
            this.topButton.Size = new System.Drawing.Size(32, 32);
            this.topButton.TabIndex = 16;
            this.toolTip.SetToolTip(this.topButton, "Move on top");
            this.topButton.UseVisualStyleBackColor = true;
            this.topButton.Click += new System.EventHandler(this.topButton_Click);
            // 
            // downButton
            // 
            this.downButton.Image = global::SpriteEditor.Properties.Resources.down;
            this.downButton.Location = new System.Drawing.Point(193, 3);
            this.downButton.Name = "downButton";
            this.downButton.Size = new System.Drawing.Size(32, 32);
            this.downButton.TabIndex = 15;
            this.toolTip.SetToolTip(this.downButton, "Move down");
            this.downButton.UseVisualStyleBackColor = true;
            this.downButton.Click += new System.EventHandler(this.downButton_Click);
            // 
            // upButton
            // 
            this.upButton.Image = global::SpriteEditor.Properties.Resources.up;
            this.upButton.Location = new System.Drawing.Point(155, 3);
            this.upButton.Name = "upButton";
            this.upButton.Size = new System.Drawing.Size(32, 32);
            this.upButton.TabIndex = 14;
            this.toolTip.SetToolTip(this.upButton, "Move up");
            this.upButton.UseVisualStyleBackColor = true;
            this.upButton.Click += new System.EventHandler(this.upButton_Click);
            // 
            // cloneButton
            // 
            this.cloneButton.Image = global::SpriteEditor.Properties.Resources.clone;
            this.cloneButton.Location = new System.Drawing.Point(117, 3);
            this.cloneButton.Name = "cloneButton";
            this.cloneButton.Size = new System.Drawing.Size(32, 32);
            this.cloneButton.TabIndex = 13;
            this.toolTip.SetToolTip(this.cloneButton, "Clone");
            this.cloneButton.UseVisualStyleBackColor = true;
            this.cloneButton.Click += new System.EventHandler(this.cloneButton_Click);
            // 
            // deleteButton
            // 
            this.deleteButton.Image = global::SpriteEditor.Properties.Resources.delete;
            this.deleteButton.Location = new System.Drawing.Point(79, 3);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(32, 32);
            this.deleteButton.TabIndex = 12;
            this.toolTip.SetToolTip(this.deleteButton, "Delete");
            this.deleteButton.UseVisualStyleBackColor = true;
            this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
            // 
            // insertButton
            // 
            this.insertButton.Image = global::SpriteEditor.Properties.Resources.insert;
            this.insertButton.Location = new System.Drawing.Point(41, 3);
            this.insertButton.Name = "insertButton";
            this.insertButton.Size = new System.Drawing.Size(32, 32);
            this.insertButton.TabIndex = 11;
            this.toolTip.SetToolTip(this.insertButton, "Insert");
            this.insertButton.UseVisualStyleBackColor = true;
            this.insertButton.Click += new System.EventHandler(this.insertButton_Click);
            // 
            // addButton
            // 
            this.addButton.Image = global::SpriteEditor.Properties.Resources.add;
            this.addButton.Location = new System.Drawing.Point(3, 3);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(32, 32);
            this.addButton.TabIndex = 10;
            this.toolTip.SetToolTip(this.addButton, "Add");
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog";
            this.openFileDialog.Filter = "Palette file(*.act)|*.act";
            // 
            // listViewEx
            // 
            this.listViewEx.AutoArrange = false;
            this.listViewEx.FullRowSelect = true;
            this.listViewEx.GridLines = true;
            this.listViewEx.HideSelection = false;
            this.listViewEx.Location = new System.Drawing.Point(4, 42);
			this.listViewEx.MultiSelect = false;
            this.listViewEx.Name = "listViewEx";
            this.listViewEx.Size = new System.Drawing.Size(293, 238);
            this.listViewEx.TabIndex = 9;
            this.listViewEx.UseCompatibleStateImageBehavior = false;
            this.listViewEx.View = System.Windows.Forms.View.Details;
            this.listViewEx.SubItemChanged += new System.EventHandler(this.listViewEx_SubItemChanged);
            this.listViewEx.SelectedIndexChanged += new System.EventHandler(this.listViewEx_SelectedIndexChanged);
            this.listViewEx.KeyDown += new System.Windows.Forms.KeyEventHandler(this.listViewEx_KeyDown);
            // 
            // ListViewSpriteItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.bottomButton);
            this.Controls.Add(this.topButton);
            this.Controls.Add(this.downButton);
            this.Controls.Add(this.upButton);
            this.Controls.Add(this.cloneButton);
            this.Controls.Add(this.deleteButton);
            this.Controls.Add(this.insertButton);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.listViewEx);
            this.Name = "ListViewSpriteItem";
            this.Size = new System.Drawing.Size(327, 320);
            this.Resize += new System.EventHandler(this.ListViewSpriteItem_Resize);
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button bottomButton;
		private System.Windows.Forms.ToolTip toolTip;
		private System.Windows.Forms.Button topButton;
		private System.Windows.Forms.Button downButton;
		private System.Windows.Forms.Button upButton;
		private System.Windows.Forms.Button cloneButton;
		private System.Windows.Forms.Button deleteButton;
		private System.Windows.Forms.Button insertButton;
		private System.Windows.Forms.Button addButton;
		private ListViewEx listViewEx;
        private System.Windows.Forms.OpenFileDialog openFileDialog;

	}
}
