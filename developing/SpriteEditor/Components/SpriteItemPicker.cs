﻿using System;
using System.Drawing;
using System.Windows.Forms;

using SpriteEditor.Core;

namespace SpriteEditor.Components
{
	public partial class SpriteItemPicker : UserControl
	{
		public const int PICTURE_BOX_LEFT		= 5;
		public const int PICTURE_BOX_TOP		= 5;
		public const int PICTURE_BOX_DISTANCE	= 5;
		public const int PICTURE_BOX_WIDTH		= 60;
		public const int PICTURE_BOX_HEIGHT		= 60;
		public Color PICTURE_BOX_COLOR			= Color.Transparent;
		public Color PICTURE_BOX_COLOR_SELECTED	= Color.Gray;
		public Color LABEL_COLOR				= Color.Black;
		public Color LABEL_COLOR_SELECTED		= Color.Blue;

		private PictureBox[] pictureBoxes;
		private Label[] labels;
		private ListSpriteItem items;
		private Image[] images;
		private int selectedIndex;

		public SpriteItemPicker()
		{
			InitializeComponent();
		}

		private void InitializePictureBoxes()
		{
			base.Controls.Clear();

			int length = this.items.Count;
			this.pictureBoxes = new PictureBox[length];
			this.labels = new Label[length];

			for (int i = 0; i < length; i++) {
				SpriteItem item = this.items[i];
				//
				// Picture box
				//
				this.pictureBoxes[i] = new PictureBox();
				if (i != 0)
					this.pictureBoxes[i].Left = this.pictureBoxes[i - 1].Right + PICTURE_BOX_DISTANCE;
				else
					this.pictureBoxes[i].Left = PICTURE_BOX_LEFT;

				this.pictureBoxes[i].Top = PICTURE_BOX_TOP;
				this.pictureBoxes[i].Width = PICTURE_BOX_WIDTH;
				this.pictureBoxes[i].Height = PICTURE_BOX_HEIGHT;
				this.pictureBoxes[i].BackColor = PICTURE_BOX_COLOR;
				this.pictureBoxes[i].BorderStyle = BorderStyle.FixedSingle;
				this.pictureBoxes[i].SizeMode = PictureBoxSizeMode.CenterImage;
				this.pictureBoxes[i].Image = this.images[i];

				this.pictureBoxes[i].Click += new System.EventHandler(this.OnPictureBoxClick);
				this.pictureBoxes[i].DoubleClick += new System.EventHandler(this.OnPictureBoxDoubleClick);
				base.Controls.Add(this.pictureBoxes[i]);

				//
				// Label
				//
				this.labels[i] = new Label();
				this.labels[i].Left = this.pictureBoxes[i].Left;
				this.labels[i].Top = PICTURE_BOX_TOP + PICTURE_BOX_HEIGHT;
				this.labels[i].Width = PICTURE_BOX_WIDTH;
				this.labels[i].TextAlign = ContentAlignment.MiddleCenter;
				this.labels[i].Text = i.ToString() + " (" + item.ID.ToString() + ")";
				if (this.pictureBoxes[i].Image == null) {
					this.labels[i].ForeColor = Color.Red;
				}

				base.Controls.Add(this.labels[i]);
			}
		}

		private void OnPictureBoxClick(object sender, EventArgs e)
		{
			for (int i = 0; i < this.pictureBoxes.Length; i++) {
				if (sender != this.pictureBoxes[i]) {
					this.pictureBoxes[i].BackColor = PICTURE_BOX_COLOR;
					this.labels[i].ForeColor = LABEL_COLOR;
				} else {
					this.selectedIndex = i;
					this.pictureBoxes[i].BackColor = PICTURE_BOX_COLOR_SELECTED;
					this.labels[i].ForeColor = LABEL_COLOR_SELECTED;
				}
			}
		}

		private void OnPictureBoxDoubleClick(object sender, EventArgs e)
		{
			this.OnPictureBoxClick(sender, e);
			base.OnDoubleClick(e);
		}

		#region Properties
		public ListSpriteItem Items
		{
			set
			{
				this.items = value;
			}
		}

		public Image[] Images
		{
			set
			{
				this.images = value;
				this.InitializePictureBoxes();
			}
		}

		public int SelectedIndex
		{
			get
			{
				return this.selectedIndex;
			}
		}

		public SpriteItem SelectedItem
		{
			get
			{
				return this.items[this.selectedIndex];
			}
		}
		#endregion
	}
}
