﻿namespace SpriteEditor.UI
{
	partial class SpriteEditorForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SpriteEditorForm));
            this.tabControl = new System.Windows.Forms.TabControl();
            this.modulesTabPage = new System.Windows.Forms.TabPage();
            this.paletteGroupBox = new System.Windows.Forms.GroupBox();
            this.paletteListView = new SpriteEditor.Components.ListViewSpriteItem();
            this.moduleGroupBox = new System.Windows.Forms.GroupBox();
            this.moduleListView = new SpriteEditor.Components.ListViewSpriteItem();
            this.spriteDetailsGroupBox = new System.Windows.Forms.GroupBox();
            this.changeSpriteImageLinkLabel = new System.Windows.Forms.LinkLabel();
            this.spriteImageLabel = new System.Windows.Forms.Label();
            this.spriteImageHeaderLabel = new System.Windows.Forms.Label();
            this.spriteNameLabel = new System.Windows.Forms.Label();
            this.spriteNameHeader = new System.Windows.Forms.Label();
            this.moduleImageViewer = new SpriteEditor.Components.ImageViewer();
            this.framesTabPage = new System.Windows.Forms.TabPage();
            this.modulePickerGroupBox = new System.Windows.Forms.GroupBox();
            this.modulePicker = new SpriteEditor.Components.SpriteItemPicker();
            this.fmoduleGroupBox = new System.Windows.Forms.GroupBox();
            this.fmoduleListView = new SpriteEditor.Components.ListViewSpriteItem();
            this.frameGroupBox = new System.Windows.Forms.GroupBox();
            this.frameListView = new SpriteEditor.Components.ListViewSpriteItem();
            this.frameImageViewer = new SpriteEditor.Components.ImageViewer();
            this.animationsTabPage = new System.Windows.Forms.TabPage();
            this.aframePickerGroupBox = new System.Windows.Forms.GroupBox();
            this.framePicker = new SpriteEditor.Components.SpriteItemPicker();
            this.aframeGroupBox = new System.Windows.Forms.GroupBox();
            this.aframeListView = new SpriteEditor.Components.ListViewSpriteItem();
            this.animationGroupBox = new System.Windows.Forms.GroupBox();
            this.animationListView = new SpriteEditor.Components.ListViewSpriteItem();
            this.animationImageViewer = new SpriteEditor.Components.ImageViewer();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.mousePositionStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.playAnimationTimer = new System.Windows.Forms.Timer(this.components);
            this.toolStrip = new SpriteEditor.UI.SpriteEditorToolStrip();
            this.tabControl.SuspendLayout();
            this.modulesTabPage.SuspendLayout();
            this.paletteGroupBox.SuspendLayout();
            this.moduleGroupBox.SuspendLayout();
            this.spriteDetailsGroupBox.SuspendLayout();
            this.framesTabPage.SuspendLayout();
            this.modulePickerGroupBox.SuspendLayout();
            this.fmoduleGroupBox.SuspendLayout();
            this.frameGroupBox.SuspendLayout();
            this.animationsTabPage.SuspendLayout();
            this.aframePickerGroupBox.SuspendLayout();
            this.aframeGroupBox.SuspendLayout();
            this.animationGroupBox.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl.Controls.Add(this.modulesTabPage);
            this.tabControl.Controls.Add(this.framesTabPage);
            this.tabControl.Controls.Add(this.animationsTabPage);
            this.tabControl.Location = new System.Drawing.Point(0, 34);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(1184, 653);
            this.tabControl.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControl.TabIndex = 0;
            this.tabControl.SelectedIndexChanged += new System.EventHandler(this.tabControl_SelectedIndexChanged);
            // 
            // modulesTabPage
            // 
            this.modulesTabPage.AutoScroll = true;
            this.modulesTabPage.AutoScrollMinSize = new System.Drawing.Size(1176, 627);
            this.modulesTabPage.Controls.Add(this.paletteGroupBox);
            this.modulesTabPage.Controls.Add(this.moduleGroupBox);
            this.modulesTabPage.Controls.Add(this.spriteDetailsGroupBox);
            this.modulesTabPage.Controls.Add(this.moduleImageViewer);
            this.modulesTabPage.Location = new System.Drawing.Point(4, 22);
            this.modulesTabPage.Name = "modulesTabPage";
            this.modulesTabPage.Size = new System.Drawing.Size(1176, 627);
            this.modulesTabPage.TabIndex = 2;
            this.modulesTabPage.Text = "Modules";
            this.modulesTabPage.UseVisualStyleBackColor = true;
            // 
            // paletteGroupBox
            // 
            this.paletteGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.paletteGroupBox.Controls.Add(this.paletteListView);
            this.paletteGroupBox.Location = new System.Drawing.Point(8, 387);
            this.paletteGroupBox.Name = "paletteGroupBox";
            this.paletteGroupBox.Size = new System.Drawing.Size(364, 167);
            this.paletteGroupBox.TabIndex = 12;
            this.paletteGroupBox.TabStop = false;
            this.paletteGroupBox.Text = "Palettes";
            // 
            // paletteListView
            // 
            this.paletteListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.paletteListView.Items = null;
            this.paletteListView.Location = new System.Drawing.Point(3, 16);
            this.paletteListView.Name = "paletteListView";
            this.paletteListView.Size = new System.Drawing.Size(358, 148);
            this.paletteListView.TabIndex = 11;
            this.paletteListView.SelectedIndexChanged += new System.EventHandler(this.paletteListView_SelectedIndexChanged);
            // 
            // moduleGroupBox
            // 
            this.moduleGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.moduleGroupBox.Controls.Add(this.moduleListView);
            this.moduleGroupBox.Location = new System.Drawing.Point(8, 8);
            this.moduleGroupBox.Name = "moduleGroupBox";
            this.moduleGroupBox.Size = new System.Drawing.Size(364, 373);
            this.moduleGroupBox.TabIndex = 10;
            this.moduleGroupBox.TabStop = false;
            this.moduleGroupBox.Text = "Modules";
            // 
            // moduleListView
            // 
            this.moduleListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.moduleListView.Items = null;
            this.moduleListView.Location = new System.Drawing.Point(3, 16);
            this.moduleListView.Name = "moduleListView";
            this.moduleListView.Size = new System.Drawing.Size(358, 354);
            this.moduleListView.TabIndex = 0;
            this.moduleListView.ItemsChanged += new System.EventHandler(this.listViewItemsChanged);
            this.moduleListView.SelectedIndexChanged += new System.EventHandler(this.moduleListView_SelectedIndexChanged);
            // 
            // spriteDetailsGroupBox
            // 
            this.spriteDetailsGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.spriteDetailsGroupBox.Controls.Add(this.changeSpriteImageLinkLabel);
            this.spriteDetailsGroupBox.Controls.Add(this.spriteImageLabel);
            this.spriteDetailsGroupBox.Controls.Add(this.spriteImageHeaderLabel);
            this.spriteDetailsGroupBox.Controls.Add(this.spriteNameLabel);
            this.spriteDetailsGroupBox.Controls.Add(this.spriteNameHeader);
            this.spriteDetailsGroupBox.Location = new System.Drawing.Point(8, 560);
            this.spriteDetailsGroupBox.Name = "spriteDetailsGroupBox";
            this.spriteDetailsGroupBox.Size = new System.Drawing.Size(364, 64);
            this.spriteDetailsGroupBox.TabIndex = 9;
            this.spriteDetailsGroupBox.TabStop = false;
            this.spriteDetailsGroupBox.Text = "Sprite details";
            // 
            // changeSpriteImageLinkLabel
            // 
            this.changeSpriteImageLinkLabel.AutoSize = true;
            this.changeSpriteImageLinkLabel.Location = new System.Drawing.Point(291, 36);
            this.changeSpriteImageLinkLabel.Name = "changeSpriteImageLinkLabel";
            this.changeSpriteImageLinkLabel.Size = new System.Drawing.Size(44, 13);
            this.changeSpriteImageLinkLabel.TabIndex = 10;
            this.changeSpriteImageLinkLabel.TabStop = true;
            this.changeSpriteImageLinkLabel.Text = "Change";
            this.changeSpriteImageLinkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.changeSpriteImageLinkLabel_LinkClicked);
            // 
            // spriteImageLabel
            // 
            this.spriteImageLabel.AutoSize = true;
            this.spriteImageLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spriteImageLabel.Location = new System.Drawing.Point(129, 35);
            this.spriteImageLabel.Name = "spriteImageLabel";
            this.spriteImageLabel.Size = new System.Drawing.Size(27, 13);
            this.spriteImageLabel.TabIndex = 3;
            this.spriteImageLabel.Text = "N/A";
            // 
            // spriteImageHeaderLabel
            // 
            this.spriteImageHeaderLabel.AutoSize = true;
            this.spriteImageHeaderLabel.Location = new System.Drawing.Point(16, 36);
            this.spriteImageHeaderLabel.Name = "spriteImageHeaderLabel";
            this.spriteImageHeaderLabel.Size = new System.Drawing.Size(65, 13);
            this.spriteImageHeaderLabel.TabIndex = 2;
            this.spriteImageHeaderLabel.Text = "Sprite image";
            // 
            // spriteNameLabel
            // 
            this.spriteNameLabel.AutoSize = true;
            this.spriteNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spriteNameLabel.Location = new System.Drawing.Point(129, 15);
            this.spriteNameLabel.Name = "spriteNameLabel";
            this.spriteNameLabel.Size = new System.Drawing.Size(27, 13);
            this.spriteNameLabel.TabIndex = 1;
            this.spriteNameLabel.Text = "N/A";
            // 
            // spriteNameHeader
            // 
            this.spriteNameHeader.AutoSize = true;
            this.spriteNameHeader.Location = new System.Drawing.Point(16, 16);
            this.spriteNameHeader.Name = "spriteNameHeader";
            this.spriteNameHeader.Size = new System.Drawing.Size(63, 13);
            this.spriteNameHeader.TabIndex = 0;
            this.spriteNameHeader.Text = "Sprite name";
            // 
            // moduleImageViewer
            // 
            this.moduleImageViewer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.moduleImageViewer.Cursor = System.Windows.Forms.Cursors.Default;
            this.moduleImageViewer.Location = new System.Drawing.Point(375, 3);
            this.moduleImageViewer.Mode = SpriteEditor.Core.ImageViewerMode.Module;
            this.moduleImageViewer.Name = "moduleImageViewer";
            this.moduleImageViewer.Size = new System.Drawing.Size(798, 621);
            this.moduleImageViewer.TabIndex = 8;
            this.moduleImageViewer.Zoom = 100;
            this.moduleImageViewer.DrawedRect += new System.EventHandler(this.imageViewer_DrawedRect);
            this.moduleImageViewer.MouseMove += new System.Windows.Forms.MouseEventHandler(this.moduleImageViewer_MouseMove);
            this.moduleImageViewer.MovedRect += new System.EventHandler(this.imageViewer_MovedRect);
            this.moduleImageViewer.ZoomInOut += new System.EventHandler(this.moduleImageViewer_ZoomInOut);
            this.moduleImageViewer.ResizedRect += new System.EventHandler(this.imageViewer_ResizedRect);
            // 
            // framesTabPage
            // 
            this.framesTabPage.AutoScroll = true;
            this.framesTabPage.AutoScrollMinSize = new System.Drawing.Size(1176, 627);
            this.framesTabPage.Controls.Add(this.modulePickerGroupBox);
            this.framesTabPage.Controls.Add(this.fmoduleGroupBox);
            this.framesTabPage.Controls.Add(this.frameGroupBox);
            this.framesTabPage.Controls.Add(this.frameImageViewer);
            this.framesTabPage.Location = new System.Drawing.Point(4, 22);
            this.framesTabPage.Name = "framesTabPage";
            this.framesTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.framesTabPage.Size = new System.Drawing.Size(1176, 627);
            this.framesTabPage.TabIndex = 0;
            this.framesTabPage.Text = "Frames";
            this.framesTabPage.UseVisualStyleBackColor = true;
            // 
            // modulePickerGroupBox
            // 
            this.modulePickerGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.modulePickerGroupBox.Controls.Add(this.modulePicker);
            this.modulePickerGroupBox.Location = new System.Drawing.Point(377, 458);
            this.modulePickerGroupBox.Name = "modulePickerGroupBox";
            this.modulePickerGroupBox.Size = new System.Drawing.Size(791, 167);
            this.modulePickerGroupBox.TabIndex = 11;
            this.modulePickerGroupBox.TabStop = false;
            this.modulePickerGroupBox.Text = "Modules";
            // 
            // modulePicker
            // 
            this.modulePicker.AutoScroll = true;
            this.modulePicker.Dock = System.Windows.Forms.DockStyle.Fill;
            this.modulePicker.Location = new System.Drawing.Point(3, 16);
            this.modulePicker.Name = "modulePicker";
            this.modulePicker.Size = new System.Drawing.Size(785, 148);
            this.modulePicker.TabIndex = 10;
            this.modulePicker.DoubleClick += new System.EventHandler(this.modulePicker_DoubleClick);
            // 
            // fmoduleGroupBox
            // 
            this.fmoduleGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.fmoduleGroupBox.Controls.Add(this.fmoduleListView);
            this.fmoduleGroupBox.Location = new System.Drawing.Point(8, 401);
            this.fmoduleGroupBox.Name = "fmoduleGroupBox";
            this.fmoduleGroupBox.Size = new System.Drawing.Size(363, 224);
            this.fmoduleGroupBox.TabIndex = 9;
            this.fmoduleGroupBox.TabStop = false;
            this.fmoduleGroupBox.Text = "Frame Modules";
            // 
            // fmoduleListView
            // 
            this.fmoduleListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fmoduleListView.Items = null;
            this.fmoduleListView.Location = new System.Drawing.Point(3, 16);
            this.fmoduleListView.Name = "fmoduleListView";
            this.fmoduleListView.Size = new System.Drawing.Size(357, 205);
            this.fmoduleListView.TabIndex = 10;
            this.fmoduleListView.SelectedIndexChanged += new System.EventHandler(this.fmoduleListView_SelectedIndexChanged);
            // 
            // frameGroupBox
            // 
            this.frameGroupBox.Controls.Add(this.frameListView);
            this.frameGroupBox.Location = new System.Drawing.Point(8, 8);
            this.frameGroupBox.MinimumSize = new System.Drawing.Size(364, 381);
            this.frameGroupBox.Name = "frameGroupBox";
            this.frameGroupBox.Size = new System.Drawing.Size(364, 381);
            this.frameGroupBox.TabIndex = 8;
            this.frameGroupBox.TabStop = false;
            this.frameGroupBox.Text = "Frames";
            // 
            // frameListView
            // 
            this.frameListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.frameListView.Items = null;
            this.frameListView.Location = new System.Drawing.Point(3, 16);
            this.frameListView.Name = "frameListView";
            this.frameListView.Size = new System.Drawing.Size(358, 362);
            this.frameListView.TabIndex = 6;
            this.frameListView.SelectedIndexChanged += new System.EventHandler(this.frameListView_SelectedIndexChanged);
            // 
            // frameImageViewer
            // 
            this.frameImageViewer.AllowDrop = true;
            this.frameImageViewer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.frameImageViewer.BackColor = System.Drawing.Color.Transparent;
            this.frameImageViewer.Cursor = System.Windows.Forms.Cursors.Default;
            this.frameImageViewer.Location = new System.Drawing.Point(377, 3);
            this.frameImageViewer.Mode = SpriteEditor.Core.ImageViewerMode.Module;
            this.frameImageViewer.Name = "frameImageViewer";
            this.frameImageViewer.Size = new System.Drawing.Size(796, 449);
            this.frameImageViewer.TabIndex = 5;
            this.frameImageViewer.Zoom = 100;
            this.frameImageViewer.MovedRect += new System.EventHandler(this.imageViewer_MovedRect);
            // 
            // animationsTabPage
            // 
            this.animationsTabPage.AutoScroll = true;
            this.animationsTabPage.AutoScrollMinSize = new System.Drawing.Size(1176, 627);
            this.animationsTabPage.Controls.Add(this.aframePickerGroupBox);
            this.animationsTabPage.Controls.Add(this.aframeGroupBox);
            this.animationsTabPage.Controls.Add(this.animationGroupBox);
            this.animationsTabPage.Controls.Add(this.animationImageViewer);
            this.animationsTabPage.Location = new System.Drawing.Point(4, 22);
            this.animationsTabPage.Name = "animationsTabPage";
            this.animationsTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.animationsTabPage.Size = new System.Drawing.Size(1176, 627);
            this.animationsTabPage.TabIndex = 1;
            this.animationsTabPage.Text = "Animations";
            this.animationsTabPage.UseVisualStyleBackColor = true;
            // 
            // aframePickerGroupBox
            // 
            this.aframePickerGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.aframePickerGroupBox.Controls.Add(this.framePicker);
            this.aframePickerGroupBox.Location = new System.Drawing.Point(377, 458);
            this.aframePickerGroupBox.Name = "aframePickerGroupBox";
            this.aframePickerGroupBox.Size = new System.Drawing.Size(791, 166);
            this.aframePickerGroupBox.TabIndex = 13;
            this.aframePickerGroupBox.TabStop = false;
            this.aframePickerGroupBox.Text = "Frames";
            // 
            // framePicker
            // 
            this.framePicker.AutoScroll = true;
            this.framePicker.Dock = System.Windows.Forms.DockStyle.Fill;
            this.framePicker.Location = new System.Drawing.Point(3, 16);
            this.framePicker.Name = "framePicker";
            this.framePicker.Size = new System.Drawing.Size(785, 147);
            this.framePicker.TabIndex = 10;
            this.framePicker.DoubleClick += new System.EventHandler(this.framePicker_DoubleClick);
            // 
            // aframeGroupBox
            // 
            this.aframeGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.aframeGroupBox.Controls.Add(this.aframeListView);
            this.aframeGroupBox.Location = new System.Drawing.Point(8, 373);
            this.aframeGroupBox.Name = "aframeGroupBox";
            this.aframeGroupBox.Size = new System.Drawing.Size(363, 251);
            this.aframeGroupBox.TabIndex = 12;
            this.aframeGroupBox.TabStop = false;
            this.aframeGroupBox.Text = "Animation Frames";
            // 
            // aframeListView
            // 
            this.aframeListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.aframeListView.Items = null;
            this.aframeListView.Location = new System.Drawing.Point(3, 16);
            this.aframeListView.Name = "aframeListView";
            this.aframeListView.Size = new System.Drawing.Size(357, 232);
            this.aframeListView.TabIndex = 10;
            this.aframeListView.SelectedIndexChanged += new System.EventHandler(this.aframeListView_SelectedIndexChanged);
            // 
            // animationGroupBox
            // 
            this.animationGroupBox.Controls.Add(this.animationListView);
            this.animationGroupBox.Location = new System.Drawing.Point(8, 3);
            this.animationGroupBox.Name = "animationGroupBox";
            this.animationGroupBox.Size = new System.Drawing.Size(364, 364);
            this.animationGroupBox.TabIndex = 11;
            this.animationGroupBox.TabStop = false;
            this.animationGroupBox.Text = "Animations";
            // 
            // animationListView
            // 
            this.animationListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.animationListView.Items = null;
            this.animationListView.Location = new System.Drawing.Point(3, 16);
            this.animationListView.Name = "animationListView";
            this.animationListView.Size = new System.Drawing.Size(358, 345);
            this.animationListView.TabIndex = 6;
            this.animationListView.SelectedIndexChanged += new System.EventHandler(this.animationListView_SelectedIndexChanged);
            // 
            // animationImageViewer
            // 
            this.animationImageViewer.AllowDrop = true;
            this.animationImageViewer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.animationImageViewer.BackColor = System.Drawing.Color.Transparent;
            this.animationImageViewer.Cursor = System.Windows.Forms.Cursors.Default;
            this.animationImageViewer.Location = new System.Drawing.Point(375, 6);
            this.animationImageViewer.Mode = SpriteEditor.Core.ImageViewerMode.Module;
            this.animationImageViewer.Name = "animationImageViewer";
            this.animationImageViewer.Size = new System.Drawing.Size(798, 446);
            this.animationImageViewer.TabIndex = 10;
            this.animationImageViewer.Zoom = 100;
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mousePositionStatusLabel});
            this.statusStrip.Location = new System.Drawing.Point(0, 690);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(1184, 22);
            this.statusStrip.TabIndex = 2;
            this.statusStrip.Text = "statusStrip1";
            // 
            // mousePositionStatusLabel
            // 
            this.mousePositionStatusLabel.Name = "mousePositionStatusLabel";
            this.mousePositionStatusLabel.Size = new System.Drawing.Size(26, 17);
            this.mousePositionStatusLabel.Text = "X, Y";
            // 
            // toolStrip
            // 
            this.toolStrip.Dock = System.Windows.Forms.DockStyle.Top;
            this.toolStrip.Location = new System.Drawing.Point(0, 0);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(1184, 28);
            this.toolStrip.TabIndex = 3;
            // 
            // SpriteEditorForm
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1184, 712);
            this.Controls.Add(this.toolStrip);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.tabControl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SpriteEditorForm";
            this.Text = "Sprite Editor";
            this.Load += new System.EventHandler(this.SpriteEditorForm_Load);
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.SpriteEditorForm_DragDrop);
            this.DragEnter += new System.Windows.Forms.DragEventHandler(this.SpriteEditorForm_DragEnter);
            this.tabControl.ResumeLayout(false);
            this.modulesTabPage.ResumeLayout(false);
            this.paletteGroupBox.ResumeLayout(false);
            this.moduleGroupBox.ResumeLayout(false);
            this.spriteDetailsGroupBox.ResumeLayout(false);
            this.spriteDetailsGroupBox.PerformLayout();
            this.framesTabPage.ResumeLayout(false);
            this.modulePickerGroupBox.ResumeLayout(false);
            this.fmoduleGroupBox.ResumeLayout(false);
            this.frameGroupBox.ResumeLayout(false);
            this.animationsTabPage.ResumeLayout(false);
            this.aframePickerGroupBox.ResumeLayout(false);
            this.aframeGroupBox.ResumeLayout(false);
            this.animationGroupBox.ResumeLayout(false);
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		private System.Windows.Forms.TabControl tabControl;
		private System.Windows.Forms.TabPage framesTabPage;
		private System.Windows.Forms.TabPage animationsTabPage;
		private Components.ImageViewer frameImageViewer;
		private System.Windows.Forms.StatusStrip statusStrip;
		private System.Windows.Forms.ToolStripStatusLabel mousePositionStatusLabel;
		private UI.SpriteEditorToolStrip toolStrip;
		private System.Windows.Forms.TabPage modulesTabPage;
		private Components.ListViewSpriteItem moduleListView;
		private Components.ImageViewer moduleImageViewer;
		private System.Windows.Forms.GroupBox spriteDetailsGroupBox;
		private System.Windows.Forms.Label spriteNameLabel;
		private System.Windows.Forms.Label spriteNameHeader;
		private System.Windows.Forms.Label spriteImageLabel;
		private System.Windows.Forms.Label spriteImageHeaderLabel;
		private System.Windows.Forms.LinkLabel changeSpriteImageLinkLabel;
		private System.Windows.Forms.GroupBox frameGroupBox;
		private Components.ListViewSpriteItem frameListView;
		private System.Windows.Forms.GroupBox fmoduleGroupBox;
		private Components.ListViewSpriteItem fmoduleListView;
		private System.Windows.Forms.GroupBox moduleGroupBox;
		private Components.SpriteItemPicker modulePicker;
        private System.Windows.Forms.GroupBox modulePickerGroupBox;
		private Components.ListViewSpriteItem paletteListView;
        private System.Windows.Forms.GroupBox paletteGroupBox;
		private System.Windows.Forms.GroupBox aframeGroupBox;
		private Components.ListViewSpriteItem aframeListView;
		private System.Windows.Forms.GroupBox animationGroupBox;
		private Components.ListViewSpriteItem animationListView;
		private Components.ImageViewer animationImageViewer;
		private System.Windows.Forms.GroupBox aframePickerGroupBox;
		private Components.SpriteItemPicker framePicker;
        private System.Windows.Forms.Timer playAnimationTimer;
	}
}

