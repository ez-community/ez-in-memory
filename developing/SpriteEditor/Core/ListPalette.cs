﻿using System;

namespace SpriteEditor.Core
{
	public class ListPalette : ListSpriteItem
	{
		public const int PALETTE_ID_START = 0; //0x1000;

		#region Override
		public override ListSpriteItem Clone()
		{
			return null;
		}

		public override int GetNextId()
		{
			return base.GetNextId(PALETTE_ID_START);
		}

		public override SpriteItem CreateDefaultItem(int id)
		{
			SpriteItem palette = new Palette(id, new System.Drawing.Color[0]);
			return palette;
		}

		public override string[] GetHeaders()
		{
			string[] headers = { "", "#", "Name" };
			return headers;
		}

		public override int[] GetHeadersWidth()
		{
			int[] widths = { 0, 60, 100 };
			return widths;
		}
		#endregion
	}
}
