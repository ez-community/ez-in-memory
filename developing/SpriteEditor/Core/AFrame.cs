﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace SpriteEditor.Core
{
	public class AFrame : SpriteItem
	{
		private int x, y;
		private Frame frame;
		private byte time;
		private byte flag;

		public AFrame(Frame frame, int x, int y)
			: base(frame.ID, frame.Name)
		{
			this.frame = frame;
			this.x = x;
			this.y = y;
		}

        public AFrame(Frame frame)
            : this(frame, 0, 0)
		{
		}

		#region Override
		public override SpriteItem Clone()
		{
			AFrame aframe = new AFrame(this.frame, this.x, this.y);
			return aframe;
		}

		public override bool IsValidItem()
		{
            if (this.frame == null) {
				return false;
            } else if (!this.frame.IsValidItem()) {
				return false;
			} else if (this.x < 0 || this.y < 0) {
				return false;
			} else {
				return true;
			}
		}

		public override string[] GetValues()
		{
			string[] values = {
				this.ID.ToString(), this.x.ToString(), this.y.ToString(), this.time.ToString(), 
				this.Name, base.GetFlagName(this.flag)
			};

			return values;
		}

		public override void Dispose()
		{
            this.frame = null;
		}
		#endregion

		#region Properties
		public int X
		{
			get
			{
				return this.x;
			}

			set
			{
				this.x = value;
			}
		}

		public int Y
		{
			get
			{
				return this.y;
			}

			set
			{
				this.y = value;
			}
		}

		public Frame Frame
		{
			get
			{
                return this.frame;
			}
		}
		
		public byte Time
		{
			get
			{
				return this.time;
			}
			
			set
			{
				this.time = value;
			}
		}

		public byte Flag
		{
			get
			{
				return this.flag;
			}

			set
			{
				this.flag = value;
			}
		}
		#endregion

		#region Public
		public Rectangle GetRectangle()
		{
			Rectangle rect = new Rectangle();

			if (this.frame.IsValidItem()) {
				rect = this.frame.GetRectangle();
				rect.X += this.x;
				rect.Y += this.y;
			}

			return rect;
		}
		#endregion
	}
}
