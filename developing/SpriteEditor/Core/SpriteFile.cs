﻿using System;
using System.Text;
using System.IO;
using System.Xml;
using System.Drawing;

using SpriteEditor.Core;

namespace SpriteEditor.Core
{
	public class SpriteFile
	{
		public const String SPRITE_NODE					= "sprite";
		public const String IMAGE_NODE					= "image";
		public const String PALETTES_NODE				= "palettes";
		public const String PALETTE_NODE				= "palette";
		public const String SUMMARY_NODE				= "sumarry";
		public const String MODULES_NODE				= "modules";
		public const String MODULE_NODE					= "module";
		public const String FMODULES_NODE				= "fmodules";
		public const String FMODULE_NODE				= "fmodule";
		public const String FRAMES_NODE					= "frames";
		public const String FRAME_NODE					= "frame";
		public const String AFRAMES_NODE				= "aframes";
		public const String AFRAME_NODE					= "aframe";
        public const String ANIMATIONS_NODE         	= "animations";
        public const String ANIMATION_NODE          	= "animation";

        public const String MODULE_COUNT_ATTRIBUTE		= "module-count";
        public const String FMODULE_COUNT_ATTRIBUTE		= "fmodule-count";
        public const String FRAME_COUNT_ATTRIBUTE		= "frame-count";
        public const String AFRAME_COUNT_ATTRIBUTE		= "aframe-count";
        public const String ANIMATION_COUNT_ATTRIBUTE	= "animation-count";

		public const String ID_ATTRIBUTE				= "id";
		public const String X_ATTRIBUTE					= "x";
		public const String Y_ATTRIBUTE					= "y";
		public const String WIDTH_ATTRIBUTE				= "width";
		public const String HEIGHT_ATTRIBUTE			= "height";
		public const String NAME_ATTRIBUTE				= "name";
		public const String TIME_ATTRIBUTE				= "time";
		public const String FLAGS_ATTRIBUTE				= "flags";

		//
		// Properties
		//
		public String FileName { get; set; }
		public ListModule Modules { get; set; }
		public ListFrame Frames { get; set; }
		public bool IsNewSprite { get; set; }
		public ListPalette Palettes { get; set; }
		public ListAnimation Animations { get; set; }

		private Platform platform;
		private String imageFileName;

		#region Static
		public static void New(String fileName)
		{
			XmlWriterSettings settings = new XmlWriterSettings();
			settings.Indent = true;
			settings.IndentChars = "\t";
			settings.Encoding = Encoding.UTF8;
			XmlWriter writer = XmlWriter.Create(fileName, settings);
			writer.WriteStartElement(SPRITE_NODE);			            // Sprite (root)
			writer.WriteEndElement();
			writer.Close();
		}
		#endregion

		public SpriteFile()
		{
			this.FileName = "";
			this.Modules = new ListModule();
			this.Frames = new ListFrame();
			this.Palettes = new ListPalette();
			this.Animations = new ListAnimation();
			this.IsNewSprite = true;
		}

		public SpriteFile(String name, String imageFileName)
		{
			this.FileName = name;
			this.imageFileName = imageFileName;
			this.Modules = new ListModule();
			this.Frames = new ListFrame();
			this.Palettes = new ListPalette();
			this.Animations = new ListAnimation();
			this.IsNewSprite = true;
			Module.MaxSize = new Size(0, 0);
		}

		public SpriteFile(String inputFile)
		{
			this.FileName = inputFile;
			this.Modules = new ListModule();
			this.Frames = new ListFrame();
			this.Palettes = new ListPalette();
			this.Animations = new ListAnimation();
			this.IsNewSprite = false;

			this.Read();

            for (int i = 0; i < this.Modules.Count; i++)
                this.Modules[i].SpriteItemImage = GetModuleImage((Module)this.Modules[i], true);
            for (int i = 0; i < this.Frames.Count; i++)
                this.Frames[i].SpriteItemImage = GetFrameImage((Frame)this.Frames[i], true);
		}

		public Size GetMaxModuleSize()
		{
			Size size = new Size();

			for (int i = 0; i < this.Modules.Count - 1; i++) {
				size.Width = Math.Max(((Module) this.Modules[i]).Width, ((Module) this.Modules[i + 1]).Width);
				size.Height = Math.Max(((Module) this.Modules[i]).Height, ((Module) this.Modules[i + 1]).Height);
			}

			return size;
		}

		private int GetAllModuleSize()
		{
			int length = 0;
			for (int i = 0; i < this.Modules.Count; i++) {
				Module module = (Module)this.Modules[i];
				length += (module.Width * module.Height);
			}

			return length;
		}
				
		#region Properties
		public Platform Platform
		{
			set
			{
				this.platform = value;
			}
		}

		public string ImageFileName
		{
			get
			{
				return this.imageFileName;
			}
			set
			{
				this.imageFileName = value;
				Module.MaxSize = ImageLib.GetSize(value);
				Color[] colors = ImageLib.GetColorPalette(value);
				Palette.Size = colors.Length;
				if (this.Palettes.Count == 0) {
					Palette palette = new Palette(0, "IMAGE", colors);
					this.Palettes.Add(palette);
				} else {
					((Palette)this.Palettes[0]).Colors = colors;
				}
			}
		}
		#endregion

		#region Image
		public Image GetSpriteImage()
		{
			return ImageLib.GetImage(this.imageFileName);
		}

		public Image GetModuleImage(Module module, bool createNew)
		{
			if (module != null) {
                if (createNew)
                {
                    Bitmap bitmap = null;
                    bitmap = ImageLib.Crop(this.imageFileName, module.GetRectangle());
                    if (bitmap != null)
                        bitmap.MakeTransparent(Color.Magenta);
                    return bitmap;
                }
                else
                {
                    return module.SpriteItemImage;
                }
			}
            return null;
		}

		public Image[] GetAllModuleImages()
		{
			int length = this.Modules.Count;
			Image[] images = new Image[length];
			for (int i = 0; i < length; i++) {
				Module module = (Module)this.Modules[i];
				images[i] = this.GetModuleImage(module, false);
			}

			return images;
		}

		public Image GetFModuleImage(FModule fmodule)
		{
            Image image;
            if (fmodule.Module.SpriteItemImage != null)
                image = fmodule.Module.SpriteItemImage;
            else
			    image = this.GetModuleImage(fmodule.Module, true);
			ImageLib.RotateFlip(image, fmodule.Flags);

			return image;
		}

		public Image GetFrameImage(Frame frame, bool createNew)
		{
            if (createNew)
            {
                Rectangle rect = frame.GetRectangle();
                int baseX = -rect.X;
                int baseY = -rect.Y;
                int width = rect.Width;
                int height = rect.Height;

                Bitmap bitmap = null;

                if (width > 0 && height > 0)
                {
                    bitmap = new Bitmap(width, height);
                    Graphics g = Graphics.FromImage(bitmap);

                    for (int i = 0; i < frame.FModules.Count; i++)
                    {
                        FModule fmodule = (FModule)frame.FModules[i];
                        Image image = this.GetFModuleImage(fmodule);

                        if (image != null)
                        {
                            int x = fmodule.X;
                            int y = fmodule.Y;
                            g.DrawImage(image, baseX + x, baseY + y);
                        }
                    }
                    g.Dispose();
                }

                return bitmap;
            }
            else
            {
                return frame.SpriteItemImage;
            }
		}

		public Image[] GetAllFrameImages()
		{
			int length = this.Frames.Count;
			Image[] images = new Image[length];
			for (int i = 0; i < length; i++) {
				Frame frame = (Frame)this.Frames[i];
				images[i] = this.GetFrameImage(frame, false);
			}

			return images;
		}

		public Image GetAFrameImage(AFrame aframe)
		{
            Image image;
            if (aframe.Frame.SpriteItemImage != null)
                image = aframe.Frame.SpriteItemImage;
            else
			    image = this.GetFrameImage(aframe.Frame,true);
			ImageLib.RotateFlip(image, aframe.Flag);

			return image;
		}
		#endregion

		#region Read/write
		public void Read()
		{
			XmlReader reader = XmlReader.Create(this.FileName);

			while (reader.Read()) {
				if (reader.IsStartElement()) {			// Only dectect start elements
					if (reader.NodeType == XmlNodeType.Element && reader.Name == IMAGE_NODE) {
						reader.Read();

						if (reader.Value.Trim().Length > 0) {
							this.ImageFileName = Path.GetDirectoryName(this.FileName) + Path.DirectorySeparatorChar + reader.Value.Trim();
						} else {
							this.ImageFileName = "null";
						}
					} else if (reader.NodeType == XmlNodeType.Element && reader.Name == PALETTE_NODE) {
						int id = Int16.Parse(reader.GetAttribute(ID_ATTRIBUTE));
						String name = Path.GetDirectoryName(this.FileName) + Path.DirectorySeparatorChar + reader.GetAttribute(NAME_ATTRIBUTE);
						Palette palette = new Palette(this.Palettes.GetNextId(), name);
						this.Palettes.Add(palette);
					} else if (reader.NodeType == XmlNodeType.Element && reader.Name == MODULE_NODE)	{
						int id = Int16.Parse(reader.GetAttribute(ID_ATTRIBUTE));
						int x = Int16.Parse(reader.GetAttribute(X_ATTRIBUTE));
						int y = Int16.Parse(reader.GetAttribute(Y_ATTRIBUTE));
						int width = Int16.Parse(reader.GetAttribute(WIDTH_ATTRIBUTE));
						int height = Int16.Parse(reader.GetAttribute(HEIGHT_ATTRIBUTE));
						String name = reader.GetAttribute(NAME_ATTRIBUTE);

						Module module = new Module(id, x, y, width, height, name);

						this.Modules.Add(module);
					} else if (reader.NodeType == XmlNodeType.Element && reader.Name == FRAME_NODE)	{
						int id = Int16.Parse(reader.GetAttribute(ID_ATTRIBUTE));
						String name = reader.GetAttribute(NAME_ATTRIBUTE);

						Frame frame = new Frame(id, name);

						this.Frames.Add(frame);
					} else if (reader.NodeType == XmlNodeType.Element && reader.Name == FMODULE_NODE) {
						int id = Int16.Parse(reader.GetAttribute(ID_ATTRIBUTE));
						int x = Int16.Parse(reader.GetAttribute(X_ATTRIBUTE));
						int y = Int16.Parse(reader.GetAttribute(Y_ATTRIBUTE));
						byte flags = byte.Parse(reader.GetAttribute(FLAGS_ATTRIBUTE));

						Module module = (Module)this.Modules.GetItemById(id);
						if (module != null) {
							FModule fmodule = new FModule(module, x, y);
							fmodule.Flags = flags;
							Frame frame = (Frame)this.Frames[this.Frames.Count - 1];
							frame.FModules.Add(fmodule);
						}
					} else if (reader.NodeType == XmlNodeType.Element && reader.Name == ANIMATION_NODE)	{
						int id = Int16.Parse(reader.GetAttribute(ID_ATTRIBUTE));
						String name = reader.GetAttribute(NAME_ATTRIBUTE);

						Animation animation = new Animation(id, name);
						this.Animations.Add(animation);
					}
					else if (reader.NodeType == XmlNodeType.Element && reader.Name == AFRAME_NODE)
					{
						int id = Int16.Parse(reader.GetAttribute(ID_ATTRIBUTE));
						int x = Int16.Parse(reader.GetAttribute(X_ATTRIBUTE));
						int y = Int16.Parse(reader.GetAttribute(Y_ATTRIBUTE));
						byte time = byte.Parse(reader.GetAttribute(TIME_ATTRIBUTE));
						byte flags = byte.Parse(reader.GetAttribute(FLAGS_ATTRIBUTE));

						Frame frame = (Frame)this.Frames.GetItemById(id);
						if (frame != null)
						{
							AFrame aframe = new AFrame(frame, x, y);
							aframe.Time = time;
							aframe.Flag = flags;
							Animation animation = (Animation)this.Animations[this.Animations.Count - 1];
							animation.AFrames.Add(aframe);
						}
					}
				}
			}
			reader.Close();
		}

		public void Save(String fileName)
		{
			XmlWriterSettings settings = new XmlWriterSettings();
			settings.Indent = true;
			settings.IndentChars = "\t";
			settings.Encoding = Encoding.UTF8;
			XmlWriter writer = XmlWriter.Create(fileName, settings);

			writer.WriteStartElement(SPRITE_NODE);						// Sprite (root)
			writer.WriteElementString(IMAGE_NODE,
				Path.GetFileName(this.imageFileName));      			// Image

			writer.WriteStartElement(PALETTES_NODE);					// Palettes
			for (int i = 1; i < this.Palettes.Count; i++) {
				writer.WriteStartElement(PALETTE_NODE);					// Palette

				Palette palette = (Palette)this.Palettes[i];
				writer.WriteAttributeString(ID_ATTRIBUTE, palette.ID.ToString());
				writer.WriteAttributeString(NAME_ATTRIBUTE, palette.Name);

				writer.WriteEndElement();								// End palette
			}
			writer.WriteEndElement();									// End palettes

			writer.WriteStartElement(SUMMARY_NODE);						// Summary
			writer.WriteAttributeString(MODULE_COUNT_ATTRIBUTE, this.Modules.Count.ToString());
			writer.WriteAttributeString(FMODULE_COUNT_ATTRIBUTE, this.Frames.GetFModulesCount().ToString());
			writer.WriteAttributeString(FRAME_COUNT_ATTRIBUTE, this.Frames.Count.ToString());
			writer.WriteAttributeString(AFRAME_COUNT_ATTRIBUTE, this.Animations.GetAFrameCount().ToString());
			writer.WriteAttributeString(ANIMATION_COUNT_ATTRIBUTE, this.Animations.Count.ToString());
			writer.WriteEndElement();									// End summary

			writer.WriteStartElement(MODULES_NODE);						// Modules
			for (int i = 0; i < this.Modules.Count; i++) {
				writer.WriteStartElement(MODULE_NODE);					// Module

				Module module = (Module)this.Modules[i];
				writer.WriteAttributeString(ID_ATTRIBUTE, module.ID.ToString());
				writer.WriteAttributeString(X_ATTRIBUTE, module.X.ToString());
				writer.WriteAttributeString(Y_ATTRIBUTE, module.Y.ToString());
				writer.WriteAttributeString(WIDTH_ATTRIBUTE, module.Width.ToString());
				writer.WriteAttributeString(HEIGHT_ATTRIBUTE, module.Height.ToString());
				writer.WriteAttributeString(NAME_ATTRIBUTE, module.Name);

				writer.WriteEndElement();								// End module
			}
			writer.WriteEndElement();									// End modules

			writer.WriteStartElement(FRAMES_NODE);						// Frames
			for (int i = 0; i < this.Frames.Count; i++) {
				writer.WriteStartElement(FRAME_NODE);					// Frame

				Frame frame = (Frame)this.Frames[i];
				writer.WriteAttributeString(ID_ATTRIBUTE, frame.ID.ToString());
				writer.WriteAttributeString(NAME_ATTRIBUTE, frame.Name);

				writer.WriteStartElement(FMODULES_NODE);				// FModules
				for (int j = 0; j < frame.FModules.Count; j++) {
					FModule fmodule = (FModule)frame.FModules[j];
					writer.WriteStartElement(FMODULE_NODE);				// FModule

					writer.WriteAttributeString(ID_ATTRIBUTE, fmodule.ID.ToString());
					writer.WriteAttributeString(X_ATTRIBUTE, fmodule.X.ToString());
					writer.WriteAttributeString(Y_ATTRIBUTE, fmodule.Y.ToString());
					writer.WriteAttributeString(FLAGS_ATTRIBUTE, fmodule.Flags.ToString());

					writer.WriteEndElement();							// End fmodule
				}
				writer.WriteEndElement();								// End fmodules
				writer.WriteEndElement();								// End frame
			}
			writer.WriteEndElement();									// End frames

			writer.WriteStartElement(ANIMATIONS_NODE);					// Animations
			for (int i = 0; i < this.Animations.Count; i++)
			{
				writer.WriteStartElement(ANIMATION_NODE);				// Animation

				Animation animation = (Animation)this.Animations[i];
				writer.WriteAttributeString(ID_ATTRIBUTE, animation.ID.ToString());
				writer.WriteAttributeString(NAME_ATTRIBUTE, animation.Name);

				writer.WriteStartElement(AFRAMES_NODE);					// AFrames
				for (int j = 0; j < animation.AFrames.Count; j++)
				{
					AFrame aframe = (AFrame)animation.AFrames[j];
					writer.WriteStartElement(AFRAME_NODE);				// AFrame

					writer.WriteAttributeString(ID_ATTRIBUTE, aframe.ID.ToString());
					writer.WriteAttributeString(X_ATTRIBUTE, aframe.X.ToString());
					writer.WriteAttributeString(Y_ATTRIBUTE, aframe.Y.ToString());
					writer.WriteAttributeString(TIME_ATTRIBUTE, aframe.Time.ToString());
					writer.WriteAttributeString(FLAGS_ATTRIBUTE, aframe.Flag.ToString());

					writer.WriteEndElement();							// End AFrame
				}
				writer.WriteEndElement();								// End AFrames
				writer.WriteEndElement();								// End Animtion
			}
			writer.WriteEndElement();									// End Animtions

			writer.WriteEndElement();									// End Sprite (root)

			writer.Close();
		}

		public void Save()
		{
			this.Save(this.FileName);
		}
		#endregion

		#region Export
		/// <summary>
		/// Save the binary file.
		/// </summary>
		public void SaveBinary(String fileName)
		{
			// Use Big Endian instead Little Endian (Little Endian is default in C#)
			FileStream stream = new FileStream(fileName, FileMode.Create);
			BinaryWriterEx writer = new BinaryWriterEx(stream);
			if (this.platform == Platform.J2ME) {
				writer.ByteOrder = ByteOrder.BigEndian;
			} else if (this.platform == Platform.WindowsPhone) {
				writer.ByteOrder = ByteOrder.LittleEndian;
			}

			//
			// Palettes, using on J2ME only
			//
			if (this.platform == Platform.J2ME) {
				writer.Write((byte) this.Palettes.Count);			// Palette count
				writer.Write((short) Palette.Size);					// Color count in a palette

				for (int i = 0; i < this.Palettes.Count; i++) {
					Palette palette = (Palette)this.Palettes[i];
					byte[] data = palette.GetPalette();
					writer.Write(data);
				}
			}

			//
			// Modules
			//
			writer.Write(ImageLib.GetPixelLength(this.imageFileName));	// Format Pixel (length)
			if (this.platform == Platform.J2ME) {					// J2ME only
				writer.Write((uint) this.GetAllModuleSize());		// All module image buffer size
			}
			writer.Write((short) Modules.Count);					// Modules count
			for (int i = 0; i < this.Modules.Count; i++) {			// Modules details
				Module module = (Module) this.Modules[i];
				writer.Write((short) module.Width);					// Width
				writer.Write((short) module.Height);				// Height

				byte[] data = ImageLib.GetPixelData(this.imageFileName, module.GetRectangle());
				writer.Write(data);									// Image
			}

			//
			// Frames
			//
			writer.Write((short) this.Frames.Count);				// Frames count
			writer.Write((short) this.Frames.GetFModulesCount());	// FModules count in all frames
			for (int i = 0; i < this.Frames.Count; i++) {			// Frames details
				Frame frame = (Frame) this.Frames[i];
				writer.Write((short) frame.FModules.Count);			// FModules count in current frame
				for (int j = 0; j < frame.FModules.Count; j++) {	// FModule details
					FModule fmodule = (FModule) frame.FModules[j];
					Module module = (Module) this.Modules.GetItemById(fmodule.ID);
					int index = this.Modules.IndexOf(module);
					writer.Write((short) index);					// Module index
					writer.Write((short) fmodule.X);				// X
					writer.Write((short) fmodule.Y);				// Y
					writer.Write(fmodule.Flags);					// Flags
				}

				Rectangle rect = frame.GetRectangle();				// Frame rect
				writer.Write((short) rect.X);						// X
				writer.Write((short) rect.Y);						// Y
				writer.Write((short) rect.Width);					// Width
				writer.Write((short) rect.Height);					// Height
			}

			//
			// Animations
			//
			writer.Write((short) this.Animations.Count);			// Animations count
			writer.Write((short) this.Animations.GetAFrameCount());	// AFrames count in all animations
			for (int i = 0; i < this.Animations.Count; i++) {		// Animations details
				Animation animation = (Animation) this.Animations[i];
				writer.Write((short) animation.AFrames.Count);		// AFrames count in current animation
				for (int j = 0; j < animation.AFrames.Count; j++) {	// AFrame details
					AFrame aframe = (AFrame) animation.AFrames[j];
					Frame frame = (Frame) this.Frames.GetItemById(aframe.ID);
					int index = this.Frames.IndexOf(frame);
					writer.Write((short) index);					// Frame index
					writer.Write((short) aframe.X);					// X
					writer.Write((short) aframe.Y);					// Y
					writer.Write((byte) aframe.Time);				// Time
					writer.Write(aframe.Flag);						// Flags
				}
			}

			writer.Close();
			stream.Close();
		}

		public void SaveSource(String fileName)
		{
			String spriteName = Path.GetFileNameWithoutExtension(fileName).ToUpper();

			//
			// StringBuilder
			//
			StringBuilder builder = new StringBuilder();
			builder.Append("\n\t// Generate by Sprite Editor");
			builder.Append("\n\t//\n\t// ");
			builder.Append(spriteName);
			builder.Append("\n\t//\n");

			//
			// Modules ID
			//
			
			for (int i = 0; i < this.Modules.Count; i++) {
				SpriteItem module = this.Modules[i];
				if (module.IsValidName()) {
					if (this.platform == Platform.J2ME) {
						builder.Append("\tpublic static final int ");
					} else if (this.platform == Platform.WindowsPhone) {
						builder.Append("\tpublic const int ");
					}
					builder.Append(spriteName);
					builder.Append("_MODULE_");
					builder.Append(module.Name.ToUpper());
					builder.Append(" = ");
					builder.Append(i);
					builder.Append(";\n");
				}
			}

			//
			// Frames ID
			//
			for (int i = 0; i < this.Frames.Count; i++) {
				SpriteItem frame = this.Frames[i];
				if (frame.IsValidName()) {
					if (this.platform == Platform.J2ME) {
						builder.Append("\tpublic static final int ");
					} else if (this.platform == Platform.WindowsPhone) {
						builder.Append("\tpublic const int ");
					}
					builder.Append(spriteName);
					builder.Append("_FRAME_");
					builder.Append(frame.Name.ToUpper());
					builder.Append(" = ");
					builder.Append(i);
					builder.Append(";\n");
				}
			}

			//
			// Animations ID
			//
			for (int i = 0; i < this.Animations.Count; i++) {
				SpriteItem animation = this.Animations[i];
				if (animation.IsValidName()) {
					if (this.platform == Platform.J2ME) {
						builder.Append("\tpublic static final int ");
					} else if (this.platform == Platform.WindowsPhone) {
						builder.Append("\tpublic const int ");
					}
					builder.Append(spriteName);
					builder.Append("_ANIMATION_");
					builder.Append(animation.Name.ToUpper());
					builder.Append(" = ");
					builder.Append(i);
					builder.Append(";\n");
				}
			}

			//
			// Save
			//
			StreamWriter writer = new StreamWriter(fileName, false);	// Always overwriter
			writer.Write(builder.ToString());
			writer.Close();
		}
		#endregion
	}
}
