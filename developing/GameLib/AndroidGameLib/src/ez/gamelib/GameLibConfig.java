package ez.gamelib;

public class GameLibConfig
{
	// Debug mode
	public static boolean debug						= true;

	// Screen width & screen height
	public static boolean useFullScreen 			= false;	// Set true if want using full screen
	public static boolean setScreenSize 			= false;	// Set true if want define screen size
	public static int screenWidth					= 800;
	public static int screenHeight					= 480;

	public static boolean useSleepAfterEachFrame 	= true;
	public static int sleepTime 					= 20;

	// Sound
	public static boolean useCachedPlayers			= true;
	public static boolean usePrefetchedPlayers		= true;		// Set true when useCachedPlayers only (call realize() & prefetch() while cache)

	// Software double buffering
	public static boolean useDoubleBuffering		= false;

	// Transition effects
	public static boolean useTransitionEffect		= true;		// Use transition effect (set true in template file to avoid removed code while optimize)
	public static int transitionSpeed				= 20;		// Speed, should be a even number

	// Use circular buffer for inprove FPS
	public static boolean useCircularBuffer			= true;		// Paint all tilesets to buffer.
}
