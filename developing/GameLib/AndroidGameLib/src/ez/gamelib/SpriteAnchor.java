package ez.gamelib;

public class SpriteAnchor
{
	public static final int NONE				= 0;
	public static final int LEFT				= 0;
	public static final int TOP					= 0;
	public static final int RIGHT				= 1;
	public static final int BOTTOM				= 1 << 1;
	public static final int HCENTER				= 1 << 2;
	public static final int VCENTER				= 1 << 3;
}
