package ez.gamelib;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.zip.GZIPInputStream;

import android.app.Activity;
import android.content.res.Resources;

public class Package
{
	//
	// Package compress method
	//
//	private static final byte COMPRESS_TYPE_NONE 	= 0;
	private static final byte COMPRESS_TYPE_GZIP 	= 1;

	// Text encoding
	private static final String TEXT_ENCODING		= "UTF-8";

	//
	// Data package
	//
	private static Activity activity;
	private static String packageFile;
	private static boolean useGzip;		// Compress
	private static int size;			// Package size
	private static int fileCount;		// Total files in this package
	private static int[] offsets;		// File offsets
	private static int[] lengths;		// File length

	private static String[] texts;		// Text cache

	public static void SetActivity(Activity _activity)
	{
		activity = _activity;
	}

	public static int GetGzipDecompressSize(byte[] buf)
	{
		int pos = buf.length;						// Last 4 bytes of Gzip buffer (Little Endian)
		int length = ((buf[pos] & 0xff) << 24)
					| ((buf[pos - 1] & 0xff) << 16)
					| ((buf[pos - 2] & 0xff) << 8)
					| (buf[pos - 3] & 0xff);
		return length;
	}

	public static void Open(String fileName)
	{
		packageFile = fileName;

		try {
//			InputStream in = Resources.class.getResourceAsStream(packageFile);
			InputStream in = activity.getAssets().open(fileName);
			DataInputStream reader = new DataInputStream(in);
			useGzip = (reader.readByte() == COMPRESS_TYPE_GZIP) ? true : false;	// Compress type
			size = reader.readInt();											// Package size
			fileCount = reader.readShort();										// File count

			// Load file offsets
			offsets = new int[fileCount];										// All file offsets
			for (int i = 0; i < fileCount; i++) {
				offsets[i] = reader.readInt();									// File offset
			}

			// Calculate file lengths
			lengths = new int[fileCount];
			for (int i = 1; i < fileCount; i++) {						// From begin to end, except the last file
				lengths[i - 1] = offsets[i] - offsets[i - 1];
			}
			lengths[fileCount - 1] = size - offsets[fileCount - 1] - 1;	// The last file

			reader.close();
			in.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void Close()
	{
		// Reset package properties
		packageFile = null;
		useGzip = false;
		size = 0;
		offsets = null;
		lengths = null;
	}

	public static int GetFileCount()
	{
		return fileCount;
	}

	/**
	 * Get data from a package which packed by data-package tool.
	 */
	public static byte[] GetBytes(int fileIndex)
	{
		int offset = offsets[fileIndex];
		int length = lengths[fileIndex];
		byte[] buf = new byte[length];

		try {				// Read data
			InputStream in = Resources.class.getResourceAsStream(packageFile);
			DataInputStream reader = new DataInputStream(in);
			reader.skipBytes(offset);
			reader.read(buf, 0, length);

			reader.close();
			in.close();

			// TODO Support GZIP
			if (useGzip) {	// Decompress data
				ByteArrayInputStream stream = new ByteArrayInputStream(buf);
				GZIPInputStream gzipReader = new GZIPInputStream(stream);
				length = GetGzipDecompressSize(buf);
				byte[] decompress = new byte[length];
				gzipReader.read(decompress, 0, length);

				gzipReader.close();
				stream.close();

				buf = null;
				buf = decompress;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return buf;
	}

	/**
	 * Load a text package into a String array.
	 * @param fileName	Package file name which is defined properties file.
	 */
	public static void LoadText(int index)
	{
		try {
			ByteArrayInputStream stream = new ByteArrayInputStream(GetBytes(index));
			DataInputStream reader = new DataInputStream(stream);

			int size = reader.readInt();
			texts = new String[size];

			for (int i = 0; i < size; i++) {
				int length = reader.readInt();
				byte[] buf = new byte[length];
				reader.read(buf, 0, length);
				String value = new String(buf, TEXT_ENCODING);
				texts[i] = value;
			}

			reader.close();
			stream.close();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Get a row from text package.
	 * @param textId	The index of this row which defined in Text class.
	 * @return			Value of this row in selected package.
	 */
	public static String GetText(int index)
	{
		if (index > -1)
			return texts[index];
		else
			return null;
	}

	public static Sprite LoadSprite(int fileIndex)
	{
		try {
			byte[] buf = GetBytes(fileIndex);
			Sprite sprite = new Sprite(buf);
			return sprite;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static FontSprite LoadFontSprite(int fileIndex, int characterMapping)
	{
		try {
			byte[] buf = GetBytes(fileIndex);
			FontSprite font = new FontSprite(buf);
			buf = GetBytes(characterMapping);
			font.SetFontMapping(buf);
			return font;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
}
