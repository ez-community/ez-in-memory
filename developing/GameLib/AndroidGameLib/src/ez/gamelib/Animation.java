package ez.gamelib;

import android.graphics.Canvas;

public class Animation
{
	private static int INVALID_ANIM = -1;

	private Sprite sprite;
	private int aframe;
	private int anim;
	private int frameCounter;
	private boolean isLoop;

	public Animation(Sprite sprite)
	{
		this.sprite = sprite;
		this.frameCounter = -1;
		this.isLoop = true;
	}

	//
	// Properties
	//
	public void SetSprite(Sprite value)
	{
		this.sprite = value;
		this.frameCounter = -1;
	}

	public Sprite GetSprite()
	{
		return this.sprite;
	}

	public void SetAnim(int anim, boolean loop)
	{
		this.anim = anim;
		this.isLoop = loop;

		this.aframe = 0;
		this.frameCounter = 0;
	}

	public void SetAnim(int anim)
	{
		this.SetAnim(anim, true);
	}

	public int GetAnim()
	{
		return this.anim;
	}

	public void GetRect(int[] rect, int x, int y)
	{
		if (this.anim > INVALID_ANIM && !IsEndAnim()) {
			this.sprite.GetAFrameRect(rect, this.anim, this.aframe, x, y);
		} else {
			rect[2] = rect[3] = 0;
		}
	}

	//
	// Methods
	//
	public boolean IsEndAnim()
	{
		if (this.anim > INVALID_ANIM) {
			return (this.aframe >= this.sprite.GetAFrameCount(this.anim));
		}

		return true;
	}

	public void Update()
	{
		if (this.anim > INVALID_ANIM) {
			this.frameCounter++;

			if (this.frameCounter > this.sprite.GetAFrameTime(this.anim, this.aframe)) {
				this.frameCounter = 0;
				this.aframe++;
			}

			if (this.IsEndAnim()) {
				if (this.isLoop) {
					this.SetAnim(anim);
				} else {
					this.anim = INVALID_ANIM;
				}
			}
		}
	}

	public void Draw(Canvas canvas, int x, int y, int flags, float rotation, int anchor)
	{
		if (this.anim > INVALID_ANIM)
		{
			this.sprite.DrawAFrame(canvas, this.anim, this.aframe, x, y, flags, anchor);
		}
	}

	public void Draw(Canvas canvas, int x, int y, int anchor)
	{
		this.Draw(canvas, x, y, SpriteFlag.NONE, 0, anchor);
	}
}
