
#if USE_BLACKBERRY
import javax.microedition.lcdui.Canvas;

import net.rim.device.api.system.Application;
import net.rim.device.api.system.KeyListener;
import net.rim.device.api.ui.Keypad;
import net.rim.device.api.ui.KeypadUtil;
import net.rim.device.api.system.TrackwheelListener;
import net.rim.device.api.system.DeviceInfo;

#if IMPLEMENT_TRACKWHELLCLICK
public abstract class BlackBerryMIDP extends Canvas implements KeyListener, TrackwheelListener
#else	//IMPLEMENT_TRACKWHELLCLICK
public abstract class BlackBerryMIDP extends Canvas implements KeyListener
#endif	//IMPLEMENT_TRACKWHELLCLICK
{
	public static final int KEYCODE_GREENKEY 	= 2000;
	public static final int KEYCODE_REDKEY 		= 2001;
	public static final int KEYCODE_ESCAPE 		= -7;
	public static final int KEYCODE_MENU 		= -6;
	public static final int KEYCODE_BACKSPACE	= -9;

	public BlackBerryMIDP()
	{
		// Listeners
		Application.getApplication().addKeyListener(this);
		// Application.getApplication().addTrackwheelListener(this);
		// Application.getApplication().addSystemListener(this);
		// Application.getApplication().addGlobalEventListener(this);
	}

	public int getKey(int code)
	{
		int keypadCode;
		keypadCode = Keypad.key(code);

		if (keypadCode == Keypad.KEY_ESCAPE) {
			return KEYCODE_ESCAPE;
		} else if (keypadCode == Keypad.KEY_END) {
			return KEYCODE_REDKEY;
		} else if (keypadCode == Keypad.KEY_MENU) {
			return KEYCODE_MENU;
		} else if (keypadCode == Keypad.KEY_SEND) {
			return KEYCODE_GREENKEY;
		}

		return keypadCode;
	}

	// @Override
	public boolean keyChar(char key, int status, int time)
	{
		return true;
	}

	// @Override
	public boolean keyDown(int keycode, int arg1)
	{
		keycode = this.getKey(keycode);
		if (keycode == KEYCODE_REDKEY || keycode == KEYCODE_GREENKEY){
			hideNotify();
			Application.getApplication().requestBackground();
        }
		// super.keyPressed(keycode);
		keyPressed(keycode);			// !!!! Note: Call super.keyPressed doesn't call GameLib.keyPressed
		return true;
	}

	// @Override
	public boolean keyRepeat(int keycode, int time)
	{
		keycode = this.getKey(keycode);
		// super.keyRepeated(keycode);
		keyRepeated(keycode);
		return true;
	}

	// @Override
	public boolean keyStatus(int keycode, int time)
	{
		keycode = this.getKey(keycode);
		return true;
	}

	// @Override
	public boolean keyUp(int keycode, int time)
	{
		keycode = this.getKey(keycode);
		// super.keyReleased(keycode);
		keyReleased(keycode);
		return true;
	}

	#if IMPLEMENT_TRACKWHELLCLICK
	// @Override
	public boolean trackwheelClick(int status, int time)
	{
		keyPressed(IKeyboard.FIRE);
		return true;
	}

	public boolean trackwheelRoll(int amount, int status, int time)
	{
		if (amount < 0) {
			keyPressed(IKeyboard.UP);
		} else if (amount > 0) {
			keyPressed(IKeyboard.DOWN);
		}
		return true;
	}

	public boolean trackwheelUnclick(int status, int time)
	{
		keyReleased(IKeyboard.FIRE);
		return true;
	}
	#endif	//IMPLEMENT_TRACKWHELLCLICK
}
#endif	//USE_BLACKBERRY
