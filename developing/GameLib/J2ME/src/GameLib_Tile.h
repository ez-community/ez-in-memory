
	///*********************************************************************
	///* GameLib_Tile.h
	///*********************************************************************

	private static int tileWidth;			// Tile width
	private static int tileHeight;			// Tile height
	private static short[] layerWidths;		// Layer width
	private static short[] layerHeights;	// Layer height
	private static byte[][] tileFlags;		// Module flags in all layers
	private static short[][] tiles;			// Tile index in all layers

	/**
	 * Initialize map layers.
	 * @param count		The number of all layers.
	 * @param width		Tile width
	 * @param height	Tile height
	 */
	public static void setTileLayer(int count, int width, int height)
	{
		layerWidths = new short[count];
		layerHeights = new short[count];
		tileFlags = new byte[count][];
		tiles = new short[count][];

		tileWidth = width;
		tileHeight = height;
	}

	public static void loadTileset(int layer, String fileName, int fileIndex)
	{
		Package.open(fileName);

		try {
			byte[] buf = Package.getBytes(fileIndex);
			ByteArrayInputStream stream = new ByteArrayInputStream(buf);
			DataInputStream input = new DataInputStream(stream);

			short width = input.readShort();
			short height = input.readShort();
			int length = width * height;

			layerWidths[layer] = width;
			layerHeights[layer] = height;
			tileFlags[layer] = new byte[length];
			tiles[layer] = new short[length];

			for (int i = 0; i < length; i++) {
				byte flags = input.readByte();
				short tile = input.readShort();

				tileFlags[layer][i] = flags;
				tiles[layer][i] = tile;
			}

			stream.close();
			input.close();
		} catch (IOException e) {
			TRACE(e);
		}

		Package.close();
	}

	public static int getLayerWidth(int layer)
	{
		return layerWidths[layer] * tileWidth;
	}

	public static int getLayerHeight(int layer)
	{
		return layerHeights[layer] * tileHeight;
	}

	/**
	 * Paint the layer at camera postion.
	 * @param cameraX		Camera X
	 * @param cameraY		Camera Y
	 */
	public static void paintTileset(Graphics g, GameSprite sprite, int layer, int cameraX, int cameraY)
	{
		int x1 = cameraX / tileWidth;
		int x2 = (cameraX + getScreenWidth()) / tileWidth;
		int y1 = cameraY / tileHeight;
		int y2 = (cameraY + getScreenHeight()) / tileHeight;

		if (x2 > layerWidths[layer] - 1) {
			x2 = layerWidths[layer] - 1;
		}
		if (y2 > layerHeights[layer] - 1) {
			y2 = layerHeights[layer] - 1;
		}

		int x, y;
		for (int i = x1; i <= x2; i++) {
			x = (i % layerWidths[layer]) * tileWidth;
			for (int j = y1; j <= y2; j++) {
				y = (j % layerHeights[layer]) * tileHeight;
				int index = (j * layerWidths[layer]) + i;
				int module = tiles[layer][index];
				int flags = tileFlags[layer][index];

				if (module > -1) {
					sprite.paintModule(g, module, x - cameraX, y - cameraY, flags, 0);
				}
			}
		}
	}

	/**
	 * Initialize circular buffer.
	 * @param count		The number of all layers.
	 * @param width		Tile width
	 * @param height	Tile height
	 */
	public static void initializeCircularBuffer(int camX, int camY)
	{
		if (!GameLibConfig.useCircularBuffer) {
			return;
		}

		circularBuffer = Image.createImage(getScreenWidth() + tileWidth * 2, getScreenHeight() + tileHeight * 2);
		circularGraphic = circularBuffer.getGraphics();

		// for (int i = 0; i<layerWidths.length; i++) {

		// }
	}
