
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.InputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
/* import java.util.zip.GZIPInputStream; */
import javax.microedition.lcdui.Image;
import javax.microedition.midlet.MIDlet;

public class Package
{
	//
	// Package compress method
	//
	private static final byte COMPRESS_TYPE_NONE 	= 0;
	private static final byte COMPRESS_TYPE_GZIP 	= 1;

	// Text encoding
	private static final String TEXT_ENCODING		= "UTF-8";

	private static MIDlet midlet;

	//
	// Data package
	//
	private static String packageFile;
	private static boolean useGzip;		// Compress
	private static int size;			// Package size
	private static int fileCount;		// Total files in this package
	private static int[] offsets;		// File offsets
	private static int[] lengths;		// File length

	private static String[] texts;		// Text cache

	public static void setMIDlet(MIDlet _midlet)
	{
		midlet = _midlet;
	}

	public static int getGzipDecompressSize(byte[] buf)
	{
		int pos = buf.length;						// Last 4 bytes of Gzip buffer (Little Endian)
		int length = ((buf[pos] & 0xff) << 24)
					| ((buf[pos - 1] & 0xff) << 16)
					| ((buf[pos - 2] & 0xff) << 8)
					| (buf[pos - 3] & 0xff);
		return length;
	}

	/**
	 * Open an InputStream and close it.
	 */
	public static void open(InputStream input)
	{
		try {
			DataInputStream reader = new DataInputStream(input);
			useGzip = (reader.readByte() == COMPRESS_TYPE_GZIP) ? true : false;	// Compress type
			size = reader.readInt();											// Package size
			fileCount = reader.readShort();										// File count

			// Load file offsets
			offsets = new int[fileCount];										// All file offsets
			for (int i = 0; i < fileCount; i++) {
				offsets[i] = reader.readInt();									// File offset
			}

			// Calculate file lengths
			lengths = new int[fileCount];
			for (int i = 1; i < fileCount; i++) {						// From begin to end, except the last file
				lengths[i - 1] = offsets[i] - offsets[i - 1];
			}
			lengths[fileCount - 1] = size - offsets[fileCount - 1] - 1;	// The last file

			reader.close();
			input.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void open(String fileName)
	{
		packageFile = fileName;
		
		try {
			InputStream input = midlet.getClass().getResourceAsStream(packageFile);
			open(input);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void close()
	{
		// Reset package properties
		packageFile = null;
		useGzip = false;
		size = 0;
		offsets = null;
		lengths = null;
	}

	public static int getFileCount()
	{
		return fileCount;
	}

	/**
	 * Get data from a package which packed by data-package tool.
	 */
	public static byte[] getBytes(InputStream input, int fileIndex)
	{
		int offset = offsets[fileIndex];
		int length = lengths[fileIndex];
		byte[] buf = new byte[length];

		try {				// Read data
			DataInputStream reader = new DataInputStream(input);
			reader.skipBytes(offset);
			reader.read(buf, 0, length);

			reader.close();
			input.close();

			// TODO Support GZIP
			/* if (useGzip) {	// Decompress data
				ByteArrayInputStream stream = new ByteArrayInputStream(buf);
				GZIPInputStream gzipReader = new GZIPInputStream(stream);
				length = getGzipDecompressSize(buf);
				byte[] decompress = new byte[length];
				gzipReader.read(decompress, 0, length);

				gzipReader.close();
				stream.close();

				buf = null;
				buf = decompress;
			} */
		} catch (Exception e) {
			e.printStackTrace();
		}

		return buf;
	}
	
	/**
	 * Get data from a package which packed by data-package tool.
	 */
	public static byte[] getBytes(int fileIndex)
	{
		byte[] buf = null;

		try {				// Read data
			InputStream input = midlet.getClass().getResourceAsStream(packageFile);
			buf = getBytes(input, fileIndex);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return buf;
	}

	/**
	 * Load a text package into a String array.
	 * @param fileName	Package file name which is defined properties file.
	 */
	public static void loadText(int index)
	{
		try {
			ByteArrayInputStream stream = new ByteArrayInputStream(getBytes(index));
			DataInputStream reader = new DataInputStream(stream);

			int size = reader.readInt();
			texts = new String[size];

			for (int i = 0; i < size; i++) {
				int length = reader.readInt();
				byte[] buf = new byte[length];
				reader.read(buf, 0, length);
				String value = new String(buf, TEXT_ENCODING);
				texts[i] = value;
			}

			reader.close();
			stream.close();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Get a row from text package.
	 * @param textId	The index of this row which defined in Text class.
	 * @return			Value of this row in selected package.
	 */
	public static String getText(int index)
	{
		if (index > -1)
			return texts[index];
		else
			return null;
	}

	public static GameSprite loadSprite(int fileIndex, boolean isCacheImages)
	{
		try {
			byte[] buf = getBytes(fileIndex);
			GameSprite sprite = new GameSprite(buf, isCacheImages);
			return sprite;
		} catch (IOException e) {
			TRACE(e);
			return null;
		}
	}

	public static FontSprite loadFontSprite(int fileIndex, int characterMapping, boolean isCacheImages)
	{
		try {
			byte[] buf = getBytes(fileIndex);
			FontSprite font = new FontSprite(buf, isCacheImages);
			buf = getBytes(characterMapping);
			font.setFontMapping(buf);
			return font;
		} catch (IOException e) {
			TRACE(e);
			return null;
		}
	}
}
