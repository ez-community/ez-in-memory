
	///*********************************************************************
	///* GameLib_Draw.h
	///*********************************************************************

	public static void setClip(int left, int top, int width, int height)
	{
		graphics.setClip(left, top, width, height);
	}

	public static void drawLine(Graphics g, int color, int x1, int y1, int x2, int y2)
	{
		g.setColor(color);
		g.drawLine(x1, y1, x2, y2);
	}

	public static void drawRect(Graphics g, int color, int x, int y, int width, int height, int borderWidth)
	{
		g.setColor(color);
		for (int i = 0; i < borderWidth; i++) {
			g.drawRect(x - i, y - i, width + (i << 1), height + (i << 1));
		}
	}

	public static void drawRect(Graphics g, int color, int x, int y, int width, int height)
	{
		drawRect(g, color, x, y, width, height, 1);
	}

	public static void fillRect(Graphics g, int color, int x, int y, int width, int height)
	{
		g.setColor(color);
		g.fillRect(x, y, width, height);
	}

	public static void fillRect(int color)
	{
		setClip(0, 0, screenWidth, screenHeight);
		fillRect(graphics, color, 0, 0, screenWidth, screenHeight);
	}

	public static void fillRect(Graphics g, int backgroundColor, int borderColor, int x, int y,
		int width, int height, int borderWidth)
	{
		fillRect(g, backgroundColor, x, y, width, height);
		drawRect(g, borderColor, x, y, width, height, borderWidth);
	}

	public static void fillRect(Graphics g, int backgroundColor, int borderColor, int x, int y, int width, int height)
	{
		fillRect(g, backgroundColor, borderColor, x, y, width, height, 1);
	}

	public static void fillRect(Graphics g, Image image, int left, int top, int width, int height)
	{
		g.setClip(left, top, width, height);

		int imageWidth = image.getWidth();
		int imageHeight = image.getHeight();

		for (int row = top; row < (top + height); row += imageHeight) {
			for (int col = left; col < (left + width); col += imageWidth) {
				g.drawImage(image, col, row, 0);
			}
		}
	}

	public static void fillRoundRect(Graphics g, int backgroundColor, int borderColor, int x,
			int y, int width, int height, int arcWidth)
	{
		g.setColor(backgroundColor);
		g.fillRoundRect(x, y, width, height, arcWidth, arcWidth);
		g.setColor(borderColor);
		g.drawRoundRect(x, y, width, height, arcWidth, arcWidth);
	}

	public static Image createTransparentImage(int width, int height, int color, int alpha)
	{
		DBG("[GameLib] Create a transparent image with color: " + color + " and alpha: " + alpha);
		int pixel = (alpha << 24) | color;
		int[] rgbData = new int[width * height];
		for (int i = 0; i < rgbData.length; i++) {
			rgbData[i] = pixel;
		}

		Image image = Image.createRGBImage(rgbData, width, height, true);
		return image;
	}
