
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;

import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;

/**
 * A very simple sprite.
 * @date Oct 24, 2011
 */

public class GameSprite
{
	//
	// Image
	//
	public static final int PALETTE_PIXEL_SIZE_BYTE		= 3;	// 3 bytes: RGB
	public static final int PIXEL_FORMAT_8BPP_INDEXED	= 1;
	public static final int PIXEL_FORMAT_24BPP_RGB		= 3;
	public static final int PIXEL_FORMAT_32BPP_ARGB		= 4;

	//
	// Flags, same as same transform values in javax.microedition.lcdui.game.Sprite class
	//
	public static final int FLAG_NONE					= 0;
	public static final int FLAG_FLIPY					= 1;
	public static final int FLAG_FLIPX					= 1 << 1;
	public static final int FLAG_ROTATE90				= 1 << 2;
	/* public static final int FLAG_NONE				= 0;
	public static final int FLAG_FLIPY					= 1;
	public static final int FLAG_FLIPX					= 2;
	public static final int FLAG_ROTATE180				= 3;
	public static final int FLAG_ROTATE270_FLIPY		= 4;
	public static final int FLAG_ROTATE90				= 5;
	public static final int FLAG_ROTATE270				= 6;
	public static final int FLAG_ROTATE90_FLIPY			= 7; */

	//
	// Palette
	//
	private int paletteCount;			// Palette count
	private int colorCount;				// Color count in a palette
	private int[] palettes;				// Palettes RGB	(all palettes)
	private int pixelLength;			// Length of a pixel by bytes
	private int palette;				// Current palette

	//
	// Module
	//
	private short[] modulesWidth;		// Module: width
	private short[] modulesHeight;		// Module: height
	private int[] modulesRGB;			// RGB data
	private int[] modulesRGBOffset;		// RGB data offset

	//
	// Frame
	//
	private short[] frames;				// Frames offset in fmodules
	private short[] framesLength;		// Frames length
	private short[] framesX;			// Frame X
	private short[] framesY;			// Frame Y
	private short[] framesWidth;		// Frame width
	private short[] framesHeight;		// Frame height

	//
	// FModule
	//
	private short[] fmodules;
	private short[] fmodulesX;
	private short[] fmodulesY;
	private byte[] fmodulesFlag;

	//
	// Animation
	//
	private short[] animations;			// Animations offset in aframes
	private short[] animationsLength;	// Animations length

	//
	// AFrame
	//
	private short[] aframes;			// AFrames offset in animations
	private short[] aframesX;			// AFrames x
	private short[] aframesY;			// AFrames y
	private byte[] aframesTime;			// AFrames time
	private byte[] aframesFlags;		// AFrames flags

	//
	// Cache
	//
	private boolean isCached;			// Use drawImage instead drawRGB
	private Image[] images;				// Cache all modules

	public GameSprite()
	{
	}

	public GameSprite(byte[] buf, boolean isCacheModules) throws IOException
	{
		this.load(buf);

		if (isCacheModules) {
			this.isCached = true;
			this.images = new Image[this.getModuleCount() * this.paletteCount];
		}
	}

	private static int[] getPalette(byte[] buf)
	{
		int length = buf.length / PALETTE_PIXEL_SIZE_BYTE;
		int[] palette = new int[length];

		for (int i = 0; i < length; i++) {
			int red = buf[i * 3] & 0xff;
			int green = buf[i * 3 + 1] & 0xff;
			int blue = buf[i * 3 + 2] & 0xff;
			int color = (0xff << 24) | (red << 16) | (green << 8) | blue;

			palette[i] = color;
		}

		return palette;
	}

	private static int[] getRGB(byte[] buf, int pixelFormat)
	{
		int length = buf.length / pixelFormat;
		int[] rgb = new int[length];

		for (int i = 0; i < length; i++) {
			int color, alpha, red, green, blue;
			if (pixelFormat == PIXEL_FORMAT_32BPP_ARGB) {
				alpha = buf[i * pixelFormat] & 0xff;
				red = buf[i * pixelFormat + 1] & 0xff;
				green = buf[i * pixelFormat + 2] & 0xff;
				blue = buf[i * pixelFormat + 3] & 0xff;
			} else {
				alpha = 0xff;
				red = buf[i * pixelFormat] & 0xff;
				green = buf[i * pixelFormat + 1] & 0xff;
				blue = buf[i * pixelFormat + 2] & 0xff;
			}
			color = (alpha << 24) | (red << 16) | (green << 8) | blue;
			rgb[i] = color;
		}

		return rgb;
	}

	// TODO Support more palettes
	private static int[] getRGB(byte[] buf, int[] palette, int colorCount, int index)
	{
		int length = buf.length;
		int[] rgb = new int[length];
		for (int i = 0; i < length; i++) {
			int pos = buf[i] & 0xff;
			rgb[i] = palette[colorCount * index + pos];

			// Remove transparent color
			if (rgb[i] == 0xffff00ff) {
				rgb[i] = 0x00ff00ff;
			}
		}

		return rgb;
	}

	private int getModuleOffset(int module)
	{
		return this.modulesRGBOffset[module];
	}

	private int getAFrameOffset(int animation)
	{
		return this.animations[animation];
	}

	private int getModuleRGBOffset(int module)
	{
		return this.modulesRGBOffset[module];
	}

	private int getFModuleOffset(int frame)
	{
		return this.frames[frame];
	}

	private int getFModuleX(int frame, int fmodule)
	{
		int offset = this.getFModuleOffset(frame) + fmodule;
		return this.fmodulesX[offset];
	}

	private int getFModuleY(int frame, int fmodule)
	{
		int offset = this.getFModuleOffset(frame) + fmodule;
		return this.fmodulesY[offset];
	}

	private int getFModuleFlag(int frame, int fmodule)
	{
		int offset = this.getFModuleOffset(frame) + fmodule;
		return this.fmodulesFlag[offset];
	}

	private int getAFrameX(int animation, int aframe)
	{
		int offset = this.getAFrameOffset(animation) + aframe;
		return this.aframesX[offset];
	}

	private int getAFrameY(int animation, int aframe)
	{
		int offset = this.getAFrameOffset(animation) + aframe;
		return this.aframesY[offset];
	}

	private int getAFrameFlag(int animation, int aframe)
	{
		int offset = this.getAFrameOffset(animation) + aframe;
		return this.aframesFlags[offset];
	}

	public void load(byte[] buf) throws IOException
	{
		ByteArrayInputStream stream = new ByteArrayInputStream(buf);
		DataInputStream input = new DataInputStream(stream);

		int length;
		byte[] data;

		//
		// Palettes
		//
		this.paletteCount = input.readByte();				// Palette count
		this.colorCount = input.readShort();				// Sprite image colors
		length = this.paletteCount * this.colorCount * PALETTE_PIXEL_SIZE_BYTE;
		data = new byte[length];
		input.read(data, 0, length);
		this.palettes = getPalette(data);

		//
		// Modules
		//
		this.pixelLength = input.readByte();				// Pixel format
		length = input.readInt();							// All modules image length
		length *= this.paletteCount;						// Store all palettes
		this.modulesRGB = new int[length];
		length = input.readShort();							// Modules count
		this.modulesWidth = new short[length];
		this.modulesHeight = new short[length];
		this.modulesRGBOffset = new int[length];

		for (int i = 0; i < modulesWidth.length; i++) {
			short width = input.readShort();				// Width
			short height = input.readShort();				// Height
			length = width * height;

			data = new byte[length * this.pixelLength];
			input.read(data, 0, length);					// Image data (bytes)

			this.modulesWidth[i] = width;
			this.modulesHeight[i] = height;

			for (int j = 0; j < this.paletteCount; j++) {
				int[] rgb;									// Image data (rgb)
				if (this.pixelLength == PIXEL_FORMAT_8BPP_INDEXED) {
					rgb = this.getRGB(data, this.palettes, this.colorCount, j);
				} else {
					rgb = getRGB(data, this.pixelLength);
				}

				System.arraycopy(rgb, 0, this.modulesRGB,
					this.modulesRGBOffset[i] + (this.modulesRGB.length / this.paletteCount * j), length);
			}

			if (i < this.modulesRGBOffset.length - 1) {		// RGB offsets
				this.modulesRGBOffset[i + 1] += this.modulesRGBOffset[i] + length;
			}
		}

		//
		// Frames
		//
		length = input.readShort();							// Frames count
		this.frames = new short[length];
		this.framesLength = new short[length];
		this.framesX = new short[length];
		this.framesY = new short[length];
		this.framesWidth = new short[length];
		this.framesHeight = new short[length];

		length = input.readShort();							// FModules count
		this.fmodules = new short[length];
		this.fmodulesX = new short[length];
		this.fmodulesY = new short[length];
		this.fmodulesFlag = new byte[length];
		for (int i = 0; i < this.frames.length; i++) {
			length = input.readShort();						// FModules count in the current frame
			this.framesLength[i] = (short)length;
			for (int j = 0; j < length; j++) {
				this.fmodules[this.frames[i] + j] = input.readShort();		// Module index
				this.fmodulesX[this.frames[i] + j] = input.readShort();		// X
				this.fmodulesY[this.frames[i] + j] = input.readShort();		// Y
				this.fmodulesFlag[this.frames[i] + j] = input.readByte();	// Flag
			}

			this.framesX[i] = input.readShort();			// X
			this.framesY[i] = input.readShort();			// Y
			this.framesWidth[i] = input.readShort();		// Width
			this.framesHeight[i] = input.readShort();		// Height

			if (i < this.frames.length - 1) {				// Frames offsets
				this.frames[i + 1] += this.frames[i] + length;
			}
		}

		//
		// Animations
		//
		length = input.readShort();							// Animations count
		this.animations = new short[length];
		this.animationsLength = new short[length];

		length = input.readShort();							// AFrames count
		this.aframes = new short[length];
		this.aframesX = new short[length];
		this.aframesY = new short[length];
		this.aframesTime = new byte[length];
		this.aframesFlags = new byte[length];
		for (int i = 0; i < this.animations.length; i++) {
			length = input.readShort();						// AFrames count in the current animation
			this.animationsLength[i] = (short)length;
			for (int j = 0; j < length; j++) {
				this.aframes[this.animations[i] + j] = input.readShort();		// Frame index
				this.aframesX[this.animations[i] + j] = input.readShort();		// X
				this.aframesY[this.animations[i] + j] = input.readShort();		// Y
				this.aframesTime[this.animations[i] + j] = input.readByte();	// Time
				this.aframesFlags[this.animations[i] + j] = input.readByte();	// Flag
			}

			if (i < this.animations.length - 1) {			// Frames offsets
				this.animations[i + 1] += this.animations[i] + length;
			}
		}

		input.close();
		stream.close();
	}

	/**
	 * Destructor for CSprite.
	 * Sometimes, set CSprite = null but system still keeps image.
	 * Call this method to release them.
	 */
	protected void finalize()
	{
		if (this.images != null) {
			for (int i = 0; i < this.images.length; i++) {
				if (this.images[i] != null) {
					this.images[i] = null;
				}
			}
		}
		// this.modulesRGB = null;
	}

	public void cache(int palette)
	{
		if (this.isCached) {
			for (int i = 0; i < this.getModuleCount(); i++) {
				int offset = this.getModuleOffset(i) + (this.modulesRGB.length / this.paletteCount * palette);
				int width = this.getModuleWidth(i);
				int height = this.getModuleHeight(i);
				int length = width * height;

				int[] rgb = new int[length];
				System.arraycopy(this.modulesRGB, offset, rgb, 0, length);
				this.images[i + (this.getModuleCount() * palette)] = Image.createRGBImage(rgb, width, height, true);
				rgb = null;
			}
		} else {
			PRINT("[ERROR] GameSprite: Please load sprite with param \"isCacheModules\" is true");
		}
	}

	public void cache()
	{
		this.cache(0);
	}

	public void uncacheRGB()
	{
		this.modulesRGB = null;
	}

	public int getPaletteCount()
	{
		return this.paletteCount;
	}

	public void setPalette(int palette)
	{
		if (palette > this.paletteCount - 1) {
			palette = 0;
		}

		this.palette = palette;
	}

	public int getPalette()
	{
		return this.palette;
	}

	public int getModuleId(int frame, int fmodule)
	{
		int offset = this.frames[frame];
		int moduleId = this.fmodules[offset + fmodule];
		return moduleId;
	}

	public int getFrameId(int animation, int aframe)
	{
		int offset = this.animations[animation];
		int frameId = this.aframes[offset + aframe];
		return frameId;
	}

	public int getModuleWidth(int module)
	{
		return this.modulesWidth[module];
	}

	public int getModuleHeight(int module)
	{
		return this.modulesHeight[module];
	}

	public int getModuleCount()
	{
		return this.modulesWidth.length;	// Or, modulesHeight.length
	}

	public int getFrameWidth(int frame)
	{
		return this.framesWidth[frame];
	}

	public int getFrameHeight(int frame)
	{
		return this.framesHeight[frame];
	}

	public int getFModuleCount(int frame)
	{
		return this.framesLength[frame];
	}

	public int getAFrameCount(int animation)
	{
		return this.animationsLength[animation];
	}

	public int getAFrameTime(int animation, int aframe)
	{
		int offset = this.getAFrameOffset(animation) + aframe;
		return this.aframesTime[offset];
	}

	public int getAnimationCount()
	{
		return this.animations.length;
	}

	public void getFrameRect(int rect[], int frame, int x, int y, int flags)
	{
		int frameX = this.framesX[frame];
		int frameY = this.framesY[frame];
		int frameWidth = this.framesWidth[frame];
		int frameHeight = this.framesHeight[frame];

		if ((flags & FLAG_FLIPX) != 0) {
			frameX = -frameX - frameWidth;
		}

		if ((flags & FLAG_FLIPY) != 0) {
			frameY = -frameY - frameHeight;
		}

		if ((flags & FLAG_ROTATE90) != 0) {
			int temp = frameX;
			frameX = -frameY - frameHeight;
			frameY = temp;

			temp = frameWidth;
			frameWidth = frameHeight;
			frameHeight = temp;
		}

		rect[0] = x + frameX;
		rect[1] = y + frameY;
		rect[2] = rect[0] + frameWidth;
		rect[3] = rect[1] + frameHeight;
	}

	public void getFrameRect(int rect[], int frame, int x, int y)
	{
		this.getFrameRect(rect, frame, x, y, FLAG_NONE);
	}

	public void getAFrameRect(int rect[], int animation, int aframe, int x, int y)
	{
		int frame = this.getFrameId(animation, aframe);
		int flags = this.getFModuleFlag(animation, aframe);
		this.getFrameRect(rect, frame, x, y, flags);
	}

	// TODO Check for flags mask: FLAG_ROTATE90
	public void paintModule(Graphics g, int module, int x, int y, int flags, int anchor)
	{
		if ((anchor & Graphics.HCENTER) != 0) {				// HCENTER
			x -= this.getModuleWidth(module) >> 1;
		} else if ((anchor & Graphics.RIGHT) != 0) {		// RIGHT
			x -= this.getModuleWidth(module);
		}

		if ((anchor & Graphics.VCENTER) != 0) {				// VCENTER
			y -= this.getModuleHeight(module) >> 1;
		} else if ((anchor & Graphics.BOTTOM) != 0) {
			y -= this.getModuleHeight(module);
		}

		if (this.isCached) {
			Image image = this.images[module + (this.images.length / this.paletteCount * this.palette)];
			if (flags == FLAG_NONE) {
				g.drawImage(image, x, y, 0);
			} else {
				g.drawRegion(image, 0, 0, image.getWidth(), image.getHeight(), flags, x, y, 0);
			}
		} else {
			int width = this.getModuleWidth(module);
			int height = this.getModuleHeight(module);
			int offset = this.getModuleRGBOffset(module) + (this.modulesRGB.length / this.paletteCount * this.palette);
			// TODO Support transform with using drawRGB method
			g.drawRGB(this.modulesRGB, offset, width, x, y, width, height, true);
		}
	}

	public void paintModule(Graphics g, int module, int x, int y, int anchor)
	{
		this.paintModule(g, module, x, y, FLAG_NONE, anchor);
	}

	public void paintFrame(Graphics g, int frame, int x, int y, int flags, int anchor)
	{
		if ((anchor & Graphics.HCENTER) != 0) {				// HCENTER
			x -= this.getFrameWidth(frame) >> 1;
		} else if ((anchor & Graphics.RIGHT) != 0) {		// RIGHT
			x -= this.getFrameWidth(frame);
		}

		if ((anchor & Graphics.VCENTER) != 0) {				// VCENTER
			y -= this.getFrameHeight(frame) >> 1;
		} else if ((anchor & Graphics.BOTTOM) != 0) {
			y -= this.getFrameHeight(frame);
		}

		for (int i = 0; i < this.framesLength[frame]; i++) {
			int module = this.getModuleId(frame, i);
			int ox = this.getFModuleX(frame, i) + x;
			int oy = this.getFModuleY(frame, i) + y;

			int fmoduleFlags = this.getFModuleFlag(frame, i);
			fmoduleFlags ^= flags;

			this.paintModule(g, module, ox, oy, fmoduleFlags, 0);
		}
	}

	public void paintFrame(Graphics g, int frame, int x, int y, int anchor)
	{
		this.paintFrame(g, frame, x, y, FLAG_NONE, anchor);
	}

	public void paintAFrame(Graphics g, int animation, int aframe, int x, int y, int flags, int anchor)
	{
		int ox = this.getAFrameX(animation, aframe);
		int oy = this.getAFrameY(animation, aframe);
		int frame = this.getFrameId(animation, aframe);
		flags ^= this.getAFrameFlag(animation, aframe);
		this.paintFrame(g, frame, x + ox, y + oy, flags, anchor);
	}

	public void paintAFrame(Graphics g, int animation, int aframe, int x, int y, int anchor)
	{
		this.paintAFrame(g, animation, aframe, x, y, FLAG_NONE, anchor);
	}

	public Image getFrameImage(int frame)
	{
		int width = this.getFrameWidth(frame);
		int height = this.getFrameHeight(frame);

		Image image = Image.createImage(width, height);
		Graphics g = image.getGraphics();

		this.paintFrame(g, frame, 0, 0, 0);
		g = null;

		return image;
	}
}
