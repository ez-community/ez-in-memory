#######################################
# set global properties for this build
#######################################

# set project properties
PROJECT_NAME=GameLib
VENDOR=EZ
VERSION=0.0.2

# set define file for CPP tool
DEFINES_FILE=_DEFINES.H

# set input/output paths
SRC_PATH=src
BUILD_PATH=.version
SRC_OUT_PATH=${BUILD_PATH}/1-src
PREPROCESS_PATH=${BUILD_PATH}/2-preprocess
CLASSES_OUT_PATH=${BUILD_PATH}/3-classes
RELEASE_PATH=.release
JAVADOCS_PATH=.docs

# set WTK properties
WTK_PATH=Y:/WTK2.5.2
MIDP_LIB=${WTK_PATH}/lib/midpapi20.jar
CLDC_LIB=${WTK_PATH}/lib/cldcapi11.jar
WTK_CLDC_VERSION=1.1
WTK_MIDP_VERSION=2.0

# set for tools
TOOLS_PATH=_tools
ANTENNA_LIB=${TOOLS_PATH}/antenna-bin-1.2.1-beta.jar
BB_ANT_LIB=${TOOLS_PATH}/bb-ant-tools-1.2.11-bin/bb-ant-tools.jar
TEXT_EXPORTER=${TOOLS_PATH}/text-exporter/text-exporter.jar
FONT_EXPORTER=${TOOLS_PATH}/font-exporter/font-exporter.jar
DATA-PACKAGE=${TOOLS_PATH}/data-package/data-package.jar
CPP_TOOL_PATH=${TOOLS_PATH}/cpp
PREPROCESS_SCRIPT=${TOOLS_PATH}/cpp/preprocess.bat
PROGUARD_PATH=${TOOLS_PATH}/proguard/proguard.jar

# BlackBerry API
BLACKBERRY_LIB=${TOOLS_PATH}/BlackBerry/net_rim_api.jar

# set antenna properties
wtk.home=${WTK_PATH}
wtk.cldc.version=${WTK_CLDC_VERSION}
wtk.midp.version=${WTK_MIDP_VERSION}
#wtk.proguard.home=${PROGUARD_PATH}

# set BlackBerry ant tool properties
# BlackBerry JDE Path
jde.home=Y:/BlackBerry/JDE_4.5.0
