using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace GameLib
{
	public class Sprite
	{
		//
		// Pixel format
		//
		public const int PIXEL_FORMAT_8BPP_INDEXED		= 1;
		public const int PIXEL_FORMAT_24BPP_RGB			= 3;
		public const int PIXEL_FORMAT_32BPP_ARGB		= 4;

		//
		// Texture
		//
		private int pixelLength;

		//
		// Module
		//
		private short[] modulesWidth;		// Module: width
		private short[] modulesHeight;		// Module: height
		private Texture2D[] textures;		// Module: image

		//
		// Frame
		//
		private short[] frames;				// Frames offset in fmodules
		private short[] framesLength;		// Frames length
		private short[] framesX;			// Frame X
		private short[] framesY;			// Frame Y
		private short[] framesWidth;		// Frame width
		private short[] framesHeight;		// Frame height

		//
		// FModule
		//
		private short[] fmodules;
		private short[] fmodulesX;
		private short[] fmodulesY;
		private byte[] fmodulesFlag;

		//
		// Animation
		//
		private short[] animations;			// Animations offset in aframes
		private short[] animationsLength;	// Animations length

		//
		// AFrame
		//
		private short[] aframes;			// AFrames offset in animations
		private short[] aframesX;			// AFrames X
		private short[] aframesY;			// AFrames Y
		private byte[] aframesTime;			// AFrames time
		private byte[] aframesFlags;		// AFrames Flag

		public Sprite()
		{
		}

		public Sprite(GraphicsDevice graphicsDevice, byte[] buf)
		{
			this.Load(graphicsDevice, buf);
		}

		private byte[] GetBGRA(byte[] buf)
		{
			int length = (buf.Length * PIXEL_FORMAT_32BPP_ARGB) / this.pixelLength;
			byte[] bgra = new byte[length];

			int pos = 0;
			for (int i = 0; i < buf.Length; i += this.pixelLength) {
				byte r = 0x0, g = 0x0, b = 0x0, a = 0xff;

				if (this.pixelLength == PIXEL_FORMAT_24BPP_RGB) {			// 24-bit image (RGB)
					r = buf[i];
					g = buf[i + 1];
					b = buf[i + 2];

					if (r == 0xff && g == 0x0 && b == 0xff) {				// Magenta -> Transparent
						a = 0x0;
					}
				} else if (this.pixelLength == PIXEL_FORMAT_32BPP_ARGB) {	// 32-bit image (RGBA)
					r = buf[i];
					g = buf[i + 1];
					b = buf[i + 2];
					a = buf[i + 3];
				}

				bgra[pos] = b;
				bgra[pos + 1] = g;
				bgra[pos + 2] = r;
				bgra[pos + 3] = a;

				pos += PIXEL_FORMAT_32BPP_ARGB;
			}

			return bgra;
		}

		private int GetAFrameOffset(int animation)
		{
			return this.animations[animation];
		}

		private int GetFModuleOffset(int frame)
		{
			return this.frames[frame];
		}

		private int GetFModuleX(int frame, int fmodule)
		{
			int offset = this.GetFModuleOffset(frame) + fmodule;
			return this.fmodulesX[offset];
		}

		private int GetFModuleY(int frame, int fmodule)
		{
			int offset = this.GetFModuleOffset(frame) + fmodule;
			return this.fmodulesY[offset];
		}

		private int GetFModuleFlag(int frame, int fmodule)
		{
			int offset = this.GetFModuleOffset(frame) + fmodule;
			return this.fmodulesFlag[offset];
		}

		private int GetAFrameX(int animation, int aframe)
		{
			int offset = this.GetAFrameOffset(animation) + aframe;
			return this.aframesX[offset];
		}

		private int GetAFrameY(int animation, int aframe)
		{
			int offset = this.GetAFrameOffset(animation) + aframe;
			return this.aframesY[offset];
		}

		private int GetAFrameFlag(int animation, int aframe)
		{
			int offset = this.GetAFrameOffset(animation) + aframe;
			return this.aframesFlags[offset];
		}

		/**
		 * Destructor for CSprite.
		 * Sometimes, set CSprite = null but system still keeps image.
		 * Call this method to release them.
		 */
		protected void Dispose()
		{
			if (this.textures != null) {
				for (int i = 0; i < this.textures.Length; i++) {
					if (this.textures[i] != null) {
						this.textures[i].Dispose();
						this.textures[i] = null;
					}
				}
			}
		}

		private void Load(GraphicsDevice graphicsDevice, byte[] buf)
		{
			Stream stream = new MemoryStream(buf);
			BinaryReader reader = new BinaryReader(stream);

			int length;
			byte[] data;

			//
			// Modules
			//
			this.pixelLength = reader.ReadByte();				// Pixel format (length)
			length = reader.ReadInt16();						// Modules count
			this.textures = new Texture2D[length];
			this.modulesWidth = new short[length];
			this.modulesHeight = new short[length];

			for (int i = 0; i < modulesWidth.Length; i++) {
				short width = reader.ReadInt16();				// Width
				short height = reader.ReadInt16();				// Height
				length = width * height * this.pixelLength;		// Length

				data = new byte[length];
				reader.Read(data, 0, length);					// Image data (bytes)
				this.textures[i] = new Texture2D(graphicsDevice, width, height, false, SurfaceFormat.Color);
				byte[] bgra = this.GetBGRA(data);
				this.textures[i].SetData(bgra);					// BGRA

				this.modulesWidth[i] = width;
				this.modulesHeight[i] = height;
			}

			//
			// Frames
			//
			length = reader.ReadInt16();							// Frames count
			this.frames = new short[length];
			this.framesLength = new short[length];
			this.framesX = new short[length];
			this.framesY = new short[length];
			this.framesWidth = new short[length];
			this.framesHeight = new short[length];

			length = reader.ReadInt16();							// FModules count
			this.fmodules = new short[length];
			this.fmodulesX = new short[length];
			this.fmodulesY = new short[length];
			this.fmodulesFlag = new byte[length];
			for (int i = 0; i < this.frames.Length; i++) {
				length = reader.ReadInt16();						// FModules count in the current frame
				this.framesLength[i] = (short)length;
				for (int j = 0; j < length; j++) {
					this.fmodules[this.frames[i] + j] = reader.ReadInt16();		// Module index
					this.fmodulesX[this.frames[i] + j] = reader.ReadInt16();	// X
					this.fmodulesY[this.frames[i] + j] = reader.ReadInt16();	// Y
					this.fmodulesFlag[this.frames[i] + j] = reader.ReadByte();	// Flag
				}

				this.framesX[i] = reader.ReadInt16();				// X
				this.framesY[i] = reader.ReadInt16();				// Y
				this.framesWidth[i] = reader.ReadInt16();			// Width
				this.framesHeight[i] = reader.ReadInt16();			// Height

				if (i < this.frames.Length - 1) {					// Frames offsets
					this.frames[i + 1] += (short)(this.frames[i] + length);
				}
			}

			//
			// Animations
			//
			length = reader.ReadInt16();							// Animations count
			this.animations = new short[length];
			this.animationsLength = new short[length];

			length = reader.ReadInt16();							// AFrames count
			this.aframes = new short[length];
			this.aframesX = new short[length];
			this.aframesY = new short[length];
			this.aframesTime = new byte[length];
			this.aframesFlags = new byte[length];
			for (int i = 0; i < this.animations.Length; i++) {
				length = reader.ReadInt16();						// AFrames count in the current animation
				this.animationsLength[i] = (short)length;
				for (int j = 0; j < length; j++) {
					this.aframes[this.animations[i] + j] = reader.ReadInt16();		// Frame index
					this.aframesX[this.animations[i] + j] = reader.ReadInt16();		// X
					this.aframesY[this.animations[i] + j] = reader.ReadInt16();		// Y
					this.aframesTime[this.animations[i] + j] = reader.ReadByte();	// Time
					this.aframesFlags[this.animations[i] + j] = reader.ReadByte();	// Flag
				}

				if (i < this.animations.Length - 1) {				// Frames offsets
					this.animations[i + 1] += (short)(this.animations[i] + length);
				}
			}

			reader.Dispose();
			stream.Dispose();
		}

		public Texture2D GetTexture(int module)
		{
			return this.textures[module];
		}

		public int GetModuleId(int frame, int fmodule)
		{
			int offset = this.frames[frame];
			int moduleId = this.fmodules[offset + fmodule];
			return moduleId;
		}

		public int GetFrameId(int animation, int aframe)
		{
			int offset = this.animations[animation];
			int frameId = this.aframes[offset + aframe];
			return frameId;
		}

		public int GetModuleWidth(int module)
		{
			return this.modulesWidth[module];
		}

		public int GetModuleHeight(int module)
		{
			return this.modulesHeight[module];
		}

		public int GetModuleCount()
		{
			return this.modulesWidth.Length;	// Or, modulesHeight.length
		}

		public int GetFrameWidth(int frame)
		{
			return this.framesWidth[frame];
		}

		public int GetFrameHeight(int frame)
		{
			return this.framesHeight[frame];
		}

		public int GetFModuleCount(int frame)
		{
			return this.framesLength[frame];
		}

		public int GetAFrameCount(int animation)
		{
			return this.animationsLength[animation];
		}

		public int GetAFrameTime(int animation, int aframe)
		{
			int offset = this.GetAFrameOffset(animation) + aframe;
			return this.aframesTime[offset];
		}

		public int GetAnimationCount()
		{
			return this.animations.Length;
		}

		public void GetFrameRect(int[] rect, int frame, int x, int y, int flags)
		{
			int frameX = this.framesX[frame];
			int frameY = this.framesY[frame];
			int frameWidth = this.framesWidth[frame];
			int frameHeight = this.framesHeight[frame];

			if ((flags & SpriteFlag.FLIPX) != 0) {
				frameX = -frameX - frameWidth;
			}

			if ((flags & SpriteFlag.FLIPY) != 0) {
				frameY = -frameY - frameHeight;
			}

			if ((flags & SpriteFlag.ROTATE90) != 0) {
				int temp = frameX;
				frameX = -frameY - frameHeight;
				frameY = temp;

				temp = frameWidth;
				frameWidth = frameHeight;
				frameHeight = temp;
			}

			rect[0] = x + frameX;
			rect[1] = y + frameY;
			rect[2] = rect[0] + frameWidth;
			rect[3] = rect[1] + frameHeight;
		}

		public void GetFrameRect(int[] rect, int frame, int x, int y)
		{
			this.GetFrameRect(rect, frame, x, y, SpriteFlag.NONE);
		}

		public void GetAFrameRect(int[] rect, int animation, int aframe, int x, int y)
		{
			int frame = this.GetFrameId(animation, aframe);
			int flags = this.GetFModuleFlag(animation, aframe);
			this.GetFrameRect(rect, frame, x, y, flags);
		}

		// TODO Check for flags mask: FLAG_ROTATE90
		public void DrawModule(SpriteBatch spriteBatch, int module, int x, int y, int flags, float rotation,  Vector2 origin,int anchor)
		{
			if ((anchor & SpriteAnchor.HCENTER) != 0) {				// HCENTER
				x -= this.GetModuleWidth(module) >> 1;
			} else if ((anchor & SpriteAnchor.RIGHT) != 0) {		// RIGHT
				x -= this.GetModuleWidth(module);
			}

			if ((anchor & SpriteAnchor.VCENTER) != 0) {				// VCENTER
				y -= this.GetModuleHeight(module) >> 1;
			} else if ((anchor & SpriteAnchor.BOTTOM) != 0) {		// BOTTOM
				y -= this.GetModuleHeight(module);
			}

			Texture2D texture = this.textures[module];
			Vector2 position = new Vector2(x, y);
			SpriteEffects effect = SpriteEffects.None;				// FlipX or FlipY

			// TODO Support more flags by implement rotation
			if (flags == SpriteFlag.FLIPX) {
				effect = SpriteEffects.FlipHorizontally;
			} else if (flags == SpriteFlag.FLIPY) {
				effect = SpriteEffects.FlipVertically;
			}

            spriteBatch.Draw(texture, position, null, Color.White, rotation, origin, 1, effect, 0);
		}

		public void DrawModule(SpriteBatch spriteBatch, int module, int x, int y, int flags, int anchor)
		{
			this.DrawModule(spriteBatch, module, x, y, flags, 0, Vector2.Zero, anchor);
		}

		public void DrawModule(SpriteBatch spriteBatch, int module, int x, int y, int anchor)
		{
			this.DrawModule(spriteBatch, module, x, y, SpriteFlag.NONE, 0, Vector2.Zero, anchor);
		}

        public void DrawFrame(SpriteBatch spriteBatch, int frame, int x, int y, int flags, float rotation, Vector2 origin, int anchor)
		{
			if ((anchor & SpriteAnchor.HCENTER) != 0) {				// HCENTER
				x -= this.GetFrameWidth(frame) >> 1;
			} else if ((anchor & SpriteAnchor.RIGHT) != 0) {		// RIGHT
				x -= this.GetFrameWidth(frame);
			}

			if ((anchor & SpriteAnchor.VCENTER) != 0) {				// VCENTER
				y -= this.GetFrameHeight(frame) >> 1;
			} else if ((anchor & SpriteAnchor.BOTTOM) != 0) {		// BOTTOM
				y -= this.GetFrameHeight(frame);
			}

			for (int i = 0; i < this.framesLength[frame]; i++) {
				int module = this.GetModuleId(frame, i);
				int ox = this.GetFModuleX(frame, i) + x;
				int oy = this.GetFModuleY(frame, i) + y;

				int fmoduleFlags = this.GetFModuleFlag(frame, i);
				fmoduleFlags ^= flags;

				this.DrawModule(spriteBatch, module, ox, oy, fmoduleFlags, rotation, origin, 0);
			}
		}

		public void DrawFrame(SpriteBatch spriteBatch, int frame, int x, int y, int flags, int anchor)
		{
			this.DrawFrame(spriteBatch, frame, x, y, flags, 0, Vector2.Zero, anchor);
		}

		public void DrawFrame(SpriteBatch spriteBatch, int frame, int x, int y, int anchor)
		{
			this.DrawFrame(spriteBatch, frame, x, y, SpriteFlag.NONE, 0, Vector2.Zero, anchor);
		}

        public void DrawAFrame(SpriteBatch spriteBatch, int animation, int aframe, int x, int y, int flags, float rotation, Vector2 origin, int anchor)
		{
			int ox = this.GetAFrameX(animation, aframe);
			int oy = this.GetAFrameY(animation, aframe);
			int frame = this.GetFrameId(animation, aframe);
			flags ^= this.GetAFrameFlag(animation, aframe);
			this.DrawFrame(spriteBatch, frame, x + ox, y + oy, flags, rotation, origin, anchor);
		}

		public void DrawAFrame(SpriteBatch spriteBatch, int animation, int aframe, int x, int y, int anchor)
		{
			this.DrawAFrame(spriteBatch, animation, aframe, x, y, SpriteFlag.NONE, 0, Vector2.Zero, anchor);
		}

		//
		// Graphics methods
		//
		//public static void FillRect(SpriteBatch spriteBatch, Color color, int x, int y, int width, int height)
		//{
		//    Texture2D texture = new Texture2D(GetGraphics(), 1, 1);
		//    texture.SetData(new Color[] {color});
		//    Rectangle rect = new Rectangle(x, y, width, height);

		//    spriteBatch.Draw(texture, rect, Color.White);
		//}
	}
}
