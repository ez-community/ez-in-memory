﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace GameLib
{
	public partial class Game : Microsoft.Xna.Framework.Game
	{
		#if WINDOWS		//Using mouse/keyboard
		protected static KeyboardState prevKeyboard;
		protected static KeyboardState keyboard;
		protected static MouseState prevMouse;
		protected static MouseState mouse;
		#endif	//WINDOWS

		#if WINDOWS_PHONE		//Using multiple touch on WP
		protected static TouchCollection touchCollection;		// All points
		#endif

		//
		// Keyboard
		//
		public static bool IsKeyPressed(Keys key)
		{
			#if WINDOWS
			return (prevKeyboard.IsKeyUp(key) && keyboard.IsKeyDown(key));
			#endif

			#if WINDOWS_PHONE
			return false;
			#endif
		}

		public static bool IsKeyHold(Keys key)
		{
			#if WINDOWS
			return (prevKeyboard.IsKeyDown(key) && keyboard.IsKeyDown(key));
			#endif

			#if WINDOWS_PHONE
			return false;
			#endif
		}

		//
		// Mouse/Touch function
		//
		public static bool IsPointInRect(int x, int y, int x1, int y1, int x2, int y2)
		{
			return (x > x1 && y > y1 && x < x2 && y < y2);
		}

		public static bool IsPointInRect(Vector2 point, int x1, int y1, int x2, int y2)
		{
			return (point.X > x1 && point.Y > y1 && point.X < x2 && point.Y < y2);
		}

		#if WINDOWS_PHONE
        public static bool IsPointInRect(int x1, int y1, int x2, int y2)
        {
            foreach (TouchLocation tl in touchCollection)
            {
                if ((tl.State == TouchLocationState.Pressed)
                        || (tl.State == TouchLocationState.Moved))
                {
                    if (tl.Position.X > x1 && tl.Position.Y > y1 && tl.Position.X < x2 && tl.Position.Y < y2)
                        return true;
                }
            }
            return false;
        }
		#endif

		public static bool IsTap(int x1, int y1, int x2, int y2)
		{
			#if WINDOWS
			return (
				prevMouse.LeftButton == ButtonState.Released
				&& mouse.LeftButton == ButtonState.Pressed
				&& IsPointInRect(GetPointerX(), GetPointerY(), x1, y1, x2, y2)
				);
			#endif

			#if WINDOWS_PHONE
			// get any gestures that are ready
			// GestureSample in a queue, call IsTap method is right at the first time only!
			// TODO Recheck to use GestureSample
			//while (TouchPanel.IsGestureAvailable)
			//{
			//    GestureSample gs = TouchPanel.ReadGesture();
			//    if (gs.GestureType == GestureType.Tap && IsPointInRect(gs.Position, x1, y1, x2, y2)) {
			//        return true;
			//    }
			//}

			foreach (TouchLocation tl in touchCollection) {
				if (tl.State == TouchLocationState.Pressed) {
					return IsPointInRect(tl.Position, x1, y1, x2, y2);
				}
			}

			return false;
			#endif
		}

		public static bool IsHold(int x1, int y1, int x2, int y2)
		{
			#if WINDOWS
			return (
				prevMouse.LeftButton == ButtonState.Pressed
				&& mouse.LeftButton == ButtonState.Pressed
				&& IsPointInRect(GetPointerX(), GetPointerY(), x1, y1, x2, y2)
			);
			#elif WINDOWS_PHONE
            foreach (TouchLocation tl in touchCollection)
            {
                if (
                        ((tl.State == TouchLocationState.Pressed) || (tl.State == TouchLocationState.Moved))
                        && IsPointInRect((int)(tl.Position.X), (int)(tl.Position.Y), x1, y1, x2, y2)
                   )
                    return true;
            }
			#endif
		}

		//public static bool IsDragged(int x1, int y1, int x2, int y2)
		//{
		//    if (IsPointerHold(x1, y1, x2, y2)) {
		//        int dx = mouse.X - prevMouse.X;
		//        int dy = mouse.Y - prevMouse.Y;

		//        return  (
		//            prevMouse != mouse
		//            && prevMouse.LeftButton == ButtonState.Pressed
		//            && mouse.LeftButton == ButtonState.Pressed
		//            && dx != 0
		//            && dy != 0
		//        );
		//    }

		//    return false;
		//}

		public static int GetPointerX()
		{
			#if WINDOWS
			return mouse.X;
			#elif WINDOWS_PHONE
            foreach (TouchLocation tl in touchCollection)
                return (int)(tl.Position.X);
			#endif
		}

		public static int GetPointerY()
		{
			#if WINDOWS
			return mouse.Y;
			#elif WINDOWS_PHONE
            foreach (TouchLocation tl in touchCollection)
                return (int)(tl.Position.Y);
			#endif
		}

		#if WINDOWS_PHONE
        public static int GetPointerX(TouchLocation tl)
        {
            if ((tl.State == TouchLocationState.Pressed)
                    || (tl.State == TouchLocationState.Moved))
            {
                return (int)(tl.Position.Y);
            }
            return -1;
        }
        public static int GetPointerY(TouchLocation tl)
        {
            if ((tl.State == TouchLocationState.Pressed)
                    || (tl.State == TouchLocationState.Moved))
            {
                return (int)(tl.Position.X);
            }
            return -1;
        }
		#endif
	}
}
