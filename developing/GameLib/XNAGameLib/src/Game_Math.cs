﻿using System;
using Microsoft.Xna.Framework;

namespace GameLib
{
	public partial class Game : Microsoft.Xna.Framework.Game
	{
		public const float MATH_PI = MathHelper.Pi;

		private static Random random;

		//
		// Random
		//
		public static void InitializeRandom()
		{
			random = new Random((int) frameCounter);
		}

		public static int GetRandomInt(int min, int max)
		{
			return random.Next(min, max);
		}

		public static int GetRandomInt(int min, int max, int exception)
		{
			int number = random.Next(min, max);
			while (number == exception) {
				number = random.Next(min, max);
			}

			return number;
		}

		public static double GetRandomDouble()
		{
			return random.NextDouble();
		}
	}
}
