
namespace GameLib
{
	public static class GameLibConfig
	{
		// Debug mode
		public static bool debug = true;

		// Screen width & screen height
		public static bool useFullScreen = false;		// Set true if want using full screen
		public static bool setScreenSize = false;	// Set true if want define screen size
		public static int screenWidth = 800;
		public static int screenHeight = 480;

		// Content
		public static string contentRootDirectory = "Content";

		// Mouse
		//#if WINDOWS
		public static bool visibleMouse = true;
		//#endif

		public static bool useSleepAfterEachFrame = true;
		public static int sleepTime = 20;

		// Sound
		public static bool useCachedPlayers = true;
		public static bool usePrefetchedPlayers = true;		// Set true when useCachedPlayers only (call realize() & prefetch() while cache)

		// Software double buffering
		public static bool useDoubleBuffering = false;

		// Transition effects
		public static bool useTransitionEffect = true;		// Use transition effect (set true in template file to avoid removed code while optimize)
		public static int transitionSpeed = 20;		// Speed, should be a even number

		// Use circular buffer for inprove FPS
		public static bool useCircularBuffer = true;		// Paint all tilesets to buffer.
	}
}
