
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Package some files into a single file.
 * @see FilePackage
 *
 * @author Thanh Vinh
 * @date Jan 05, 2012
 * @note Support compress file since version 0.0.4
 */
public class DataPackage
{
	private static final String INTRO		 			= "DataPackage version 0.0.5";
	private static final String COPYRIGHT	 			= "Copyright 2011 EZ Team <ezgroup@groups.live.com>";
	private static final int ARG_COUNT					= 5;

	private static final String TEXT_ENCODING 			= "UTF-8";
	private static final String FILE_SETS_SEPARATOR 	= ",";
	private static final String PATH_SEPARATED 			= "\\";

	private static final String NODE_PACKAGES 			= "packages";
	private static final String NODE_PACKAGE 			= "package";
	private static final String NODE_FILE	 			= "file";
	private static final String ATTRIB_NAME	 			= "name";
	private static final String ATTRIB_FILE	 			= "file";
	private static final String ATTRIB_COMPRESS			= "compress";

	private Vector<FilePackage> filePackages;

	public DataPackage(String platform, String configFile, String dataInputPath, 
		String dataOutPath, String sourceOutFile)
	{
		this.filePackages = new Vector<FilePackage>();

		//
		// Parse XML
		//
		try {
			File file = new File(configFile);
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = db.parse(file);
			doc.getDocumentElement().normalize();

			//
			// Package
			//
			NodeList packageNodes = doc.getElementsByTagName(NODE_PACKAGE);
			for (int i = 0; i < packageNodes.getLength(); i++) {
				Node packageNode = packageNodes.item(i);
				Element element = (Element) packageNode;
				// System.out.println("name : " + element.getAttribute(ATTRIB_NAME));
				// System.out.println("file : " + element.getAttribute(ATTRIB_FILE));

				String packageName = element.getAttribute(ATTRIB_NAME);
				String packageFile = element.getAttribute(ATTRIB_FILE);
				packageFile = dataOutPath + PATH_SEPARATED + packageFile;
				String compress = element.getAttribute(ATTRIB_COMPRESS);
				boolean useGZip = compress.equals("true");

				Vector<String> names = new Vector<String>();
				Vector<String> fileNames = new Vector<String>();

				if (packageNode.getNodeType() == Node.ELEMENT_NODE) {
					element = (Element) packageNode;
					NodeList fileNodes = element.getElementsByTagName(NODE_FILE);
					for (int j = 0; j < fileNodes.getLength(); j++) {
						Node fileNode = fileNodes.item(j);
						element = (Element) fileNode;
						// System.out.println("name : " + element.getAttribute(ATTRIB_NAME));
						// System.out.println("fileName : " + element.getAttribute(ATTRIB_FILE));
						String name = element.getAttribute(ATTRIB_NAME);
						String fileName = element.getAttribute(ATTRIB_FILE);
						fileName = dataInputPath + PATH_SEPARATED + fileName;

						names.add(name);
						fileNames.add(fileName);
					}
				}

				String[] inputNames = new String[names.size()];
				String[] inputFiles = new String[fileNames.size()];

				FilePackage filePackage = new FilePackage(packageName, packageFile, useGZip,
					sourceOutFile, names.toArray(inputNames), fileNames.toArray(inputFiles));
				filePackage.setPlatform(platform);
				this.filePackages.add(filePackage);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void exportPackage() throws IOException
	{
		for (int i = 0; i < this.filePackages.size(); i++) {
			FilePackage filePackage = this.filePackages.get(i);
			filePackage.writePackage();
		}
	}

	public void exportSource() throws IOException
	{
		for (int i = 0; i < this.filePackages.size(); i++) {
			FilePackage filePackage = this.filePackages.get(i);
			boolean append = (i == 0) ? false : true;
			filePackage.writeSource(append);
		}
	}

	public static void main(String args[])
	{
		System.out.println(INTRO);
		System.out.println(COPYRIGHT);

		if (args.length >= ARG_COUNT) {
			String platform = args[0];
			String configFile = args[1];
			String dataInputPath = args[2];
			String dataOutPath = args[3];
			String sourceOutFile = args[4];

			try
			{
				DataPackage dataPackage = new DataPackage(platform, configFile, dataInputPath, dataOutPath, sourceOutFile);

				System.out.println("Platform: " + platform);
				System.out.println("Configuration file: " + configFile);
				dataPackage.exportPackage();
				dataPackage.exportSource();
				System.out.println("Package complete...");
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			System.out.println("Command: DataPackage configuation-file data-input-path data-output-path source-output-file");
			System.out.println("Example: DataPackage template.mak Z:/images Z:/data Z:/source/src.h");
		}
	}
}
