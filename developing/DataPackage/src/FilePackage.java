
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.zip.GZIPOutputStream;

/**
 * File structure:
 * 	<Compress type> <Package Size> <Files count> <File 1 start offset> <File 1 end offset> .... <Data>
 * Max file size: 2^31 bytes (2,147,483,648 bytes)
 *
 * @author Thanh Vinh
 */
public class FilePackage
{
	public static final String PLATFORM_J2ME		= "J2ME";
	public static final String PLATFORM_XNA			= "XNA";

	private static final int BYTE_SIZE	 			= 1;					// 1 byte
	private static final int INT16_SIZE 			= 2;					// 2 bytes
	private static final int INT32_SIZE 			= 4;					// 4 bytes
	private static final int MAX_FILE_SIZE 			= Integer.MAX_VALUE; 	// Max file size: 2^31 bytes (2,147,483,648 bytes)
	private static final String TEXT_ENCODING 		= "UTF-8";
	private static final String DUMMY_FILE_NAME 	= "dummy";
	private static final byte COMPRESS_TYPE_NONE 	= 0;
	private static final byte COMPRESS_TYPE_GZIP 	= 1;

	private String platform = PLATFORM_J2ME;
	private String packageName;
	private String packageOutFile;
	private boolean useGZip;
	private String sourceOutFile;
	private String[] names;
	private String[] fileNames;

	// For reading data package
	private String packageFile;
	private int size;			// Package size
	private int filesCount;		// Total files in this package
	private int[] offsets;		// File offsets
	private int[] lengths;		// File length

	public FilePackage(String packageName, String packageOutFile, boolean useGZip,
		String sourceOutFile, String[] names, String[] fileNames) throws IOException
	{
		this.packageName = packageName;
		this.packageOutFile = packageOutFile;
		this.useGZip = useGZip;
		this.sourceOutFile = sourceOutFile;
		this.names = names;
		this.fileNames = fileNames;

		this.filesCount = this.names.length;
	}

	public void setPlatform(String platform)
	{
		this.platform = platform;
	}

	private byte[] read(String fileName) throws IOException
	{
		byte[] buf;

		File file = new File(fileName);
		if (file.exists()) {					// File correct
			int length = (int) file.length();	// File length
			DataInputStream reader = new DataInputStream(new FileInputStream(fileName));
			buf = new byte[length];

			reader.read(buf, 0, length);		// Read bytes from input file
			reader.close();

			if (this.useGZip) {					// Compress this file by using GZip
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				GZIPOutputStream writer = new GZIPOutputStream(stream);
				writer.write(buf, 0, length);
				writer.finish();

				buf = null;
				buf = stream.toByteArray();

				writer.close();					// Close stream
				stream.close();
			}
		} else {								// File not found (not dummy file)
			if (fileName.indexOf(DUMMY_FILE_NAME, 0) == -1) {
				System.out.println("Error: cannot find file: " + fileName);
			}

			buf = new byte[0];					// Write 0 byte if file not found or dummy file
		}

		return buf;
	}

	private byte[][] readAllFiles(String[] fileNames) throws IOException
	{
		byte[][] buf = new byte[this.filesCount][];

		for (int i = 0; i < this.filesCount; i++) {
			buf[i] = this.read(fileNames[i]);
		}

		return buf;
	}

	public void writePackage() throws IOException
	{
		// Read the information and store this information to write in the future
		byte[][] buf = this.readAllFiles(this.fileNames);
		DataOutputStreamEx writer = new DataOutputStreamEx(new FileOutputStream(this.packageOutFile));

		// Package size
		int size = INT32_SIZE * 2 + this.filesCount * INT32_SIZE;	// Package header size
		for (int i = 0; i < this.filesCount; i++) {
				size += buf[i].length;
		}

		// Use Little Endian for XNA platform
		boolean isReverseBytes = (this.platform.equals(PLATFORM_XNA) ? true : false);

		// Package header
		writer.writeByte(this.useGZip ? COMPRESS_TYPE_GZIP : COMPRESS_TYPE_NONE); // Compress method
		writer.writeInt(size, isReverseBytes);							// Package size
		writer.writeShort((short) this.filesCount, isReverseBytes);		// File count

		// Package header size
		int headerSize = BYTE_SIZE + INT32_SIZE + INT16_SIZE;			// Compress type, package size, file count
		headerSize += (this.filesCount * INT32_SIZE);					// All file offsets

		//
		// Item offsets
		//
		// For the first file: equals package header size
		int offset = headerSize;
		writer.writeInt(offset, isReverseBytes);
		// Begin at next file to end
		for (int i = 1; i < this.filesCount; i++) {
			offset += buf[i - 1].length;
			// System.out.println("[Write] File " + (i + 1) + " offset: " + offset);
			writer.writeInt(offset, isReverseBytes);
		}

		// Write files data
		for (int i = 0; i < this.filesCount; i++) {
			// System.out.println("[Write] File " + i + " size: " + data[i].length);
			writer.write(buf[i], 0, buf[i].length);
		}

		writer.close();
		buf = null;
	}

	public void writeSource(boolean append) throws IOException
	{
		StringBuffer buffer = new StringBuffer();
		buffer.append("\n\t// Generate by Data package\n");
		buffer.append("\t// Platform: ");
		buffer.append(this.platform);
		buffer.append("\n");

		// Use Java source for J2ME platform, else C++/C# source
		boolean isJavaSource = (this.platform.equals(PLATFORM_XNA) ? false : true);

		// Package name
		String fileName = new File(this.packageOutFile).getName();

		if (isJavaSource) {
			buffer.append("\tpublic static final String PACK_");
		} else {
			buffer.append("\tpublic const string PACK_");
		}
		buffer.append(this.packageName.toUpperCase());
		buffer.append(" = \"");
		buffer.append(fileName);
		buffer.append("\";\n");

		// Files index
		for (int i = 0; i < this.filesCount; i++) {
			String name = this.names[i];

			if (isJavaSource) {
				buffer.append("\tpublic static final int ");
			} else {
				buffer.append("\tpublic const int ");
			}
			buffer.append(this.packageName.toUpperCase());
			buffer.append("_");
			buffer.append(name.toUpperCase());
			buffer.append(" = ");
			buffer.append(i);
			buffer.append(";\n");
		}

		// File count
		if (isJavaSource) {
			buffer.append("\tpublic static final int ");
		} else {
			buffer.append("\tpublic const int ");
		}
		buffer.append(this.packageName.toUpperCase());
		buffer.append("_MAX = ");
		buffer.append(this.filesCount);
		buffer.append(";\n");

		//
		// Save to file
		//
		fileName = this.sourceOutFile;
		BufferedWriter writer = new BufferedWriter(
			new OutputStreamWriter(new FileOutputStream(fileName, append), TEXT_ENCODING));

		writer.write(buffer.toString());
		writer.close();
	}
}
