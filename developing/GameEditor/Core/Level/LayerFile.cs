﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using Core.Templates;

/*
 * File format:
 *	<layers name="Layers" template="template.gts">
 *		<layer name="Layer1" id="1">
 *			<actor name="Actor1" template="1" id="1" x="0" y="0" width="0" height="0" properties="1 2 3" />
 *			<actor name="Actor2" template="1" id="2" x="0" y="0" width="0" height="0" properties="1 2 3" />
 *		</layer>
 *	</layers>
 */

namespace Core.Level
{
	public class LayerFile
	{
		public const string NODE_LAYERS					= "layers";
		public const string NODE_CAMERA					= "camera";
		public const string NODE_LAYER					= "layer";
		public const string NODE_ACTOR					= "actor";
		public const string ATTRIBUTE_ID				= "id";
		public const string ATTRIBUTE_NAME				= "name";
		public const string ATTRIBUTE_FILE				= "file";
		public const string ATTRIBUTE_X					= "x";
		public const string ATTRIBUTE_Y					= "y";
		public const string ATTRIBUTE_WIDTH				= "width";
		public const string ATTRIBUTE_HEIGHT			= "height";
		public const string ATTRIBUTE_PROPERTIES		= "properties";
		public const string ATTRIBUTE_TEMPLATE			= "template";

		//
		// Properties
		//
		public string FileName { get; set; }
		public string TemplateFileName { get; set; }
		public ListLayer Layers { get; set; }
		public ListTemplate Templates { get; set; }
		public ByteOrder ByteOrder { get; set; }
		public Language Language { get; set; }
		public bool IsNewFile { get; set; }

		#region Static
		public static void New(string fileName, string templateFileName)
		{
			XmlWriterSettings settings = new XmlWriterSettings();
			settings.Indent = true;
			settings.IndentChars = "\t";
			settings.Encoding = Encoding.UTF8;
			XmlWriter writer = XmlWriter.Create(fileName, settings);
			writer.WriteStartElement(NODE_LAYERS);	// Layers (root)
			writer.WriteAttributeString(ATTRIBUTE_TEMPLATE, Path.GetFileName(templateFileName));
			writer.WriteEndElement();
			writer.Close();
		}
		#endregion

		public LayerFile(string inputFile)
		{
			this.FileName = inputFile;
			this.Layers = new ListLayer();
			this.IsNewFile = false;
		}
		
		#region Read/write
		public void Read()
		{
			XmlReader reader = XmlReader.Create(this.FileName);

			while (reader.Read())
			{
				if (reader.IsStartElement())    // Only dectect start elements
				{
					if (reader.NodeType == XmlNodeType.Element && reader.Name == NODE_LAYERS)
					{
						string name = reader.GetAttribute(ATTRIBUTE_NAME);
						this.TemplateFileName = Path.GetDirectoryName(this.FileName) + Path.DirectorySeparatorChar + reader.GetAttribute(ATTRIBUTE_TEMPLATE);

						this.Layers.Name = name;
						if (this.TemplateFileName != null)
						{
							TemplateFile templateFile = new TemplateFile(this.TemplateFileName);
							this.Templates = templateFile.Templates;
						}
					}
					else if (reader.NodeType == XmlNodeType.Element && reader.Name == NODE_CAMERA)
					{
						string name = reader.GetAttribute(ATTRIBUTE_NAME);
						int templateId = Int16.Parse(reader.GetAttribute(ATTRIBUTE_TEMPLATE));
						int id = Int16.Parse(reader.GetAttribute(ATTRIBUTE_ID));
						int x = Int16.Parse(reader.GetAttribute(ATTRIBUTE_X));
						int y = Int16.Parse(reader.GetAttribute(ATTRIBUTE_Y));
						int width = Int16.Parse(reader.GetAttribute(ATTRIBUTE_WIDTH));
						int height = Int16.Parse(reader.GetAttribute(ATTRIBUTE_HEIGHT));
						string properties = reader.GetAttribute(ATTRIBUTE_PROPERTIES);

						Template template = this.Templates[templateId];
						Actor actor = new Actor(id, template, x, y, width, height);
						this.Layers.Camera = actor;
					}
					else if (reader.NodeType == XmlNodeType.Element && reader.Name == NODE_LAYER)
					{
						string name = reader.GetAttribute(ATTRIBUTE_NAME);
						int id = Int16.Parse(reader.GetAttribute(ATTRIBUTE_ID));

						Layer layer = new Layer(id, name);
						this.Layers.Items.Add(layer);
					}
					else if (reader.NodeType == XmlNodeType.Element && reader.Name == NODE_ACTOR)
					{
						string name = reader.GetAttribute(ATTRIBUTE_NAME);
						int templateId = Int16.Parse(reader.GetAttribute(ATTRIBUTE_TEMPLATE));
						int id = Int16.Parse(reader.GetAttribute(ATTRIBUTE_ID));
						int x = Int16.Parse(reader.GetAttribute(ATTRIBUTE_X));
						int y = Int16.Parse(reader.GetAttribute(ATTRIBUTE_Y));
						int width = Int16.Parse(reader.GetAttribute(ATTRIBUTE_WIDTH));
						int height = Int16.Parse(reader.GetAttribute(ATTRIBUTE_HEIGHT));
						string properties = reader.GetAttribute(ATTRIBUTE_PROPERTIES);

						Template template = this.Templates[templateId];
						Actor actor = new Actor(id, template, x, y, width, height);
						Layer layer = this.Layers.Items[this.Layers.Count - 1];
						layer.Items.Add(actor);
					}
				}
			}
			reader.Close();
		}

		public void Save(string fileName)
		{
			XmlWriterSettings settings = new XmlWriterSettings();
			settings.Indent = true;
			settings.IndentChars = "\t";
			settings.Encoding = Encoding.UTF8;
			XmlWriter writer = XmlWriter.Create(fileName, settings);

			writer.WriteStartElement(NODE_LAYERS);									// Layers (root)
			writer.WriteAttributeString(ATTRIBUTE_NAME, this.Layers.Name);
			writer.WriteAttributeString(ATTRIBUTE_TEMPLATE, this.Templates.Name + ".gts");

			writer.WriteStartElement(NODE_CAMERA);									// Camera

			Actor actor = this.Layers.Camera;
			writer.WriteAttributeString(ATTRIBUTE_NAME, actor.Name);
			writer.WriteAttributeString(ATTRIBUTE_TEMPLATE, actor.Template.ID.ToString());
			writer.WriteAttributeString(ATTRIBUTE_ID, actor.ID.ToString());
			writer.WriteAttributeString(ATTRIBUTE_X, actor.X.ToString());
			writer.WriteAttributeString(ATTRIBUTE_Y, actor.Y.ToString());
			writer.WriteAttributeString(ATTRIBUTE_WIDTH, actor.Width.ToString());
			writer.WriteAttributeString(ATTRIBUTE_HEIGHT, actor.Height.ToString());
			writer.WriteAttributeString(ATTRIBUTE_PROPERTIES, actor.GetParameters());

			for (int i = 0; i < this.Layers.Count; i++)								// Loop all layers
			{
				Layer layer = this.Layers[i];

				writer.WriteStartElement(NODE_LAYER);								// Layer
				writer.WriteAttributeString(ATTRIBUTE_NAME, layer.Name);			// Name
				writer.WriteAttributeString(ATTRIBUTE_ID, layer.ID.ToString());		// ID

				for (int j = 0; j < layer.Items.Count; j++)
				{
					actor = layer.Items[j];

					writer.WriteStartElement(NODE_ACTOR);							// Actor

					writer.WriteAttributeString(ATTRIBUTE_NAME, actor.Name);
					writer.WriteAttributeString(ATTRIBUTE_TEMPLATE, actor.Template.ID.ToString());
					writer.WriteAttributeString(ATTRIBUTE_ID, actor.ID.ToString());
					writer.WriteAttributeString(ATTRIBUTE_X, actor.X.ToString());
					writer.WriteAttributeString(ATTRIBUTE_Y, actor.Y.ToString());
					writer.WriteAttributeString(ATTRIBUTE_WIDTH, actor.Width.ToString());
					writer.WriteAttributeString(ATTRIBUTE_HEIGHT, actor.Height.ToString());
					writer.WriteAttributeString(ATTRIBUTE_PROPERTIES, actor.GetParameters());

					writer.WriteEndElement();										// End Actor
				}
				writer.WriteEndElement();											// End Layer
			}

			writer.WriteEndElement();												// End Game (root)

			writer.Close();
		}

		public void Save()
		{
			this.Save(this.FileName);
		}
		#endregion

		#region Export
		/// <summary>
		/// Save source file.
		/// </summary>
		public void SaveSource(string fileName)
		{
			string name = this.Layers.Name.ToUpper();
			//
			// StringBuilder
			//
			StringBuilder builder = new StringBuilder();
			builder.Append("\n\t// Generate by Level Editor");
			builder.Append("\n\t//\n\t// ");
			builder.Append(name);
			builder.Append("\n\t//\n");

			//
			// Layer ID
			//
			name += "_";
			for (int i = 0; i < this.Layers.Count; i++)
			{
				if (this.Language == Language.Java)
				{
					builder.Append("\tpublic static final int ");
				}
				else
				{
					builder.Append("\tpublic const int ");
				}
				builder.Append(name);
				builder.Append(this.Layers[i].Name.ToUpper());
				builder.Append(" = ");
				builder.Append(i);
				builder.Append(";\n");
			}

			//
			// Save
			//
			StreamWriter writer = new StreamWriter(fileName, false);	// Always overwriter
			writer.Write(builder.ToString());
			writer.Close();
		}
		#endregion
	}
}
