﻿using System;
using System.IO;
using System.Text;
using System.Xml;

/*
 * File format:
 * <templates>
 * 		<template id="1" name="Template_1" sprite="menu.sprite" frame="-1" animation="1">
 *			<properties>
 *				<property name="Sprite" default="-1" description="" />
 *			</properties>
 *		</template>
 * 		<template id="2" name="Template_2">
 *		</template>
 * </templates>
 */

namespace Core.Templates
{
	public class TemplateFile
	{
		public const string NODE_TEMPLATES				= "templates";
		public const string NODE_TEMPLATE				= "template";
		public const string NODE_PROPERTIES				= "properties";
		public const string NODE_PROPERTY				= "property";
		public const string ATTRIBUTE_ID				= "id";
		public const string ATTRIBUTE_NAME				= "name";
		public const string ATTRIBUTE_VALUE				= "value";
		public const string ATTRIBUTE_DEFAULT			= "value";
		public const string ATTRIBUTE_DESCRIPTION		= "description";
		public const string ATTRIBUTE_SPRITE			= "sprite";
		public const string ATTRIBUTE_ANIMATION			= "animation";
		public const string ATTRIBUTE_FRAME				= "frame";

		//
		// Properties
		//
		public string FileName { get; set; }
		public ListTemplate Templates { get; set; }
		public ByteOrder ByteOrder { get; set; }
		public Language Language { get; set; }
		public bool IsNewFile { get; set; }

		#region Static
		public static void New(string fileName)
		{
			XmlWriterSettings settings = new XmlWriterSettings();
			settings.Indent = true;
			settings.IndentChars = "\t";
			settings.Encoding = Encoding.UTF8;
			XmlWriter writer = XmlWriter.Create(fileName, settings);
			writer.WriteStartElement(NODE_TEMPLATES);	// Templates (root)
			writer.WriteEndElement();
			writer.Close();
		}
		#endregion

		public TemplateFile(string inputFile)
		{
			this.FileName = inputFile;
			this.Templates = new ListTemplate();
			this.Templates.Name = Path.GetFileNameWithoutExtension(inputFile);
			this.IsNewFile = false;

			if (File.Exists(inputFile))
			{
				this.Read();
			}
			else
			{
				TemplateFile.New(inputFile);
			}
		}
		
		#region Read/write
		public void Read()
		{
			XmlReader reader = XmlReader.Create(this.FileName);

			while (reader.Read())
			{
				if (reader.IsStartElement())    // Only dectect start elements
				{
					if (reader.NodeType == XmlNodeType.Element && reader.Name == NODE_TEMPLATE)
					{
						int id = Int16.Parse(reader.GetAttribute(ATTRIBUTE_ID));
						string name = reader.GetAttribute(ATTRIBUTE_NAME);
						string spriteFileName = reader.GetAttribute(ATTRIBUTE_SPRITE);
						int frame = Int16.Parse(reader.GetAttribute(ATTRIBUTE_FRAME));
						int animation = Int16.Parse(reader.GetAttribute(ATTRIBUTE_ANIMATION));

						Template template = new Template(id, name);
						this.Templates.Items.Add(template);
					}
					else if (reader.NodeType == XmlNodeType.Element && reader.Name == NODE_PROPERTY)
					{
						string name = reader.GetAttribute(ATTRIBUTE_NAME);
						int value = Int16.Parse(reader.GetAttribute(ATTRIBUTE_VALUE));
						int defaultValue = Int16.Parse(reader.GetAttribute(ATTRIBUTE_DEFAULT));
						string description = reader.GetAttribute(ATTRIBUTE_NAME);

						Property property = new Property(name, value, defaultValue, description);
						Template template = this.Templates[this.Templates.Count - 1];
						template.AddProperty(property);
					}
				}
			}
			reader.Close();
		}

		public void Save(string fileName)
		{
			XmlWriterSettings settings = new XmlWriterSettings();
			settings.Indent = true;
			settings.IndentChars = "\t";
			settings.Encoding = Encoding.UTF8;
			XmlWriter writer = XmlWriter.Create(fileName, settings);

			writer.WriteStartElement(NODE_TEMPLATES);					// Templates (root)

			for (int i = 0; i < this.Templates.Count; i++)
			{
				writer.WriteStartElement(NODE_TEMPLATE);				// Template

				Template template = this.Templates[i];
				writer.WriteAttributeString(ATTRIBUTE_ID, template.ID.ToString());
				writer.WriteAttributeString(ATTRIBUTE_NAME, template.Name);
				writer.WriteAttributeString(ATTRIBUTE_SPRITE, template.SpriteName);
				writer.WriteAttributeString(ATTRIBUTE_FRAME, template.FrameProperty.ToString());
				writer.WriteAttributeString(ATTRIBUTE_ANIMATION, template.AnimationProperty.ToString());

				writer.WriteStartElement(NODE_PROPERTIES);				// Properties
				for (int j = 0; j < template.Properties.Count; j++)
				{
					Property property = template.Properties[j];
					writer.WriteStartElement(NODE_PROPERTY);			// Property

					writer.WriteAttributeString(ATTRIBUTE_NAME, property.ID.ToString());
					writer.WriteAttributeString(ATTRIBUTE_DEFAULT, property.DefaultValue.ToString());
					writer.WriteAttributeString(ATTRIBUTE_DESCRIPTION, property.Description);

					writer.WriteEndElement();							// End property
				}
				writer.WriteEndElement();								// End properties
				writer.WriteEndElement();								// End frame
			}

			writer.WriteEndElement();									// End templates (root)

			writer.Close();
		}

		public void Save()
		{
			this.Save(this.FileName);
		}
		#endregion

		#region Export
		/// <summary>
		/// Save source file.
		/// </summary>
		public void SaveSource(string fileName)
		{
			string name = Path.GetFileNameWithoutExtension(fileName).ToUpper();

			//
			// stringBuilder
			//
			StringBuilder builder = new StringBuilder();
			builder.Append("\n\t// Generate by Template Editor");
			builder.Append("\n\t//\n\t// ");
			builder.Append(name);
			builder.Append("\n\t//\n");

			//
			// Template ID
			//
			for (int i = 0; i < this.Templates.Count; i++)
			{
				Template template = this.Templates[i];
				
				if (this.Language == Language.Java)
				{
					builder.Append("\tpublic static final int ");
				}
				else
				{
					builder.Append("\tpublic const int ");
				}
				builder.Append(name);
				builder.Append("_TEMPLATE_");
				builder.Append(template.Name.ToUpper());
				builder.Append(" = ");
				builder.Append(i);
				builder.Append(";\n");
			}

			//
			// Save
			//
			StreamWriter writer = new StreamWriter(fileName, false);	// Always overwriter
			writer.Write(builder.ToString());
			writer.Close();
		}
		#endregion
	}
}
