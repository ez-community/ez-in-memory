﻿using System.Windows.Data;
using Core.Collection;

namespace Core.Templates
{
	public class ListTemplate : ListViewItem<Template>
	{
		public const string TEMPLATE_NAME_DEFAULT = "Template";

		public const string ELEMENT_TEMPLATES = "templates";
		public const string ELEMENT_TEMPLATE = "template";

		//public ListTemplate()
		//    : base()
		//{
		//    base.Name = "N/A";
		//}

		public override void AddDefaultItem()
		{
			int id = Item.DEFAULT_ID;
			if (base.Count > 0)
			{
				id = base[base.Count - 1].ID + 1;
			}

			string name = TEMPLATE_NAME_DEFAULT + " - " + id;

			Template template = new Template(id, name);
			this.Items.Add(template);
		}

		public override string[] GetColumnHeaders()
		{
			string[] headers = 
			{
				//"#",
				"ID",
				"Name"
			};

			return headers;
		}

		public override int[] GetColumnWidths()
		{
			int[] widths =
			{
				//20,
				30,
				80,
			};

			return widths;
		}

		public override string[] GetColumnBindings()
		{
			string[] bindings = 
			{
				//"#",
				"ID",
				"Name",
			};

			return bindings;
		}

#if DEBUG
		public static ListTemplate CreateExampleListTemplate()
		{
			Template camera = new Template(0, "Camera");

			Template template1 = new Template(1, "Hero");
			template1.SpriteName = "E:/hero.png";
			Property property = new Property("Health", 100, 100, "Life");
			template1.AddProperty(property);
			property = new Property("Mana", 50, 0, "Magic of hero");
			template1.AddProperty(property);

			Template template2 = new Template(2, "Monster");
			property = new Property("Strength", 1000, 500, "Strength of monster");
			template2.AddProperty(property);

			ListTemplate templates = new ListTemplate();
			templates.Items.Add(template1);
			templates.Items.Add(template2);

			return templates;
		}
#endif
	}
}

