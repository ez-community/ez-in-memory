﻿using System;
using System.Windows.Controls;

namespace Core.Controls
{
	public class EditableTextBlock : UserControl
	{
		private TextBlock textBlock;
		private TextBox textBox;

		public EditableTextBlock()
			: base()
		{
			this.textBlock = new TextBlock();
			this.textBox = new TextBox();
		}
	}
}
