﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Collection;
using Core.Level;

namespace Core.Controls
{
	public class CanvasLayer : Canvas
	{
		public const int RECTANGLE_SIZE_DEFAULT		= 5;

		public ListLayer Layers { get; private set; }
		public Item SelectedItem { get; set; }
		
		public CanvasLayer()
			: base()
		{
			this.Layers = new ListLayer();
		}

		private void AppendAll()
		{
			base.Children.Clear();
			this.AppendActor(this.Layers.Camera);

			for (int i = 0; i < this.Layers.Count; i++)
			{
				this.AppendLayer(this.Layers[i]);
			}
		}

		private void AppendActor(Actor actor)
		{
			int x = actor.X;
			int y = actor.Y;
			int width = actor.Width;
			int height = actor.Height;

			if (width == 0)
			{
				width = RECTANGLE_SIZE_DEFAULT;
			}

			if (height == 0)
			{
				height = RECTANGLE_SIZE_DEFAULT;
			}

			if (actor.Template.SpriteName != null)
			{
				Image image = new Image();
				image.Source = new BitmapImage(new Uri(actor.Template.SpriteName, UriKind.RelativeOrAbsolute));

				base.AppendImage(image, actor.UID, x, y);
			}
			else
			{
				base.AppendRectangle(actor.UID, x, y, width, height);
			}
		}

		private void AppendLayer(Layer layer)
		{
			for (int i = 0; i < layer.Count; i++)
			{
				this.AppendActor(layer[i]);
			}
		}

		private int FindChildIndex(Item item)
		{
			int index = INVALID_INDEX;
			
			int layer = 0;
			for (layer = 0; layer < this.Layers.Count; layer++)
			{
				index = this.Layers[layer].FindIndex(item);
				if (index != INVALID_INDEX)
				{
					break;
				}
			}

			if (index != INVALID_INDEX)
			{
				for (int i = 0; i < layer; i++)
				{
					index += this.Layers.Items[i].Count;
				}
			}

			return index;
		}

		private Actor FindActor(int index)
		{
			int pos = 0;
			for (int i = 0; i < this.Layers.Count; i++)
			{
				for (int j = 0; j < this.Layers[i].Items.Count; j++)
				{
					if (pos == index)
					{
						return this.Layers[i][j];
					}
					pos++;
				}
			}

			return null;
		}

		private void SetVisibleLayer(Layer layer, bool visible)
		{
			if (layer.Count > 0)
			{
				int index = this.FindChildIndex(layer[0]);
				for (int j = index; j < layer.Count + index; j++)
				{
					base.SetVisible(j, visible);
				}
			}
		}

		#region Override
		protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
		{
			base.OnMouseLeftButtonDown(e);
		}

		protected override void OnMovedChild(object sender, MouseEventArgs e)
		{
			base.OnMovedChild(sender, e);

			UIElement element = base.SelectedChild;
			double x = Canvas.GetLeft(element);
			double y = Canvas.GetTop(element);
			int uid = Int32.Parse(element.Uid);

			Actor actor = this.FindActor(base.SelectedChildIndex);
			if (actor != null)
			{
				actor.X = (int)x;
				actor.Y = (int)y;

				Debug.WriteLine("CanvasLayer->OnMovedChild: {0}", actor);
			}
		}

		protected override void OnSelectedChild(object sender, MouseEventArgs e)
		{
			base.OnSelectedChild(sender, e);
			this.SelectedItem = this.FindActor(base.SelectedChildIndex);
			//this.SelectedItem.IsSelected = true;
		}
		#endregion

		public void LoadLayers(ListLayer layers)
		{
			this.Layers = null;
			this.Layers = layers;
			this.AppendAll();
		}

		public void SelectChildUid(int uid)
		{
			for (int i = 0; i < VisualTreeHelper.GetChildrenCount(this); i++)
			{
				UIElement element = VisualTreeHelper.GetChild(this, i) as UIElement;
				if (element.Uid.Equals(uid.ToString()))
				{
					base.SelectChild(i);
					break;
				}
			}
		}

		public void SetVisible(Item item, bool visible)
		{
			if (item != null)
			{
				if (item == this.Layers)
				{
					for (int i = 0; i < this.Layers.Count; i++)
					{
						this.SetVisibleLayer(this.Layers[i], visible);
					}
				}
				else
				{
					for (int i = 0; i < this.Layers.Count; i++)
					{
						Layer layer = this.Layers[i];
						if (item.ID == layer.ID)
						{
							this.SetVisibleLayer(layer, visible);
						}
					}
				}
			}
		}

		#region Events
		#endregion
	}
}
