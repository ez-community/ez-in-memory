﻿using System;
using System.Drawing;

namespace Core.Sprite
{
	public class Module : SpriteItem
	{
		private static Size maxSize;
		private int x, y, width, height;

		public Module(int id, int x, int y, int width, int height, String name)
			: base(id, name)
		{
			this.x = x;
			this.y = y;
			this.width = width;
			this.height = height;
		}

		public Module(int id, int x, int y, int width, int height)
			: this(id, x, y, width, height, "")
		{

		}

		#region Override
		public override SpriteItem Clone()
		{
			Module module = new Module(this.ID, this.x, this.y, this.width, this.height, this.Name);
			return module;
		}

		public override bool IsValidItem()
		{
			if (this.X > -1 && this.Y > -1 &&
				this.width > 0 && this.height > 0 &&
				this.x + this.width <= maxSize.Width &&
				this.y + this.height <= maxSize.Height)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public override string[] GetValues()
		{
			string[] values =
			{
				this.ID.ToString(), this.x.ToString(), this.y.ToString(),
				this.width.ToString(), this.height.ToString(), this.Name
			};

			return values;
		}

		public override void Dispose()
		{
			this.width = 0;
			this.height = 0;
		}
		#endregion

		#region Properties
		public static Size MaxSize
		{
			set
			{
				maxSize = value;
			}
		}

		public int X
		{
			get
			{
				return this.x;
			}

			set
			{
				this.x = value;
			}
		}

		public int Y
		{
			get
			{
				return this.y;
			}

			set
			{
				this.y = value;
			}
		}

		public int Width
		{
			get
			{
				return this.width;
			}

			set
			{
				this.width = value;
			}
		}

		public int Height
		{
			get
			{
				return this.height;
			}

			set
			{
				this.height = value;
			}
		}
		#endregion

		#region Public
		public Rectangle GetRectangle()
		{
			return new Rectangle(this.x, this.y, this.width, this.height);
		}

		public void SetRectangle(Rectangle rect)
		{
			this.x = rect.X;
			this.y = rect.Y;
			this.width = rect.Width;
			this.height = rect.Height;
		}
		#endregion
	}
}
