﻿using System;
using System.Drawing;

namespace Core.Sprite
{
	public class FModule : SpriteItem
	{
		private int x, y;
		private Module module;
		private byte flag;

		public FModule(Module module, int x, int y)
			: base(module.ID, module.Name)
		{
			this.module = module;
			this.x = x;
			this.y = y;
		}

		public FModule(Module module)
			: this(module, 0, 0)
		{
		}

		#region Override
		public override SpriteItem Clone()
		{
			FModule fmodule = new FModule(this.module, this.x, this.y);
			return fmodule;
		}

		public override bool IsValidItem()
		{
			if (this.module == null)
			{
				return false;
			}
			else if (!this.module.IsValidItem())
			{
				return false;
			}
			else if (this.x < 0 || this.y < 0)
			{
				return false;
			}
			else
			{
				return true;
			}
		}

		public override string[] GetValues()
		{
			string[] values =
			{
				this.ID.ToString(), this.x.ToString(), this.y.ToString(), this.Name, base.GetFlagName(this.flag)
			};

			return values;
		}

		public override void Dispose()
		{
			this.module = null;
		}
		#endregion

		#region Properties
		public int X
		{
			get
			{
				return this.x;
			}

			set
			{
				this.x = value;
			}
		}

		public int Y
		{
			get
			{
				return this.y;
			}

			set
			{
				this.y = value;
			}
		}

		public Module Module
		{
			get
			{
				return this.module;
			}
		}

		public byte Flags
		{
			get
			{
				return this.flag;
			}

			set
			{
				this.flag = value;
			}
		}
		#endregion

		#region Public
		public Rectangle GetRectangle()
		{
			Rectangle rect = new Rectangle();

			if (this.module.IsValidItem())
			{
				rect.X = this.x;
				rect.Y = this.y;
				rect.Width = this.module.Width;
				rect.Height = this.module.Height;
			}

			return rect;
		}
		#endregion
	}
}
