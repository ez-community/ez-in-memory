﻿using System;

namespace Core.Sprite
{
	public class ListFrame : ListSpriteItem
	{
		public const int FRAME_ID_START = 2000; //0x2000;
		public const int HEADER_NAME = 3;

		#region Override
		public override ListSpriteItem Clone()
		{
			ListFrame frames = new ListFrame();
			for (int i = 0; i < base.Count; i++)
			{
				Frame frame = (Frame)base[i];
				Frame newFrame = new Frame(frame.ID, frame.Name);
				newFrame.FModules = (ListFModule)frame.FModules.Clone();

				frames.Add(newFrame);
			}

			return frames;
		}

		public override int GetNextId()
		{
			return base.GetNextId(FRAME_ID_START);
		}

		public override SpriteItem CreateDefaultItem(int id)
		{
			Frame frame = new Frame(id);
			return frame;
		}

		public override string[] GetHeaders()
		{
			string[] headers = { "", "#", "ID", "Name" };
			return headers;
		}

		public override int[] GetHeadersWidth()
		{
			int[] widths = { 0, 40, 60, 100 };
			return widths;
		}
		#endregion

		#region Public
		public int GetFModulesCount()
		{
			int length = 0;
			for (int i = 0; i < base.Count; i++)
			{
				Frame frame = (Frame)base[i];
				length += frame.FModules.Count;
			}

			return length;
		}
		#endregion
	}
}
