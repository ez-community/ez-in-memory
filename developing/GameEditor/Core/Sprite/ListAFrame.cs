﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Sprite
{
	public class ListAFrame : ListSpriteItem
	{
		public const int HEADER_X 		= 3;
		public const int HEADER_Y 		= HEADER_X + 1;
		public const int HEADER_TIME	= HEADER_Y + 1;
		public const int HEADER_NAME 	= HEADER_TIME + 1;
		public const int HEADER_FLAGS 	= HEADER_NAME + 1;

		#region Override
		public override ListSpriteItem Clone()
		{
			ListAFrame aframes = new ListAFrame();
			for (int i = 0; i < base.Count; i++)
			{
				AFrame aframe = (AFrame)base[i];
				AFrame newAFrame = new AFrame(aframe.Frame, aframe.X, aframe.Y);
				aframes.Add(newAFrame);
			}

			return aframes;
		}

		public override int GetNextId()
		{
			return 0;
		}

		public override SpriteItem CreateDefaultItem(int id)
		{
			return null;
		}

		public override string[] GetHeaders()
		{
			string[] headers = { "", "#", "FrameID", "X", "Y", "Time", "FrameName", "Flags" };
			return headers;
		}

		public override int[] GetHeadersWidth()
		{
			int[] widths = { 0, 30, 60, 40, 40, 40, 80, 80 };
			return widths;
		}
		#endregion
	}
}
