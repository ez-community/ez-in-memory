<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE tileset SYSTEM "http://mapeditor.org/dtd/1.0/map.dtd">
<tileset name="objects" tilewidth="32" tileheight="32">
 <image source="objects.png" width="64" height="32"/>
 <tile id="0">
  <properties>
   <property name="health" value="10"/>
   <property name="mana" value="5"/>
   <property name="name" value="object 1"/>
  </properties>
 </tile>
 <tile id="1">
  <properties>
   <property name="health" value="20"/>
   <property name="mana" value="5"/>
   <property name="name" value="object 2"/>
  </properties>
 </tile>
</tileset>
