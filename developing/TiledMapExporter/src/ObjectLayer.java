
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Tiled Map Editor object layer information.
 * @author Thanh Vinh
 */
public class ObjectLayer
{
	private static final byte TILE_FIRST_GID			= 10;	// First tile at 10 instead of 0

	private String name;
	private short size;
	private byte[] data;

	public ObjectLayer(String name, short size)
	{
		this.name = name;
		this.size = size;

		this.data = new byte[size];
	}

	public String getName()
	{
		return this.name;
	}

	public void setTileAt(int pos, byte gid)
	{
		this.data[pos] = gid;
	}

	/**
	 * Binary file format:
	 * - Tile id = gid - TILE_FIRST_GID instead of gid in XML file.
	 * - Tile none is 0xff (0 - 1 = -1) instead of 0 in XML file.
	 */
	public void saveBinary(String binaryFile)
	{
		try {
			DataOutputStream output = new DataOutputStream(new FileOutputStream(binaryFile, false));
			// Layer size
			output.writeShort(this.size);

			/* // Layer data
			for (int i = 0; i < this.data.length; i++) {
				byte tileId = (byte)(data[i] - TILE_FIRST_GID);
				output.writeByte(tileId);
			} */

			output.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
