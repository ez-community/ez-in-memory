@echo off

call config.bat

set COMPILE_MODE=%1%
if "%COMPILE_MODE%"=="" (
	set COMPILE_MODE=debug
)

echo ##
echo # Building %PROJECT_FULL_NAME%...
echo ##
echo.
echo Build mode = %COMPILE_MODE%
ant %COMPILE_MODE%
