###################################
## font-exporter properties file ##
###################################

# Font name: define the name for your font
FONT_NAME=FONT_NORMAL
# System font name: Arial, Times New Roman, Calibri...
BASE_FONT_NAME=Calibri
# Font style: PLAIN, BOLD, ITALIC
STYLE=BOLD
# Font size
SIZE=15
# Color in hexa value
COLOR=000000
# Transparent or not
TRANSPARENT=0
# Platform: use BitEndian or LittleEndian
PLATFORM=J2ME
# PLATFORM=XNA
# All characters which will be create
INPUT_STRING=!"#$%&'()*+,-_./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ\~©®ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚÝĂĐĨŨƠƯẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼẾỀỂỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪỬỮỰỲỴỶỸ™
