
import java.awt.Color;
import java.awt.Font;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

/**
 * Export bitmap font image and generate source code.
 * @author Thanh Vinh <thanh.vinh@hotmail.com>
 * Last updated: October 29, 2011
 */

public class FontExporter
{
	public static final String PLATFORM_J2ME	= "J2ME";
	public static final String PLATFORM_XNA		= "XNA";

	private static final String VERSION 		= "0.0.4";
	private static final String COPYRIGHT 		= "EZ Team, 2011 <ezteam@googlegroups.com>";
	private static final String TEXT_ENCODING 	= "UTF-8";

	//
	// Parse font style from configuration file
	//
	private static final List<String> FONT_STYLE_PROPERTIES = Arrays.asList("PLAIN", "BOLD", "ITALIC");
	private static final int[] FONT_STYLES 					= {Font.PLAIN, Font.BOLD, Font.ITALIC};

	private static final String PATH_SEPARATED 				= "\\";

	private String platform;
	private FontImage fontImage;
	private String outputPath;

	private String fontName;
	private boolean transparent;		// Transparent output image or use magenta color as background color
	private String input;

	public FontExporter(String configFile, String outputPath) throws IOException
	{
		this.parseConfigFile(configFile);
		this.outputPath = outputPath;
	}

	private void parseConfigFile(String configFile) throws IOException
	{
		Properties properties = new Properties();
		properties.load(new InputStreamReader(new FileInputStream(configFile), TEXT_ENCODING));

		//
		// Properties in configuration file
		//
		// Platform
		this.platform = properties.getProperty("PLATFORM");
		// Font name
		this.fontName = properties.getProperty("FONT_NAME");
		// Create Font object
		String baseFontName = properties.getProperty("BASE_FONT_NAME");
		String style = properties.getProperty("STYLE");
		int size = Integer.parseInt(properties.getProperty("SIZE"));
		int styleIndex = FONT_STYLE_PROPERTIES.indexOf(style);

		// Transparent
		if (properties.getProperty("TRANSPARENT").equals("1")) {
			this.transparent = true;
		}

		// All characters which will be create
		this.input = properties.getProperty("INPUT_STRING");

		//
		// Create FontImage object
		//
		Font font = new Font(baseFontName, FONT_STYLES[styleIndex], size);
		Color color = new Color(Integer.parseInt(properties.getProperty("COLOR"), 16));

		this.fontImage = new FontImage(font, color, this.input);
	}

	public void saveImage() throws IOException
	{
		String fileName = this.outputPath + PATH_SEPARATED + this.fontName.toLowerCase() + ".png";
		this.fontImage.writeImage(fileName, this.transparent);
	}

	public void saveCharacterMapping() throws IOException
	{
		Character[] characters = this.fontImage.getCharacters();
		String fileName = this.outputPath + PATH_SEPARATED + this.fontName.toLowerCase() + "_mapping.bin";
		DataOutputStreamEx output = new DataOutputStreamEx(new FileOutputStream(fileName, false));

		// Use Little Endian for XNA platform
		boolean isReverseBytes = (this.platform.equals(PLATFORM_XNA) ? true : false);
		output.writeShort((short) characters.length, isReverseBytes);
		for (int i = 0; i < characters.length; i++) {
			output.writeShort((short) characters[i].getChar(), isReverseBytes);
		}
		output.writeShort((short) this.fontImage.getSpaceCharWidth(), isReverseBytes);	// Space char length
		output.writeShort((short) this.fontImage.getCharHeight(), isReverseBytes);		// All char height

		output.close();
	}

	public void saveSprite() throws IOException
	{
		StringBuffer buffer = new StringBuffer();

		buffer.append("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n");
		// buffer.append("<!-- Generate by font-exporter -->\n");
		buffer.append("<sprite>\n");
		buffer.append("\t<image>");
		buffer.append(this.fontName.toLowerCase() + ".png");
		buffer.append("</image>\n");

		// Generate character offsets
		Character[] characters = this.fontImage.getCharacters();
		buffer.append("\t<modules>\n");
		for (int i = 0; i < characters.length; i++) {
			buffer.append("\t\t<module id=");
			buffer.append("\"" + (1000 + i) + "\" ");
			buffer.append("x=\"" + characters[i].getX() + "\" ");
			buffer.append("y=\"" + characters[i].getY() + "\" ");
			buffer.append("width=\"" + characters[i].getWidth() + "\" ");
			buffer.append("height=\"" + characters[i].getHeight() + "\" ");
			buffer.append("name=\"" + characters[i].getName() + "\"");
			// buffer.append("name=\"\"");
			buffer.append(" />\n");
		}
		buffer.append("\t</modules>\n");

		// Don't map frame and animation
		buffer.append("\t<frames />\n");
		buffer.append("\t<animations />\n");
		buffer.append("</sprite>\n");

		//
		// Save to file
		//
		String fileName = this.outputPath + PATH_SEPARATED + this.fontName.toLowerCase() + ".sprite";
		BufferedWriter writer = new BufferedWriter(
				// new OutputStreamWriter(new FileOutputStream(fileName, this.append), TEXT_ENCODING));
				new OutputStreamWriter(new FileOutputStream(fileName), TEXT_ENCODING));

		writer.write(buffer.toString());
		writer.close();
	}

	public static void main(String[] args)
	{
		System.out.println("FontExporter v" + VERSION);
		System.out.println(COPYRIGHT);

		if (args.length > 1) {
			String configFile = args[0];
			String outputPath = args[1];

			try
			{
				FontExporter fontExporter = new FontExporter(configFile, outputPath);

				System.out.println("\t- Config file:" + configFile);
				System.out.println("\t- Save sprite file in: " + outputPath);

				fontExporter.saveImage();
				fontExporter.saveCharacterMapping();
				fontExporter.saveSprite();

				System.out.println("Export complete...");
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			System.out.println("Command: FontExporter properties-file sprite-out-path");
			System.out.println("Example: FontExporter sample-font.mak Z:/sprites");
		}
	}
}
