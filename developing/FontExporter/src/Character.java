
public class Character
{
	private static final String[] SPECICAL_LIST = {
		"\"", "&", "<", ">"
	};

	private static final String[] ESCAPE_LIST = {
		"&quot;", "&amp;", "&lt;", "&gt;"
	};

	private int x, y, width, height;
	private char character;
	private String name;

	public Character(int x, int y, int width, int height, char character)
	{
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;

		this.character = character;
		this.name = String.valueOf(character);
		for (int i = 0; i < SPECICAL_LIST.length; i++) {
			if (this.name.equals(SPECICAL_LIST[i])) {
				this.name = ESCAPE_LIST[i];
				break;
			}
		}
	}

	public int getX()
	{
		return this.x;
	}

	public int getY()
	{
		return this.y;
	}

	public int getWidth()
	{
		return this.width;
	}

	public int getHeight()
	{
		return this.height;
	}

	public char getChar()
	{
		return this.character;
	}

	public String getName()
	{
		return name;
	}
}
