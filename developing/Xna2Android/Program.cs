﻿/*
 * Created by SharpDevelop.
 * User: Thanh Vinh
 * Date: 2/19/2012
 * Time: 11:16 AM
 *
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using Xna2Android.Core;

namespace Xna2Android
{
	public class Program
	{
		private const string INTRO		 	= "Xna2Android version 0.0.1";
		private const string COPYRIGHT	 	= "Copyright 2012 EZ Team <ezgroup@groups.live.com>";

		public static void Main(string[] args)
		{
			Console.WriteLine(INTRO);
			Console.WriteLine(COPYRIGHT);

			if (args.Length > 0) {
				string inputPath = args[0];

				Console.WriteLine("Processing " + inputPath + "...");

				Pattern.Load();
				Xna2Android.Process(inputPath);

				Console.WriteLine("Done...");
			} else {
				Console.WriteLine("Command: Xna2Android xna-input-source-path");
			}
		}
	}
}