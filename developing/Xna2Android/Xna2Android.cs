﻿/*
 * Created by SharpDevelop.
 * User: Thanh Vinh
 * Date: 2/19/2012
 * Time: 2:08 PM
 *
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.IO;
using Xna2Android.Core;

namespace Xna2Android
{
	/// <summary>
	/// Description of Xna2Android.
	/// </summary>
	public static class Xna2Android
	{
		private const string CS_SEARCH_PATTERN		= "*.cs";
		private const string CS_EXT					= ".cs";
		private const string JAVA_EXT				= ".java";

		public static string[] GetFiles(string path)
		{
			List<string> files = new List<string>();

			//
			// Get all files in sub directories
			//
			string[] dirs = Directory.GetDirectories(path);

			for (int i = 0; i < dirs.Length; i++) {
				files.AddRange(Directory.EnumerateFiles(dirs[i], CS_SEARCH_PATTERN));
			}


			//
			//  Get all files in root directory
			//
			files.AddRange(Directory.EnumerateFiles(path, CS_SEARCH_PATTERN));

			return files.ToArray();
		}

		public static string Read(string path)
		{
			string source;

			using (TextReader reader = File.OpenText(path)) {
				source = reader.ReadToEnd();
			}

			return source;
		}

		public static void Write(string path, string value)
		{
			using (TextWriter writer = new StreamWriter(path)) {
				writer.Write(value);
				writer.Flush();
			}
		}

		public static void Process(string path)
		{
			string[] files = GetFiles(path);

			for (int i = 0; i < files.Length; i++) {
				string input = files[i];
				string output = files[i].Replace(CS_EXT, JAVA_EXT);

				string source = Read(input);

				Pattern.AddPackage(ref source);
				Pattern.Remove(ref source);
				Pattern.Replace(ref source);

				Write(output, source);
			}
		}
	}
}
