#ifndef  _SPRITE_H_
#define  _SPRITE_H_

#include "CCSprite.h"
#include "CCTexture2D.h"

namespace cocos2d
{
	class Sprite : public CCSprite
	{
	public:
		virtual ~Sprite(void);
		Sprite();

	public:
		virtual void draw(void);

	public:
		/** Creates an sprite with a binary sprite file. */
		static Sprite* spriteWithBSpriteFile(const char *pszFileName);
	};
}
#endif	//_SPRITE_H_
