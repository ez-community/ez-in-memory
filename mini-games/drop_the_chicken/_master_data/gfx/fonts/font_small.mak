###################################
## font-exporter properties file ##
###################################

# Font name: define the name for your font
FONT_NAME=FONT_SMALL
# System font name: Arial, Times New Roman, Calibri...
BASE_FONT_NAME=Arial Narrow
# Font style: PLAIN, BOLD, ITALIC
STYLE=BOLD
# Font size
SIZE=18
# Color in hexa value
COLOR=000000
# Transparent or not
TRANSPARENT=0
# Platform: use BitEndian or LittleEndian
# PLATFORM=J2ME
PLATFORM=XNA
# All characters which will be create
INPUT_STRING=0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ
