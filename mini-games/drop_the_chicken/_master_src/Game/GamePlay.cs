﻿
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using GameLib;

public class GamePlay
{
	private const int MAX_NINJA = 200; // hoang.nv rem temp 50
    public static int numberNinjaPass = 0;

	public static Pad       pad;
    public static LightBlade lightBlade;
	public static Ninja[]   ninjas;
	public static Arrow[]   arrows;
    public static Grenade   grenade;
    public static bonusItem itemBonus;
    public static float     TIME_APPEAR_ITEMS = 300;
    public static int       beginFrame      = 0;
    public static int       beginFrameBonus = 0;

	private static int currentNinja             = 0;
	private static int frameSetNumberNinja      = 0;
	private static int numberNinja              = 0;
	private static int NUMBER_FRAME_FOR_UPDATE  = 100;
	private static int numberNinjaEnable        = 3;

	private static int lastFrameAppearNinja;
	private static int currentFrameWaitNinja;
	private static int currentIndexNinja;
	private static int frameUpdateCounter;
	// game substate
	private static int currentSubState = -1;

    public static Boolean isEndGame = false;

	private static void DrawHUD(SpriteBatch spriteBatch)
	{
        int frameHudId = 0;
        if (Ninja.numberNinjaLive <= 0)
            frameHudId = 0;
        else if (Ninja.numberNinjaLive == 1)
            frameHudId = 1;
        else if (Ninja.numberNinjaLive == 2)
            frameHudId = 2;
        else if (Ninja.numberNinjaLive == 3)
            frameHudId = 3;
        else if (Ninja.numberNinjaLive == 4)
            frameHudId = 4;
        else if (Ninja.numberNinjaLive >= 5)
            frameHudId = 5;
        Game.sprites[DATA.SPRITE_HUD].DrawFrame(spriteBatch, frameHudId, 10, 10, SpriteAnchor.NONE);
	}

	private static void DrawBackground(SpriteBatch spriteBatch)
	{
		Game.sprites[DATA.SPRITE_FARGROUND].DrawFrame(spriteBatch, 0, 0, 0, SpriteAnchor.NONE);
		Game.sprites[DATA.SPRITE_BACKGROUND].DrawFrame(spriteBatch, DATA.BACKGROUND_FRAME_TAVERN, 0, Game.GetScreenHeight(), SpriteAnchor.LEFT | SpriteAnchor.BOTTOM);
	}

	public static void Initialize()
	{
		Game.Log("GamePlay->Initialize: Load actors...");

        numberNinjaPass = 0;
		lastFrameAppearNinja	= 0;
		currentFrameWaitNinja	= 0;
		currentIndexNinja		= 0;
		frameUpdateCounter      = 0;

        beginFrame              = 0;        

		currentSubState = GameSubState.TUTORIAL;

		ninjas = new Ninja[MAX_NINJA];
        Ninja.numberNinjaLive = Ninja.MAX_NUMBER_NINJA_DEAD;

		for (int i = 0; i < MAX_NINJA; i++)
		{
			Animation animNinja = new Animation(Game.sprites[DATA.SPRITE_NINJA]);
			animNinja.SetAnim(DATA.NINJA_ANIMATION_RUN);

			ninjas[i] = new Ninja(ActorType.ACTOR_NINJA, Game.sprites[DATA.SPRITE_NINJA]);
			ninjas[i].SetAnim(Game.GetRandomInt(DATA.NINJA_ANIMATION_NINJA1_RUN, DATA.NINJA_ANIMATION_NINJA4_RUN + 1));
			initNinja(i);
			ninjas[i].setEnableUpdate(false);
		}

		pad = new Pad(ActorType.ACTOR_PAD, Game.sprites[DATA.SPRITE_PAD]);
		pad.SetAnim(DATA.PAD_ANIMATION_WOOD);

		//cheat for Test Arrows
		arrows = new Arrow[3];
		for (int i = 0; i < 3; i++)
		{
            arrows[i] = new Arrow(ActorType.ACTOR_ARROW, Game.sprites[DATA.SPRITE_ITEM]);
			arrows[i].SetAnim(DATA.ITEM_ANIMATION_ARROW);
			////*********************////
			// Initialize Arrows
			// Velocity && Angle    : enable Arrows Fly Near or Far
			// timeChange           : Enable Arrows Fly Slow Or Fast
			// Angle : Have To > 90 because Arrow Have To Fly follow Left.
			////*********************////
			arrows[i].Initialize( 30 + i * 10, 140, 0.1f);
		}

        grenade = new Grenade(ActorType.ACTOR_GRENADE, Game.sprites[DATA.SPRITE_ITEM]);
        grenade.SetAnim(DATA.ITEM_ANIMATION_BOMB);
        grenade.Initialize(50, 130, 0.07f, false);

        itemBonus = new bonusItem(ActorType.ACTOR_GRENADE, Game.sprites[DATA.SPRITE_ITEM]);        
        
		//Init Actor Smoke
		for (int i=0; i<NINJA_SMOKE_NUM; i++) {
			ninja_smoke[i] = new Actor(ActorType.ACTOR_SMOKE, Game.sprites[DATA.SPRITE_ITEM]);
			ninja_smoke[i].SetAnim(DATA.ITEM_ANIMATION_SMOKE);
			ninjaSmokeFrameCount[i] = 99;
		}

        isEndGame = false;

		//end cheat for Test

		Sound.PlayMusic(DATA.SOUND_M_TITLE);

		Game.SetGameMessage(Message.UPDATE);

        lightBlade = new LightBlade(1, null);
        lightBlade.Initialize();
        lightBlade.setLightBlade(300,400, 500, 200);
	}

	public static void Update(GameTime gameTime, int message)
	{
		if (message == Message.INITIALIZE)
		{
			Initialize();
		}
		else if (message == Message.UPDATE)
		{
			// hoang.nv define temp, will move to other class
			int FRAME_WAIT_NEXT_NINJA_MAX	= 100;
			int FRAME_WAIT_NEXT_NINJA_MIN	= 30;

			if (isEndGame)
			{
				GameOver.Update(gameTime, message);
				return;
			}
			
			// Update pad
			//pad.Update(gameTime);
			
			switch(currentSubState)
			{
				case GameSubState.TUTORIAL:
					if (Game.IsKeyPressed(Keys.Enter)
						|| Game.IsTap(0, 0, Game.GetScreenWidth(), Game.GetScreenHeight()))
						setState(GameSubState.NORMAL);

					break;

				case GameSubState.NORMAL:
					if (lastFrameAppearNinja==0 || lastFrameAppearNinja+currentFrameWaitNinja==frameUpdateCounter)
					{
                        addNinja(currentSubState);
						lastFrameAppearNinja = frameUpdateCounter;
						currentFrameWaitNinja = Game.GetRandomInt(FRAME_WAIT_NEXT_NINJA_MIN, FRAME_WAIT_NEXT_NINJA_MAX);
					}

                    if (frameUpdateCounter - beginFrame > TIME_APPEAR_ITEMS)
                    {
                        beginFrame = frameUpdateCounter;
                        itemBonus.SetAnim(Game.GetRandomInt(DATA.ITEM_ANIMATION_ICE, DATA.ITEM_ANIMATION_BONUS_MONEY));
                        itemBonus.Initialize(Game.GetRandomInt(7, 13) * 5, 35, 0.07f, true);
                    }
					break;

				case GameSubState.BONUS_MODE:
					if (frameUpdateCounter % 10 == 0)
                        addNinja(currentSubState);
					lastFrameAppearNinja = frameUpdateCounter;
					currentFrameWaitNinja = 5;
                    //if (frameUpdateCounter == BonusMode.FRAMES_TO_CHANGE_BONUS_MODE)
                    //{
                    //    lastFrameAppearNinja = frameUpdateCounter;
                    //    currentFrameWaitNinja = random.Next(FRAME_WAIT_NEXT_NINJA_MIN, FRAME_WAIT_NEXT_NINJA_MAX);
                    //    //setState(GameSubState.NORMAL);
                    //}
                    //if (frameUpdateCounter - beginFrameBonus > BonusMode.FRAMES_TO_UPDATE_BONUS_MODE)
                    //    setState(GameSubState.NORMAL);
					break;
			}

			if (currentSubState > GameSubState.TUTORIAL)
			{
				for (int i = 0; i < MAX_NINJA; i++)
				{
					if (ninjas[i].getEnableUpdate())
						ninjas[i].Update(gameTime);
					// if (ninjas[i].GetX() > GameLibConfig.screenWidth)
						// initNinja(i);
				}

				//cheat for Test Arrows
				for (int i = 0; i < 3; i++)
				{
					arrows[i].Update(gameTime);
					if ((arrows[i].GetY() > GameLibConfig.screenHeight) || (arrows[i].GetX() < 0))
						arrows[i].Initialize(30 + i * 10, 140, 0.07f);
				}

				grenade.Update(gameTime);
                if(beginFrame != 0)
                    itemBonus.Update(gameTime);
				if ((grenade.GetY() > GameLibConfig.screenHeight) || (grenade.GetX() < 0))
					grenade.Initialize(50, 130, 0.07f, false);
				//end Test Arrows

				updateEffect(EffectType.NINJA_SMOKE, gameTime);
			}

			frameUpdateCounter ++;

            if (Ninja.numberNinjaLive <= 0 && !isEndGame)
            {
                GameOver.isBeginGameOver = true;
                isEndGame = true;
            }
		}
	}

    private static void addNinja(int currentSubState)
	{
		while(ninjas[currentIndexNinja].getEnableUpdate())
		{
			currentIndexNinja ++;
			if (currentIndexNinja >= MAX_NINJA)
				currentIndexNinja = 0;
		}
        if(currentSubState == GameSubState.NORMAL)
		    initNinja(currentIndexNinja);
        else if (currentSubState == GameSubState.BONUS_MODE)
            initNinjaBonus(currentIndexNinja);
		ninjas[currentIndexNinja].setEnableUpdate(true);
	}

	private static void removeNinja(int index)
	{
		ninjas[index].setEnableUpdate(false);
	}

	// Ninja smoke effect defines
	private const int NINJA_SMOKE_NUM = 8;
	private static Actor[] ninja_smoke = new Actor[NINJA_SMOKE_NUM];
	private static int[] ninjaSmokeFrameCount = new int[NINJA_SMOKE_NUM];
	private static int smokeCurIndex = 0;

	public static void addEffect(int type, int x, int y)
	{
		switch(type)
		{
			case EffectType.NINJA_SMOKE:
				ninja_smoke[smokeCurIndex].SetAnim(DATA.ITEM_ANIMATION_SMOKE);
				ninja_smoke[smokeCurIndex].SetPosition(x, y);
				ninjaSmokeFrameCount[smokeCurIndex] = 0;
				smokeCurIndex ++;
				smokeCurIndex %= NINJA_SMOKE_NUM;
				break;

			case EffectType.NINJA_GHOST:
				break;
		}
	}

	private static void updateEffect(int type, GameTime gameTime)
	{
		switch(type)
		{
			case EffectType.NINJA_SMOKE:
				for (int i=0; i<NINJA_SMOKE_NUM; i++) {
					if (ninjaSmokeFrameCount[i]<20)
						ninja_smoke[i].Update(gameTime);
				}
				break;

			case EffectType.NINJA_GHOST:
				break;
		}
	}

	private static void drawEffects(SpriteBatch spriteBatch, GameTime gameTime)
	{
		// EffectType.NINJA_SMOKE:
		for (int i=0; i<NINJA_SMOKE_NUM; i++) {
			if (ninjaSmokeFrameCount[i]<20)
				ninja_smoke[i].Draw(spriteBatch, gameTime);
			ninjaSmokeFrameCount[i] ++;
		}
	}

	public static void setState(int newstate)
	{
		currentSubState = newstate;
	}

	public static void Draw(SpriteBatch spriteBatch, GameTime gameTime)
	{
		DrawBackground(spriteBatch);
		DrawHUD(spriteBatch);

		switch(currentSubState)
		{
			case GameSubState.TUTORIAL:
				Game.sprites[DATA.SPRITE_TUTORIAL].DrawFrame(spriteBatch, DATA.TUTORIAL_FRAME_TUTORIAL, 0, 0, 0);
				int x;
				if ((frameUpdateCounter/100)%2==0)
					x = frameUpdateCounter%100;
				else
					x = 100 - frameUpdateCounter%100;
				Game.sprites[DATA.SPRITE_TUTORIAL].DrawFrame(spriteBatch, DATA.TUTORIAL_FRAME_FINGER, x*2, 0, 0);
				break;

			case GameSubState.NORMAL:
			case GameSubState.BONUS_MODE:
				// Draw pad
				//pad.Draw(spriteBatch, gameTime);

				// Draw Ninja
				for (int i = 0; i < MAX_NINJA; i++)
				{
					if (ninjas[i].getEnableUpdate())
						ninjas[i].Draw(spriteBatch, gameTime);
				}

				//Cheat for Test Arrows
				for (int i = 0; i < 3; i++)
				{
					arrows[i].Draw(spriteBatch, gameTime);
				}
				grenade.Draw(spriteBatch, gameTime);
                if (beginFrame != 0) 
                    itemBonus.Draw(spriteBatch, gameTime);

				drawEffects(spriteBatch, gameTime);
				//end Cheat
				break;
		}

        if (isEndGame)
        {
            GameOver.Draw(spriteBatch, gameTime);
        }

        lightBlade.Draw(spriteBatch, gameTime);
	}

	public static void Exit()
	{

	}

	public static void initNinja(int i)
	{
		int positionNinja = Game.GetRandomInt(1, 4);
		if (positionNinja == 1)
			ninjas[i].Initialize(30 + Game.GetRandomInt(1, 5) * 5,  Game.GetRandomInt(6, 8) * 5, positionNinja, 0.12f);
		else if (positionNinja == 2)
			ninjas[i].Initialize(40 + Game.GetRandomInt(1, 4) * 5, Game.GetRandomInt(1, 6) * 5 + 30 , positionNinja, 0.12f);
		else if (positionNinja == 3 || positionNinja == 4)
			ninjas[i].Initialize(40 + Game.GetRandomInt(1, 4) * 5, Game.GetRandomInt(10, 12) * 5, positionNinja, 0.13f);
	}

    static int count = 1;
    static bool isIncrease = true;
	public static void initNinjaBonus(int i)
	{
		int positionNinja = Game.GetRandomInt(1, 3);

        if (count > 15)
            isIncrease = false;
        else if (count <= 0)
            isIncrease = true;

        if (isIncrease) count = count + 1;
        else count = count - 1;
		
		if (positionNinja == 1)
            ninjas[i].Initialize(/*30 + Game.GetNextRandomInt(1, 2) * 5*/ 30 + count, Game.GetRandomInt(6, 7) * 5, 1, 0.12f);
        else if (positionNinja == 2 || positionNinja == 3)
            ninjas[i].Initialize(/*40 + Game.GetNextRandomInt(1, 2) * 5*/ 40 + count, Game.GetRandomInt(3, 4) * 5 + 30, 2, 0.12f);
        //else if (positionNinja == 3 || positionNinja == 4)
        //    ninjas[i].Initialize(/*40 + Game.GetNextRandomInt(1, 2) * 5*/ 40 + count, Game.GetNextRandomInt(10, 11) * 5, positionNinja, 0.13f);
	}

    public static int getFrameUpdateCount()
    {
        return frameUpdateCounter;
    }
}
