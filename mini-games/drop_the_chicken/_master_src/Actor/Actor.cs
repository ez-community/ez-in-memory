﻿
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using GameLib;

public class Actor
{
	//
	// Constants: rectangle information
	//
	public const int RECT_X1 = 0;
	public const int RECT_Y1 = 1;
	public const int RECT_X2 = 2;
	public const int RECT_Y2 = 3;

	//
	// Fields
	//
	protected int type;					// ActorType
	protected int state;				// ActorState
	protected int x, y;					// Current position
	protected int start_X, start_Y;
	protected int vX, vY;				// Velocity
	protected int aX, aY;				// Accelerate
	protected int angle;				// Angle
	protected int nVelocity;            // Van Toc
	protected float startTime;			// Time
	protected float timeChange;
	protected double acceleration = 9.81;
	protected int zOrder;
	protected int flags;
	protected int[] rect;
	protected Actor[] links;
	protected short[] parameters;
	protected int level;				// Actor level

	//
	// Animation
	//
	//protected Sprite sprite;			// Sprite
	//protected int frame;				// Current frame
	//protected int aframe;				// Current aframe
	//protected int anim;				// Current animation
	protected Animation animation;		// Animation
	protected Sprite sprite;			// Sprite is pointed to Animation
	protected bool isFlipX;				// Flip X
	protected bool isFlipY;				// Flip Y

	//
	// Debug
	//
	#if USE_DEBUG_RECT
	protected Texture2D _texture;
	protected Rectangle _rectangle;
	protected Color _color;
	#endif

	//
	// Constructors
	//
	public Actor()
	{
	}

	public Actor(int type, Sprite sprite)
	{
		this.type = type;
		this.sprite = sprite;
		this.animation = new Animation(sprite);

		this.Initialize();
	}

	public Actor(int type, Animation animation)
	{
		this.type = type;
		this.animation = animation;
		this.sprite = animation.GetSprite();

		this.Initialize();
	}

	//
	// Static methods
	//
	//
	// Static
	//
	public static bool isRectCrossing(int[] rect1, int[] rect2)
	{
		if (rect1 == null || rect2 == null)
			return false;
		if (rect1[0] > rect2[2]) return false;
		if (rect1[2] < rect2[0]) return false;
		if (rect1[1] > rect2[3]) return false;
		if (rect1[3] < rect2[1]) return false;
		if (rect1[0] == rect1[2] && rect1[1] == rect1[3]) return false;
		if (rect2[0] == rect2[2] && rect2[1] == rect2[3]) return false;
		return true;
	}

	//
	// Properties
	//
	public int GetActorType()
	{
		return type;
	}

	public int GetX()
	{
		return this.x;
	}

	public int GetY()
	{
		return this.y;
	}

	public void SetPosition(int x, int y)
	{
		this.x = x;
		this.y = y;
	}

	public void SetAnim(int anim)
	{
		this.animation.SetAnim(anim);
	}

	public int GetAnim()
	{
		return this.animation.GetAnim();
	}

	public bool IsEndAnim()
	{
		return this.animation.IsEndAnim();
	}

	//
	// Public methods
	//
	public int[] GetRect()
	{
		return this.rect;
	}

	public bool IsCollidingWith(Actor another)
	{
		int[] rect2 = another.GetRect();
		return isRectCrossing(this.rect, rect2);
	}

	//
	// Virtual methods
	//
	public virtual void Initialize()
	{
		this.rect = new int[4];

		#if USE_DEBUG_RECT
		this._rectangle = new Rectangle();
		this._texture = new Texture2D(Game.GetGraphics(), 1, 1);
		this._texture.SetData(new Color[] { Color.White });
		this._color = Color.Red;
		this._color.A = 0xff >> 2;
		#endif
	}

	public virtual void ComputeRect()
	{
		this.animation.GetRect(this.rect, this.x, this.y);
	}

	public virtual void Update(GameTime gameTime)
	{
		// Update animation
		this.animation.Update();
		// Update actor's rectangle
		this.ComputeRect();
	}

	public virtual void Draw(SpriteBatch spriteBatch, GameTime gameTime)
	{
		this.animation.Draw(spriteBatch, this.x, this.y, SpriteAnchor.NONE);

		#if USE_DEBUG_RECT
		this._rectangle.X = this.rect[0];
		this._rectangle.Y = this.rect[1];
		this._rectangle.Width = this.rect[2] - this.rect[0];
		this._rectangle.Height = this.rect[3] - this.rect[1];
		spriteBatch.Draw(this._texture, this._rectangle, this._color);
		#endif	//USE_DEBUG_RECT
	}

	public int GetTrajectoryX(int nVelocity, int nAngle, float time, int nStartPosX)
	{
		return (int)(nVelocity * Math.Cos(nAngle * Game.MATH_PI / 180) * time + nStartPosX);
	}

	public int GetTrajectoryY(int nVelocity, int nAngle, float time, int nStartPosY)
	{
		return nStartPosY - (int)(nVelocity * Math.Sin(nAngle * Game.MATH_PI / 180) * time - (acceleration * (time * time)) / 2);
	}
}
