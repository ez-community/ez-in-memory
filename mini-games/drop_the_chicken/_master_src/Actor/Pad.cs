﻿
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using GameLib;

public class Pad : Actor
{
	private const int PAD_SPEED = 10;			// Move speed
	private const int PAD_LIMIT_X = 150;		// Litmit x and wall
	private const int VALUE_CHANGE = 10;
	private const int MANUAL_VALUE_Y = 400;

	public bool isCollisionWithNinja = false;
	private bool isIncreaseY = true;
	private int valueChangeCurrent = 0;

	public Pad(int type, Sprite sprite)
		: base(type, sprite)
	{
	}

	public override void Initialize()
	{
		this.x = GameLibConfig.screenWidth / 2;
		this.y = MANUAL_VALUE_Y;
		base.Initialize();
	}

	public override void Update(GameTime gameTime)
	{
		//
		// Move
		//
		if (this.rect[RECT_X1] < PAD_LIMIT_X) {															// Limit x1
			this.x = PAD_LIMIT_X;
		} else if (this.rect[RECT_X2] > (Game.GetScreenWidth() - PAD_LIMIT_X)) {						// Limit x2
			this.x = (Game.GetScreenWidth() - PAD_LIMIT_X) - (this.rect[RECT_X2] - this.rect[RECT_X1]);
		} else {																						// Safe area, can move
			if (Game.IsKeyHold(Keys.Left)) {															// Press left
				this.x -= PAD_SPEED;
			} else if (Game.IsKeyHold(Keys.Right)) {													// Press right
				this.x += PAD_SPEED;
			} else if (Game.GetPointerY() > this.rect[RECT_Y1]) {											// Tap under pad
				int mouseX = Game.GetPointerX();
				if (mouseX < this.rect[RECT_X1]) {														// Tap left pad side
					this.x -= PAD_SPEED;
				} else if (mouseX > this.rect[RECT_X2]) {												// Tap right pad side
					this.x += PAD_SPEED;
				} else if (mouseX > this.rect[RECT_X1] && Game.GetPointerX() < this.rect[RECT_X2]) {		// Tap in pad
					this.x = mouseX - ((this.rect[RECT_X2] - this.rect[RECT_X1]) >> 1);
				}
			}
		}

		//
		// Colliding
		if (isCollisionWithNinja)
		{
			Sound.PlaySfx(DATA.SOUND_SFX_HIT_PAD);

			if (isIncreaseY)
				valueChangeCurrent++;
			else
				valueChangeCurrent--;
			if (valueChangeCurrent > VALUE_CHANGE)
				isIncreaseY = false;
			if (valueChangeCurrent < 0)
			{
				isIncreaseY             = true;
				isCollisionWithNinja    = false;
				valueChangeCurrent      = 0;
			}
			this.y = MANUAL_VALUE_Y + valueChangeCurrent;
		}

		// Call base.Update at here to update common properties
		base.Update(gameTime);
	}
}
