﻿
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using GameLib;

public class Light : Actor
{
    public const int MAX_ANGLE_ROTATE = 50;

    public int angle = 0;
    public int value = 1;
    public int typeDraw = 0;
    public Texture2D texture;
    private bool isStop = true;

	public Light(int type, Sprite sprite)
		: base(type, sprite)
	{
	}

    public void run()
    {
        this.isStop = false;
    }
    public void stop()
    {
        this.isStop = true;
    }

    public void Initialize(int start_X, int start_Y, float timeChange)
	{
        isStop = true;
        this.timeChange = timeChange;
        this.startTime = 0;
        this.typeDraw = 0;
        this.x = this.start_X = start_X;
        this.y = this.start_Y = start_Y;

	#if !ANDROID
        RenderTarget2D renderTarget;
        Texture2D shadowMap;

        renderTarget = new RenderTarget2D(Game.GetGraphics(), sprite.GetTexture(5).Width, sprite.GetTexture(5).Height);
        Game.GetGraphics().SetRenderTarget(renderTarget);
        //SpriteBatch spriteBatch = new SpriteBatch(renderTarget.get);
        Game.spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.NonPremultiplied);
        Game.spriteBatch.Draw(sprite.GetTexture(5), new Vector2(0, 0), Color.White);
        Game.fontSprites[DATA.FONT_NORMAL].DrawString(Game.spriteBatch, "350", 30, 30, SpriteAnchor.HCENTER | SpriteAnchor.VCENTER);
        Game.spriteBatch.End();
        Game.GetGraphics().SetRenderTarget(null);
        texture = (Texture2D)renderTarget;

        Game.GetGraphics().Clear(ClearOptions.Target | ClearOptions.DepthBuffer, Color.Transparent, 1.0f, 0);
	#endif	//!ANDROID

		base.Initialize();
	}

	public override void Update(GameTime gameTime)
	{
        if (!isStop)
        {
            this.startTime += this.timeChange;
            if (this.startTime > 1.5)
                typeDraw = 1;
        }
            // Call base.Update at here to update common properties
            base.Update(gameTime);
        
	}

    public override void Draw(SpriteBatch spriteBatch, GameTime gameTime)
    {
		#if !ANDROID
       // if (!isStop)
       // {
            int width = sprite.GetTexture(5).Width;
            int height = sprite.GetTexture(5).Height;
            if (typeDraw == 1)
            {
                angle += value;
                if (angle >= MAX_ANGLE_ROTATE)
                {
                    value = -1;
                    angle = MAX_ANGLE_ROTATE - 1;
                }
                else if (angle < -MAX_ANGLE_ROTATE)
                {
                    value = 1;
                    angle = -MAX_ANGLE_ROTATE;
                }



                //spriteBatch.Draw(sprite.GetTexture(5), new Vector2(start_X - angle / 3, start_Y - Math.Abs(angle) / 10), null, Color.White, angle * 0.01f, new Vector2(width / 2, 0), 1.0f, SpriteEffects.None, 0f);
                float s = (float)(20 * Math.Cos(Game.MATH_PI / 2 + angle * Game.MATH_PI / 100));
                int l = sprite.GetTexture(5).Height + 20;
                spriteBatch.Draw(texture, new Vector2(start_X + s, start_Y - Math.Abs(angle) / 10), null, Color.White, angle * 0.01f, new Vector2(width / 2, 0), 1.0f, SpriteEffects.None, 0f);

                //Game.fontSprites[DATA.FONT_NORMAL].DrawString(spriteBatch, "ARCADE", 100, Game.GetScreenHeight() - 40, SpriteAnchor.HCENTER | SpriteAnchor.VCENTER);


                
                //s.Dispose();
            }
            else if (typeDraw == 0)
            {
                spriteBatch.Draw(sprite.GetTexture(5), new Vector2(start_X - width * startTime / 2.4f, start_Y), new Rectangle(0, 0, width, height), Color.White, 0.0f, new Vector2(0, 0), startTime / 1.2f, SpriteAnchor.NONE, 0.0f);
            }
        //}
		#endif	//!ANDROID
    }
}
