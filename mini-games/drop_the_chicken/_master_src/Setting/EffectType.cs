﻿
public static class EffectType
{
    public const int NINJA_SMOKE = 1;
    public const int NINJA_GHOST = NINJA_SMOKE + 1;
}