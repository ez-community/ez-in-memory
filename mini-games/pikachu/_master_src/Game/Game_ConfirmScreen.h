
	///*********************************************************************
	///* Game_ConfirmScreen.h
	///*********************************************************************

	//
	// Confirm screen types
	//
	private static final int CONFIRM_SCREEN_SOUND 				= 0;
	private static final int CONFIRM_SCREEN_EXIT_GAME 			= 1;
	private static final int CONFIRM_SCREEN_EXIT_TO_MAIN_MENU 	= 2;
	private static final int CONFIRM_SCREEN_DIALOG			 	= 3;

	// Confirm screen question message
	private static final int[] CONFIRM_SCREEN_TITLES = {
		TEXT.SOUND_CONFIRM,
		TEXT.EXIT_GAME_CONFIRM,
		TEXT.EXIT_TO_MAIN_MENU_CONFIRM,
		TEXT.DIALOG_TITLE
	};

	private static final int[] CONFIRM_SCREEN_ITEMS = {
		TEXT.COMMON_YES,
		TEXT.COMMON_NO,
	};

	private static void paintConfirmScreen(Graphics g, int type)
	{
		int dy = sprites[SPRITE_MENU].getFrameHeight(MENU_FRAME_BUTTON);
		dy += (dy >> 1);
		int height = fonts[FONT_NORMAL].getCharHeight() + (dy * CONFIRM_SCREEN_ITEMS.length);
		int x = getScreenWidth() >> 1;
		int y = ((getScreenHeight() - height) >> 1);

		fillRect(0x000000);
		if (type == CONFIRM_SCREEN_EXIT_GAME) {
			sprites[SPRITE_SPLASH].paintFrame(g, SPLASH_FRAME_LARGE,
						getScreenWidth() >> 1, getScreenHeight() >> 1, Graphics.HCENTER | Graphics.VCENTER);
		} else if (type == CONFIRM_SCREEN_EXIT_TO_MAIN_MENU || type == CONFIRM_SCREEN_DIALOG) {
			paintGamePlayBackGround(g);
		}
		fillRect(g, transparentImageMask, 0, 0, getScreenWidth(), getScreenHeight());

		if(type == CONFIRM_SCREEN_DIALOG)
		{
			if(getScreenHeight() == 320)
				y += 15;
			else
				y += 25;
			fonts[FONT_NORMAL].setPalette(PALETTE_FONT_NORMAL_DEFAULT);
			fonts[FONT_NORMAL].drawString(g, Package.getText(TEXT.HIGHT_SCORE_MARK) + (Game.getActor(ACTOR_HERO).getHightScore() * 100),
					getScreenWidth() >> 1, (getScreenHeight() >> 1) - 80, Graphics.HCENTER | Graphics.VCENTER);
		}

		fonts[FONT_NORMAL].setPalette(PALETTE_FONT_NORMAL_DEFAULT);
		String title = Package.getText(CONFIRM_SCREEN_TITLES[type]);
		fonts[FONT_NORMAL].drawString(g, title, x, y, Graphics.HCENTER | Graphics.VCENTER);

		int palette = (selectedMenuItem == TEXT.COMMON_YES) ? PALETTE_FONT_NORMAL_HIGHT_LIGHT : PALETTE_FONT_NORMAL_DEFAULT;
		int frame = (selectedMenuItem == TEXT.COMMON_YES) ? MENU_FRAME_BUTTON : MENU_FRAME_BUTTON_SELECTED;
		sprites[SPRITE_MENU].paintFrame(g, frame, x, y + dy, Graphics.HCENTER | Graphics.VCENTER);
		fonts[FONT_NORMAL].setPalette(palette);
		fonts[FONT_NORMAL].drawString(g, Package.getText(TEXT.COMMON_YES),
			x, y + dy, Graphics.HCENTER | Graphics.VCENTER);

		palette = (selectedMenuItem == TEXT.COMMON_NO) ? PALETTE_FONT_NORMAL_HIGHT_LIGHT : PALETTE_FONT_NORMAL_DEFAULT;
		frame = (selectedMenuItem == TEXT.COMMON_NO) ? MENU_FRAME_BUTTON : MENU_FRAME_BUTTON_SELECTED;
		sprites[SPRITE_MENU].paintFrame(g, frame, x, y + (dy << 1), Graphics.HCENTER | Graphics.VCENTER);
		fonts[FONT_NORMAL].setPalette(palette);
		fonts[FONT_NORMAL].drawString(g, Package.getText(TEXT.COMMON_NO),
			x, y + (dy << 1), Graphics.HCENTER | Graphics.VCENTER);
	}

	private static void updateConfirmScreen(Graphics g, int message, int type)
	{
		switch (message) {
			case MESSAGE_CONSTRUCTOR:
				selectedMenuItem = TEXT.COMMON_NO;
				transparentImageMask = createTransparentImage(TRANSPARENT_IMAGE_MASK_SIZE,
					TRANSPARENT_IMAGE_MASK_SIZE, 0x000000, 0x80);
				setSoftKeys(TEXT.SOFTKEY_SELECT, SOFTKEY_NONE);
				break;

			case MESSAGE_UPDATE:
				if (GAME_KEY_UP_PRESSED) {
					selectedMenuItem = TEXT.COMMON_YES;
				} else if (GAME_KEY_DOWN_PRESSED) {
					selectedMenuItem = TEXT.COMMON_NO;
				} else if (GAME_KEY_SELECT) {
					switch (type) {
						case CONFIRM_SCREEN_SOUND:
							if (selectedMenuItem == TEXT.COMMON_YES) {
								setEnableSound(true);
							}
							setGameState(STATE_SPLASH_SCREEN);
							break;

						case CONFIRM_SCREEN_EXIT_GAME:
							if (selectedMenuItem == TEXT.COMMON_YES) {
								setGameState(STATE_EXIT);
							} else {
								setGameState(lastState);
							}
							break;

						case CONFIRM_SCREEN_EXIT_TO_MAIN_MENU:
							if (selectedMenuItem == TEXT.COMMON_YES) {
								setGameState(STATE_LOADING_TO_MAIN_MENU);
							} else {
								setGameState(lastState);
							}
							break;

						case CONFIRM_SCREEN_DIALOG:
							if (selectedMenuItem == TEXT.COMMON_YES) {
								setGameState(lastState);
							} else {
								setGameState(STATE_LOADING_TO_MAIN_MENU);
							}
							break;
					}	// switch(type)
				}	// else if (GAME_KEY_FIRE)
				break;	// case MESSAGE_UPDATE

			case MESSAGE_PAINT:
				paintConfirmScreen(g, type);
				break;

			case MESSAGE_DESTRUCTOR:
				break;
		}	//switch (message)
	}
