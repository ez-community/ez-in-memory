
	///*********************************************************************
	///*  Game_Paint.h
	///*********************************************************************

	/**
	 * Orientation for drawGradientBox() method.
	 */
	public static final int GRADIENT_VERTICAL 	= 0;
	public static final int GRADIENT_HORIZONTAL = 1;

	private static final int SOFTKEY_NONE 		= -1;

	private static int leftSoftKey = SOFTKEY_NONE;
	private static int rightSoftKey = SOFTKEY_NONE;
	private static int midSoftKey = SOFTKEY_NONE;

	public static void setSoftKeys(int left, int mid, int right)
	{
	#if !USE_INVERT_SOFTKEYS
		leftSoftKey = left;
		rightSoftKey = right;
	#else	//!USE_INVERT_SOFTKEY
		leftSoftKey = right;
		rightSoftKey = left;
	#endif	//!USE_INVERT_SOFTKEY
		midSoftKey = mid;
	}

	public static void setSoftKeys(int left, int right)
	{
	#if !USE_INVERT_SOFTKEYS
		leftSoftKey = left;
		rightSoftKey = right;
	#else	//!USE_INVERT_SOFTKEY
		leftSoftKey = right;
		rightSoftKey = left;
	#endif	//!USE_INVERT_SOFTKEY
		midSoftKey = SOFTKEY_NONE;
	}

	public static void paintSoftKeys(Graphics g)
	{
		setClip(0, 0, getScreenWidth(), getScreenHeight());
		if (fonts[FONT_NORMAL] != null) {
			fonts[FONT_NORMAL].setPalette(PALETTE_FONT_NORMAL_DEFAULT);
		}

		// Paint left soft key
		if (leftSoftKey != SOFTKEY_NONE) {
			fonts[FONT_NORMAL].drawString(g, Package.getText(leftSoftKey),
				2, getScreenHeight() - 2, Graphics.LEFT | Graphics.BOTTOM);
		}

		// Paint right soft key
		if (rightSoftKey != SOFTKEY_NONE) {
			fonts[FONT_NORMAL].drawString(g, Package.getText(rightSoftKey),
				getScreenWidth() - 2, getScreenHeight() - 2, Graphics.RIGHT | Graphics.BOTTOM);
		}

		// Paint mid soft key
		if (midSoftKey != SOFTKEY_NONE) {
			fonts[FONT_NORMAL].drawString(g, Package.getText(midSoftKey),
				getScreenWidth() >> 1, getScreenHeight() - 2, Graphics.HCENTER | Graphics.BOTTOM);
		}
	}

	public void paintPress5Key(Graphics g, FontSprite font, int x, int y)
	{
		fonts[FONT_NORMAL].setPalette(PALETTE_FONT_NORMAL_HIGHT_LIGHT);
		if (frameCounter % 10 < 4) {
			font.drawString(g, Package.getText(TEXT.PRESS_5_TO_CONTINUE), x, y, Graphics.HCENTER | Graphics.TOP);
		}
	}

	public void paintPress5Key(Graphics g)
	{
		paintPress5Key(g, fonts[FONT_NORMAL], getScreenWidth() >> 1, getScreenHeight() - 20);
	}

	public static int getMidColor(int color1, int color2, int prop, int max)
	{
		int red =
			(((color1 >> 16) & 0xff) * prop +
			((color2 >> 16) & 0xff) * (max - prop)) / max;

		int green =
			(((color1 >> 8) & 0xff) * prop +
			((color2 >> 8) & 0xff) * (max - prop)) / max;

		int blue =
			(((color1 >> 0) & 0xff) * prop +
			((color2 >> 0) & 0xff) * (max - prop)) / max;

		int color = red << 16 | green << 8 | blue;

		return color;
	}

	public static void drawGradientBox(int color1, int color2, int left, int top, int width, int height, int orientation, Graphics g)
	{
		int max = orientation == GRADIENT_VERTICAL ? height : width;

		for(int i = 0; i < max; i++) {
			int color = getMidColor(color1, color2, max * (max - 1 - i) / (max - 1), max);

			g.setColor(color);

			if(orientation == GRADIENT_VERTICAL)
				g.drawLine(left, top + i, left + width - 1, top + i);
			else
				g.drawLine(left + i, top, left + i, top + height - 1);
		}
	}

	public static void drawGradientBackground(Graphics g)
	{
		final int BACKGROUND_GRADIENT_COLOR1 = 0x000000;
		final int BACKGROUND_GRADIENT_COLOR2 = 0x1a6dbf;

		setClip(0, 0, getScreenWidth(), getScreenHeight());
		drawGradientBox(BACKGROUND_GRADIENT_COLOR1, BACKGROUND_GRADIENT_COLOR2, 0, 0,
				getScreenWidth(), getScreenHeight(), GRADIENT_VERTICAL, g);
	}
