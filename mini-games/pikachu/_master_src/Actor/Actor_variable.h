//-------------------------------------------------------------------------
    // Type of cActor (see IActorType) and .gts
    int         m_type;
    
    // id in layers(xxx.level)
    int         m_unid;

    // Position of cActor in 3D
    int         m_x;
    int         m_y;
    int         m_z;
    
	int 		m_ax; 				// the acceleration of actor by pixel << FIXED_PRECISION
	int 		m_ay; 				// the acceleration of actor by pixel << FIXED_PRECISION

    // Velocity of cActor in 3D
    int         m_vx;
    int         m_vy;

    // Generic flag variable for any boolean info (see IActorFlags)
    int         m_flags;
    
    int[] _rect;
    //    int[] _rectVis;
    //    

    int _state;
    int _substate;
    // previous full state (full state = _state + _subState), it's used for the flag SF_GOTO_PRE_STATE
    public int _preFullState;
    // the counter of the time that current state has run
    public int _stateCounter;
    // the counter of the time that current state has run
    public int _stateTimer;

    // For animations
    GameSprite sprite;
	public int animation;
	public boolean visible;
	
	public Actor(GameSprite sprite, int animation, int x, int y)
	{
		this.sprite = sprite;
		this.animation = animation;
		this.m_x = x;
		this.m_y = y;
		this.visible = true;
	}

	public Actor(GameSprite sprite)
	{
		this(sprite, 0, 0, 0);
	}

	public Actor()
	{
		this(null, 0, 0, 0);
	}
	
	public void setSprite(GameSprite sprite)
	{
		this.sprite = sprite;
	}
	
	public void setAnimation(int index)
	{
		this.animation = index;
	}
	
    void setXY(int x, int y)
    {
        if(x == m_x && y == m_y)
            return;
        setX(x);
        setY(y);
//        m_x = x;
//        m_y = y;
//        m_posx = x << FIXED_PRECISION;
//        m_posy = y << FIXED_PRECISION;
//        
//        this.callBackReGetRectAfterSetXY();
    }

    // setx: after this set, let adjustX is equal x (eg: let _rct[1] == 60)
    void setXadjust(int x, int adjustX)
    {
        setX(m_x - adjustX  + x);
    }
    void setYadjust(int y, int adjustY)
    {
        setY(m_y - adjustY + y);
    }      
    void setX(int x)
    {

        if(x == m_x)
            return;
        m_x = x;
//        pre_mX = m_x = x;
        // m_posx = x << FIXED_PRECISION;
    }
    void setY(int y)
    {
        if( y == m_y)
            return;
//        pre_mY = m_y = y;
        m_y = y;
        // m_posy = y << FIXED_PRECISION;
    }
    
    public void setZorder(int z)
    {
        m_z = z;
    }

    

    void setVx(int vx)
    {
        m_vx = vx;
    }
    
    void setVy(int vy)
    {  
        m_vy = vy; 	
    }
    
    void setV(int vx, int vy)
    {
       setVx(vx);
       setVy(vy); 
    }

    void clearVx()
    {
        m_vx  = m_ax = 0;
    }
    void clearVy()
    {
        m_vy = m_ay = 0;
    }

    void clearVAll()
    {
        m_vx = m_vy = m_ax = m_ay = 0;
    }  
  
    public boolean hasFlag(int flagMask)
    {
        return (this.m_flags & flagMask) != 0;
    }

    public void setFlag(int flagMask)
    {
         this.m_flags |= flagMask ;
    }

    public void removeFlag(int flagMask)
    {
         this.m_flags &= ~flagMask ;
    }

    