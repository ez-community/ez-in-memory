
	///*********************************************************************
	///* Actor_Update.h
	///*********************************************************************

	public void update()
	{
		if ((this.flags & ACTOR_FLAG_NOT_ACTIVE) == 0) {
			switch (type) {
				case ACTOR_HERO:
					if (GAME_KEY_LEFT_REPEATED) {
						this.x -= ACTOR_MC_MOVE_SPEED;
						// fix for width screen (320x240)
						if (Game.s_isWidthScreen)
							this.x -= 2;
						if (x < ACTOR_MC_MOVE_LIMIT_LEFT) {
							this.x = ACTOR_MC_MOVE_LIMIT_LEFT;
						}

						if (this.animation != CHARACTER_ANIMATION_MOVE && this.isEndAnimation()) {
							this.setAnimation(CHARACTER_ANIMATION_MOVE);
						}
					} else if (GAME_KEY_RIGHT_REPEATED) {
						this.x += ACTOR_MC_MOVE_SPEED;
						int offset = 0;
						// fix for width screen (320x240)
						if (Game.s_isWidthScreen) {
							this.x += 2;
							offset = 25;
						}
						if (x > Game.getScreenWidth() - ACTOR_MC_MOVE_LIMIT_RIGHT - offset) {
							this.x = Game.getScreenWidth() - ACTOR_MC_MOVE_LIMIT_RIGHT - offset;
						}

						if (this.animation != CHARACTER_ANIMATION_MOVE && this.isEndAnimation()) {
							this.setAnimation(CHARACTER_ANIMATION_MOVE);
						}
					}

					if (this.isCollidingWith(Game.getActor(ACTOR_NPC))
							|| this.isCollidingWith(Game.getActor(ACTOR_NPC_2))
							|| this.isCollidingWith(Game.getActor(ACTOR_NPC_3))
							) {
						Game.playSound(SOUND_SFX_JUMP);
						this.setAnimation(CHARACTER_ANIMATION_SAVE_1);
					}

					if (this.isCollidingWith(Game.getActor(ACTOR_BONUS))) {
						// play sound get item
						if (Game.getActor(ACTOR_BONUS)._state == ACTOR_BONUS_ITEM) {
							Game.getActor(ACTOR_HERO).setBlood(Game.getActor(ACTOR_HERO).getBlood() + 1);
						} else { // ACTOR_BONUS_WORM
							Game.getActor(ACTOR_HERO).setBlood(Game.getActor(ACTOR_HERO).getBlood() - 1);
						}
						Game.getActor(ACTOR_BONUS).cleanBonus();
					}

					if (this.isEndAnimation()) {
						this.setAnimation(CHARACTER_ANIMATION_STAND);
					}

					break;	//ACTOR_HERO

				case ACTOR_NPC:
				case ACTOR_NPC_2:
				case ACTOR_NPC_3:
					updateAI();
					break;

				case ACTOR_OBJECT:
					updateObject();
					break;

				case ACTOR_BONUS:
					updateBonus();
					break;
			}	//switch(type)

			// Check for loop animation objects
			if (this.animation > -1 && (this.flags & ACTOR_ANIM_LOOP) != 0) {
				if (this.isEndAnimation()) {
					this.aframe = 0;
				}
			}

			this.updateRect();
		}	//if ((this.flags & ACTOR_FLAG_INVISIBLE) == 0)
	}

	private int parabolX, parabolY;
	private int startTime;

	private boolean is_NPCDied = true;
	private long time_NPCDie;
	private int currentJumpInfoID = -1;

	public void initAI()
	{
		if (is_NPCDied) {
			currentJumpInfoID = GameLib.getRandomNumber(m_NPCJumpInfoList.length/5, currentJumpInfoID);
			this.x = 0;
			this.y = Game.getScreenHeight()/3 - 10;
			parabolX = 0;
			parabolY = Game.getScreenHeight()/3 - 10;

			this.angle = m_NPCJumpInfoList[5*currentJumpInfoID];
			this.vX = m_NPCJumpInfoList[5*currentJumpInfoID + 1];
			this.vY = m_NPCJumpInfoList[5*currentJumpInfoID + 2];
			startTime = 0;// 0.0f;

			// fix vX for width screen (320x240)
			if (Game.s_isWidthScreen)
				this.vX = this.vX*320/240;

			int animation = GameLib.getRandomNumber(NPC_ANIMATION_JUMP_10 - NPC_ANIMATION_JUMP_1);
			this.setAnimation(animation);

			Game.playSound(SOUND_SFX_JUMP);
			is_NPCDied = false;
			// System.out.println(" -------------------------------- ");
			// System.out.println("   currentJumpInfoID: " + currentJumpInfoID);
		}
	}

	public void cleanAI()
	{
		this.setAnimation(-1);
		is_NPCDied = true;
	}

	public void updateAI()
	{
		if (!is_NPCDied) {
			startTime += 1;// 0.5f;
			this.x = getTrajectoryX(this.vX, this.angle, startTime, parabolX);
			this.y = getTrajectoryY(this.vY, this.angle, startTime, parabolY);

			// TODO Correct colliding with hero, change the direction once time only
			if (this.vX > 0 && this.isCollidingWith(Game.getActor(ACTOR_HERO))) {
				startTime = 0;// 0.0f;
				parabolX = this.x;
				parabolY = this.y = this.y - 20;
				// change vX & vY value
				this.vX = m_NPCJumpInfoList[5*currentJumpInfoID + 3];
				this.vY = m_NPCJumpInfoList[5*currentJumpInfoID + 4];
				
				// fix vX for width screen (320x240)
				if (Game.s_isWidthScreen)
					this.vX = this.vX*320/240;
			} else if (this.x > Game.getScreenWidth() || this.vX <= 0
				|| (this.isCollidingWith(Game.getActor(ACTOR_OBJECT))) ) {

				if(this.isCollidingWith(Game.getActor(ACTOR_OBJECT)))
					Game.getActor(ACTOR_HERO).setHightScore(Game.getActor(ACTOR_HERO).getHightScore() + 1);
				is_NPCDied = true;
				cleanAI();

			} else if (this.y >= Game.getScreenHeight() - ACTOR_NPC_MOVE_LIMIT_BOTTOM) {
				//TODO: set NPC die animation here
				is_NPCDied = true;
				time_NPCDie = System.currentTimeMillis();
				this.setAnimation(this.animation + NPC_ANIMATION_NPC_1_DIE);
				Game.playSound(SOUND_SFX_FAIL);

				Game.getActor(ACTOR_HERO).setBlood(Game.getActor(ACTOR_HERO).getBlood() - 1);
				//Cheat : Add Blood For Hero When Blood = 0
				if(Game.getActor(ACTOR_HERO).getBlood() == 0)
				{
					// Game.getActor(ACTOR_HERO).setBlood(3);
					// Game.getActor(ACTOR_HERO).setHightScore(0);
				}
				//End Cheat
			}
		} else if (System.currentTimeMillis()-time_NPCDie>500) {
			cleanAI();
		}

		if (/*!is_NPCDied && */this.animation>-1 && this.isEndAnimation()) {
			this.setAnimation(this.animation);
		}
	}

	public void initObject()
	{
		this.x = Game.getScreenWidth() - 45;
		// fix vX for width screen (320x240)
		if (Game.s_isWidthScreen)
			this.x -= 20;
		this.y = Game.getScreenHeight() - 100;
		this.setAnimation(OBJECT_ANIMATION_LEAF);

	}

	public void updateObject()
	{
		if (this.isCollidingWith(Game.getActor(ACTOR_NPC))
				|| this.isCollidingWith(Game.getActor(ACTOR_NPC_2))
				|| this.isCollidingWith(Game.getActor(ACTOR_NPC_3))
				) {
			this.setAnimation(OBJECT_ANIMATION_LEAF_FULL);
		} else if (this.isEndAnimation()) {
			this.setAnimation(OBJECT_ANIMATION_LEAF);
		}
	}

	public static boolean s_isBonusFalling = false;
	public static final int ACTOR_BONUS_ITEM		= 0;
	public static final int ACTOR_BONUS_WORM		= 1;
	public static final int ACTOR_BONUS_TYPES_NUM	= 2;

	public void initBonus()
	{
		if (!s_isBonusFalling) {
			this._state = GameLib.getRandomNumber(ACTOR_BONUS_TYPES_NUM);

			this.x = GameLib.getRandomNumber(ACTOR_MC_MOVE_LIMIT_RIGHT - ACTOR_MC_MOVE_LIMIT_LEFT, this.x-ACTOR_MC_MOVE_LIMIT_LEFT) + ACTOR_MC_MOVE_LIMIT_LEFT;
			this.y = 10;
			if (this._state == ACTOR_BONUS_ITEM)
				// this.setAnimation(OBJECT_ANIMATION_ITEM_BONUS);
				this.setFrame(OBJECT_FRAME_ITEM_LIFE);
			else // ACTOR_BONUS_WORM
				this.setAnimation(OBJECT_ANIMATION_WROM_MOVE);
			s_isBonusFalling = true;
		}
	}

	public void updateBonus()
	{
		if (s_isBonusFalling) {
			this.y += 4;
			if (this.y>Game.getScreenHeight())
				this.cleanBonus();
		}
	}

	public void cleanBonus()
	{
		this.setAnimation(-1);
		this.y = -20;
		s_isBonusFalling = false;
	}

	public void updateHub()
	{
	}
