
public interface IActorType
{
	public static final int ACTOR_HERO					= 0;
	public static final int ACTOR_NPC					= ACTOR_HERO 			+ 1;
	public static final int ACTOR_NPC_2					= ACTOR_NPC 			+ 1;
	public static final int ACTOR_NPC_3					= ACTOR_NPC_2 			+ 1;
	public static final int ACTOR_OBJECT				= ACTOR_NPC_3 			+ 1;
	public static final int ACTOR_BONUS					= ACTOR_OBJECT 			+ 1;
	
	public static final int ACTOR_MAX					= ACTOR_BONUS				+ 1;
}
