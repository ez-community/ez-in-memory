
import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;
import javax.microedition.lcdui.Display;

public class PikachuMIDlet extends MIDlet
{
	private static Display display;
	private static Game game;

	protected void pauseApp()
	{
		if (game != null) {
			game.pause();
		}
	}

	protected void startApp() throws MIDletStateChangeException
	{
		if (game == null) {
			game = new Game(this);
			display = Display.getDisplay(this);
			game.start();
			display.setCurrent(game);
		}
		game.resume();
	}

	public void exit()
	{
		try {
			this.destroyApp(false);
		} catch (MIDletStateChangeException e) {
			e.printStackTrace();
		}
		super.notifyDestroyed();
	}

	protected void destroyApp(boolean unconditional) throws MIDletStateChangeException
	{
		if (game != null) {
			game.exit();
		}
		super.notifyDestroyed();
	}
}
