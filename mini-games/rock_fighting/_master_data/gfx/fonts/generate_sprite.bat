
@echo off
call ..\..\..\config.bat

echo Generate font sprite...
for /f %%i in ('dir /b *.mak') do (
	echo Processing file: %%i
	java -jar %FONT_EXPORTER% ./%%i .
)
