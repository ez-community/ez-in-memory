
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;

/**
 * A very simple game actor.
 * @author EZ Solution Inc.
 */

public class Actor implements DATA, IDefines, IState, IActorType, IActorFlag
{
	//
	// Static fields
	//
	protected static int frameCounter;

	//
	// Fields
	//
	protected int type;					// IActorType
	protected int sex;					// IActorType
	protected int blood;				// Blood
	protected int state;				// IActorType
	protected int x, y;					// Current position
	protected int vX, vY;				// Velocity
	protected int aX, aY;				// Accelerate
	protected int angle;				// Angle
	protected int zOrder;
	protected int flags;
	protected int[] rect;
	protected Actor[] links;
	protected short[] params;

	//
	// Animation
	//
	protected GameSprite sprite;		// Sprite
	protected int frame;				// Current frame
	protected int aframe;				// Current aframe
	protected int animation;			// Current animation
	protected boolean isFlipX;			// Flip X
	protected boolean isFlipY;			// Flip Y

	public Actor(int type, GameSprite sprite, int animation, int x, int y)
	{
		this.type = type;
		this.sprite = sprite;
		this.animation = animation;
		this.x = x;
		this.y = y;
	}

	public Actor(int type, GameSprite sprite)
	{
		this(type, sprite, 0, 0, 0);
		this.rect = new int[4];				// x1, y1, x2, y2

		this.initialize();					// Initialize all actors
	}

	//
	// Static
	//
	public static boolean isRectCrossing(int[] rect1, int[] rect2)
	{
		if(rect1 == null || rect2 == null)
			return false;
		if (rect1[0] > rect2[2]) return false;
		if (rect1[2] < rect2[0]) return false;
		if (rect1[1] > rect2[3]) return false;
		if (rect1[3] < rect2[1]) return false;
		if (rect1[0] == rect1[2] && rect1[1] == rect1[3]) return false;
		if (rect2[0] == rect2[2] && rect2[1] == rect2[3]) return false;
		return true;
	}

	//
	// Properties
	//
	public void setSprite(GameSprite sprite)
	{
		this.sprite = sprite;
	}

	public void setPosition(int x, int y)
	{
		this.x = x;
		this.y = y;
	}

	public int getX()
	{
		return this.x;
	}

	public int getY()
	{
		return this.y;
	}

	public void setFrame(int frame)
	{
		this.frame = frame;
		this.animation = -1;
	}

	public void setAnimation(int animation)
	{
		this.animation = animation;
		this.frame = -1;
		this.aframe = 0;
	}

	public boolean isEndAnimation()
	{
		if (this.aframe < this.sprite.getAFrameCount(this.animation)) {
			return false;
		} else {
			return true;
		}
	}

	public void setFlags(int mask)
    {
		this.flags |= mask ;
    }

	public int getFlags()
	{
		return this.flags;
	}

	//
	// Colliding
	//
	public void updateRect()
	{
		if (this.animation > -1) {
			this.sprite.getAFrameRect(this.rect, this.animation, this.aframe,
				this.x - Game.getCameraX(), this.y - Game.getCameraY());
		} else if (this.frame > -1) {
			this.sprite.getFrameRect(this.rect, this.frame, this.x, this.y);
		}
	}

	public int[] getRect()
	{
		return this.rect;
	}

	public boolean isCollidingWith(Actor another)
	{
		int[] rect2 = another.getRect();
		return isRectCrossing(this.rect, rect2);
	}

	public void render(Graphics g)
	{
		if ((this.flags & ACTOR_FLAG_NOT_ACTIVE) == 0 && (this.flags & ACTOR_FLAG_INVISIBLE) == 0) {
			if (this.animation > -1) {
				this.sprite.paintAFrame(g, this.animation, this.aframe, this.x, this.y, 0);
				this.aframe++;

				// Check for loop animation objects
				if (this.isEndAnimation()) {
					if ((this.flags & ACTOR_FLAG_ANIM_LOOP) != 0) {
						this.aframe = 0;
					} else {
						this.animation = -1;
					}
				}
			} else if (this.frame > - 1) {
				this.sprite.paintFrame(g, this.frame, this.x, this.y, 0);
			}

			#if USE_DEBUG_RECT
			GameLib.drawRect(g, 0xff00ff, this.rect[0], this.rect[1],
			this.rect[2] - this.rect[0], this.rect[3] - this.rect[1]);
			#endif
		}
	}

	#include "Actor/Actor_Initialize.h"
	#include "Actor/Actor_Math.h"
	#include "Actor/Actor_Update.h"
	#include "Actor/Actor_Hero.h"
	#include "Actor/Actor_Computer.h"
}
