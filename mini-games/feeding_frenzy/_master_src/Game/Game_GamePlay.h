
	///*********************************************************************
	///* Game_GamePlay.h
	///*********************************************************************

	private static int gameLevel;
	private static int heroLevel;
	private static int score;

	public static int levelMapWidth;
	public static int levelMapHeight;

	private static int cameraX;
	private static int cameraY;

	public static Actor hero;
	private static Actor boss;

	public static void setCameraXY(int x, int y)
	{
		cameraX = x;
		cameraY = y;
	}

	public static int getCameraX()
	{
		return cameraX;
	}

	public static void setCameraX(int x)
	{
		cameraX = x;
	}

	public static int getCameraY()
	{
		return cameraY;
	}

	public static void setCameraY(int y)
	{
		cameraY = y;
	}

	private static void paintGamePlayBackGround(Graphics g)
	{
		// fillRect(0x2e69f6);
		if (!GameLibConfig.useCircularBuffer) {
			GameLib.paintTileset(g, sprites[SPRITE_TILE_SEA], MAP_SEA, cameraX, cameraY);
		} else {
			GameLib.paintTileset(null, sprites[SPRITE_TILE_SEA], MAP_SEA, cameraX, cameraY);
		}
		GameLib.paintTileset(g, sprites[SPRITE_TILE_MAP1], MAP_MAP1, cameraX, cameraY);
	}

	private static void paintGamePlayHUD(Graphics g)
	{

	}

	private static void updateGameplay(Graphics g, int message)
	{
		switch (message) {
			case MESSAGE_CONSTRUCTOR:
				setSoftKeys(TEXT.SOFTKEY_MENU, SOFTKEY_NONE);
				playSound(SOUND_M_TITLE);
				levelMapWidth = GameLib.getLayerWidth(MAP_MAP1);
				levelMapHeight = GameLib.getLayerHeight(MAP_MAP1);
				GameLib.initializeCircularBuffer();
				GameLib.drawCircularBuffer(MAP_SEA, sprites[SPRITE_TILE_SEA], cameraX, cameraY);
				GameLib.drawCircularBuffer(MAP_MAP1, sprites[SPRITE_TILE_MAP1], cameraX, cameraY);
				break;

			case MESSAGE_UPDATE:
				hero.update();
				for (int i = 0; i < actors.length; i++) {
					if (actors[i] != null) {
						actors[i].update();
					}
				}

				// Camera
				// cameraX = hero.getX() - (getScreenWidth() >> 1);
				// cameraY = hero.getY() - (getScreenHeight() >> 1);

				// NOTE update camera by Hoang, move from Actor to Game by vinh
				// TODO Unrem later
				int hX = hero.getX();
				int hY = hero.getY();

				int screen_w_2_5 = GameLib.getScreenWidth()*2/5;
				int screen_w_3_5 = GameLib.getScreenWidth()*3/5;
				int screen_h_2_5 = GameLib.getScreenHeight()*2/5;
				int screen_h_3_5 = GameLib.getScreenHeight()*3/5;

				if (hX < cameraX + screen_w_2_5)
					cameraX -= ACTOR_HERO_MOVE_SPEED;
				else if (hX > cameraX + screen_w_3_5)
					cameraX += ACTOR_HERO_MOVE_SPEED;

				if (hY < cameraY + screen_h_2_5)
					cameraY -= ACTOR_HERO_MOVE_SPEED;
				else if (hY > cameraY + screen_h_3_5)
					cameraY += ACTOR_HERO_MOVE_SPEED;

				if (cameraX < 0)
					cameraX = 0;
				else if (cameraX > levelMapWidth - GameLib.getScreenWidth())
					cameraX = levelMapWidth - GameLib.getScreenWidth();

				if (cameraY < 0)
					cameraY = 0;
				else if (cameraY > levelMapHeight - GameLib.getScreenHeight())
					cameraY = levelMapHeight - GameLib.getScreenHeight();

				if (LEFT_SOFT_KEY) {
					setGameState(STATE_INGAME_MENU);
				}

				break;

			case MESSAGE_PAINT:
				setClip(0, 0, getScreenWidth(), getScreenHeight());
				paintGamePlayBackGround(g);					// Background

				hero.render(g);
				for (int i = 0; i < actors.length; i++) {	// Actor
					if (actors[i] != null) {
						actors[i].render(g);
					}
				}
				paintGamePlayHUD(g);						// Interface
				break;

			case MESSAGE_DESTRUCTOR:
				break;

			case MESSAGE_SHOWNOTIFY:
				setGameState(STATE_INGAME_MENU);
				break;
		}
	}