
public interface IRecordStore
{
	public static final int RECORD_LANGUAGE					= 0;
	public static final int RECORD_SOUND_ENABLED			= RECORD_LANGUAGE + 1;
	public static final int RECORD_MUSIC_ENABLED			= RECORD_SOUND_ENABLED + 1;
	public static final int RECORD_SOUND_LEVEL				= RECORD_MUSIC_ENABLED + 1;
	public static final int RECORD_VIBRATION_ENABLED		= RECORD_SOUND_LEVEL + 1;
	public static final int RECORD_ACCELEROMETER_ENABLED	= RECORD_VIBRATION_ENABLED + 1;

	public static final int RECORD_LEVEL					= RECORD_ACCELEROMETER_ENABLED + 1;
	public static final int RECORD_HERO_LEVEL				= RECORD_LEVEL + 1;
	public static final int RECORD_SCORE					= RECORD_HERO_LEVEL + 1;

	public static final String RECORD_NAME					= "feeding_frenzy";
	public static final int RECORD_SIZE						= RECORD_SCORE + 2;	// Score: 2 bytes
}
