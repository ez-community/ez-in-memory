
	///*********************************************************************
	///* Actor_Initialize.h
	///*********************************************************************

	public void initialize()
	{
		switch (type) {
			case ACTOR_HERO:
				this.setFlags(ACTOR_FLAG_ANIM_LOOP);
				this.x = Game.getScreenWidth() >> 1;
				this.y = Game.getScreenHeight() >> 1;
				break;

			case ACTOR_FISH_LEVEL1:
			case ACTOR_FISH_LEVEL2:
			case ACTOR_FISH_LEVEL3:
			case ACTOR_FISH_LEVEL4:
			case ACTOR_BOSS:
				this.setFlags(ACTOR_FLAG_INVISIBLE | ACTOR_FLAG_ANIM_LOOP);
				this.initializeFish();
				break;

			case ACTOR_PEARL:
				break;

			case ACTOR_BONUS:
				break;

			case ACTOR_OBJECT:
				break;

			case ACTOR_FROTHY_WATER:
				break;
		}
	}
