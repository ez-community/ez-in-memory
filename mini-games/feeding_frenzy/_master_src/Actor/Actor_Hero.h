
	///*********************************************************************
	///* Actor_Hero.h
	///*********************************************************************

	public void updateHero()
	{
		// NOTE Temporary disable moving camera while wait for game background
		if (GAME_KEY_LEFT_REPEATED) {
			this.x -= ACTOR_HERO_MOVE_SPEED;
			if (this.x < 0)
				this.x = 0;
			setFaceLeft();
		} else if (GAME_KEY_RIGHT_REPEATED) {
			this.x += ACTOR_HERO_MOVE_SPEED;
			if (this.x > Game.levelMapWidth - 20)
				this.x = Game.levelMapWidth - 20;
			setFaceRight();
		} else if (GAME_KEY_UP_REPEATED) {
			this.y -= ACTOR_HERO_MOVE_SPEED;
			if (this.y < 0)
				this.y = 0;
		} else if (GAME_KEY_DOWN_REPEATED) {
			this.y += ACTOR_HERO_MOVE_SPEED;
			if (this.y > Game.levelMapHeight - 20)
				this.y = Game.levelMapHeight - 20;
		}
	}
