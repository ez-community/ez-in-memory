
public interface ILoading
{
	//
	// Loading types
	//
	public static final int LOADING_TYPE_INIT_GAME					= 0;
	public static final int LOADING_TYPE_GAME_PLAY					= LOADING_TYPE_INIT_GAME + 1;
	public static final int LOADING_TYPE_GAME_PLAY_TO_MAIN_MENU		= LOADING_TYPE_GAME_PLAY + 1;

	//
	// Package operations
	//
	public static final int LOADING_PACK_CLOSE				= 0;
	public static final int LOADING_PACK_OPEN_FONT			= LOADING_PACK_CLOSE + 1;
	public static final int LOADING_PACK_OPEN_SPRITE		= LOADING_PACK_OPEN_FONT + 1;
	public static final int LOADING_PACK_OPEN_SPRITE2		= LOADING_PACK_OPEN_SPRITE + 1;
	public static final int LOADING_PACK_OPEN_SPRITE3		= LOADING_PACK_OPEN_SPRITE2 + 1;
	public static final int LOADING_PACK_OPEN_SPRITE4		= LOADING_PACK_OPEN_SPRITE3 + 1;
	public static final int LOADING_PACK_OPEN_TEXT			= LOADING_PACK_OPEN_SPRITE4 + 1;
	// public static final int LOADING_PACK_OPEN_SOUND			= LOADING_PACK_OPEN_TEXT + 1;

	//
	// Resources
	//
	public static final int LOADING_RMS						= LOADING_PACK_OPEN_TEXT + 1;
	public static final int LOADING_FONT					= LOADING_RMS + 1;
	public static final int LOADING_SPRITE_LOGO				= LOADING_FONT + 1;
	public static final int LOADING_TEXT					= LOADING_SPRITE_LOGO + 1;
	public static final int LOADING_SPRITE_MENU				= LOADING_TEXT + 1;
	public static final int LOADING_SOUND					= LOADING_SPRITE_MENU + 1;
	public static final int LOADING_SPRITE_SPLASH			= LOADING_SOUND + 1;
	public static final int LOADING_SPRITE_ITEMS			= LOADING_SPRITE_SPLASH + 1;
	public static final int LOADING_SPRITE_HERO				= LOADING_SPRITE_ITEMS + 1;
	public static final int LOADING_SPRITE_FISH				= LOADING_SPRITE_HERO + 1;
	public static final int LOADING_SPRITE_BOSS				= LOADING_SPRITE_FISH + 1;
	public static final int LOADING_INIT_ACTOR				= LOADING_SPRITE_BOSS + 1;
	public static final int LOADING_SPRITE_TILESET			= LOADING_INIT_ACTOR + 1;
	public static final int LOADING_MAP_LAYER				= LOADING_SPRITE_TILESET + 1;
	public static final int LOADING_FREE_GAME_PLAY			= LOADING_MAP_LAYER + 1;

	//
	// Define loading steps for each loading type
	//
	public static final int[] LOADING_STEPS_INIT_GAME = {
		LOADING_RMS,
		LOADING_PACK_OPEN_SPRITE,
		LOADING_SPRITE_LOGO,
		LOADING_SPRITE_MENU,
		LOADING_SPRITE_SPLASH,
		LOADING_PACK_CLOSE,
		LOADING_PACK_OPEN_FONT,
		LOADING_FONT,
		LOADING_PACK_CLOSE,
		LOADING_PACK_OPEN_TEXT,
		LOADING_TEXT,
		LOADING_PACK_CLOSE,
		LOADING_SOUND,
	};

	public static final int[] LOADING_STEPS_GAMEPLAY = {
		LOADING_PACK_OPEN_SPRITE2,
		LOADING_SPRITE_ITEMS,
		LOADING_PACK_CLOSE,
		LOADING_PACK_OPEN_SPRITE3,
		LOADING_SPRITE_HERO,
		LOADING_SPRITE_FISH,
		LOADING_SPRITE_BOSS,
		LOADING_PACK_CLOSE,
		LOADING_INIT_ACTOR,
		LOADING_PACK_OPEN_SPRITE4,
		LOADING_SPRITE_TILESET,
		LOADING_PACK_CLOSE,
		LOADING_MAP_LAYER,
	};

	public static final int[] LOADING_STEPS_GAMEPLAY_TO_MAIN_MENU = {
		LOADING_FREE_GAME_PLAY,
		LOADING_PACK_OPEN_SPRITE,
		LOADING_SPRITE_SPLASH,
		LOADING_PACK_CLOSE,
	};
}
