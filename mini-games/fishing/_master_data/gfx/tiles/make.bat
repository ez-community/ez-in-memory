
@echo off

echo Export tileset sprites...
for /f %%i in ('dir /b *.sprite') do (
	echo Exporting file: %%i
	%SPRITE_EDITOR% J2ME ./%%i %1 %2
)
