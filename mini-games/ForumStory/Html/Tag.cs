﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Html
{
	public class Tag
	{
		public const string NODE_TITLE = "title";
		public const string NODE_POST = "div";
		public const string POST_ATTRIBUE_ID = "post_message_";	//post_message_15462778

		public const string META_ATTRIBUE_DESCRIPTION = "description";
		public const string META_ATTRIBUE_CONTENT = "content";

		//
		// Page link
		//
		public const string PAGE_LINK_NODE = "a";
		public const string PAGE_LINK_CLASS = "smallfont";

		//
		// Post link
		//
		public const string POST_LINK_NODE = "a";
		public const string POST_LINK_HREF_CONTAINT = "showpost.php";
	}
}
