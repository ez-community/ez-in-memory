﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Xml;
using HtmlAgilityPack;

namespace ForumStory.Html
{
	public interface IWebPage
	{
		HtmlNodeCollection GetPost();
	}
}
