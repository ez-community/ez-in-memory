
import javax.microedition.midlet.MIDlet;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import java.util.Vector; 

public class Game extends GameLib implements IDefines, IState, ILoading, IRecordStore, DATA, IActorType
{
	private static int lastState, currentState, nextState;
	private static boolean isExitCurrentState, isEnterNextState;
	private static int selectedMenuIndex;
	private static int selectedMenuItem;
	private static FontSprite[] fonts;
	private static GameSprite[] sprites;
	private static ImagePack pack;
	private static Setting setting;
	private static String[] bufferText;
	private static int bufferTextOffset;
	private static Image transparentImageMask;
	private static IAP sms = new IAP();

	private static Actor[] actors;

	public Game(MIDlet midlet)
	{
		super(midlet);
		isExitCurrentState = false;
		isEnterNextState = true;
		soundPlayer = new SoundPlayer();
	}

	// @Override
	public void gameUpdate()
	{
		if (isExitCurrentState) {
			sendGameMessage(MESSAGE_DESTRUCTOR);
			isExitCurrentState = false;
			isEnterNextState = true;
			lastState = currentState;
			currentState = nextState;

		} else if (isEnterNextState) {
			sendGameMessage(MESSAGE_CONSTRUCTOR);
			isEnterNextState = false;
			frameCounter = 0;

		} else {
			sendGameMessage(MESSAGE_UPDATE);
			sendGameMessage(MESSAGE_PAINT);
			paintSoftKeys(graphics);
		}
	}

	// @Override
	protected void showNotify()
	{
		super.showNotify();
		sendGameMessage(MESSAGE_SHOWNOTIFY);
	}

	// @Override
	protected void hideNotify()
	{
		super.hideNotify();
		sendGameMessage(MESSAGE_HIDENOTIFY);
	}

	/**
	 * Create a game thread and start it.
	 */
	public void start()
	{
		Package.setMIDlet(midlet);
		setGameState(STATE_LOADING_INIT_GAME);

		Thread gameThread = new Thread(this);
		gameThread.start();
	}

	public static void setGameState(int state)
	{
		nextState = state;
		isExitCurrentState = true;
		#if _DEBUG
		printStateName(currentState, nextState);
		#endif
	}

	public void sendGameMessage(int message)
	{
		switch(currentState) {
			case STATE_LOADING_INIT_GAME:
				updateLoading(graphics, message, LOADING_TYPE_INIT_GAME);
				break;
			case STATE_LOGO_SCREEN:
				updateLogoScreen(graphics, message);
				break;
			case STATE_LOADING_TO_MAIN_MENU:
				updateLoading(graphics, message, LOADING_TYPE_MAIN_MENU);
				break;
			case STATE_SPLASH_SCREEN:
				updateSplashScreen(graphics, message);
				break;
			case STATE_MAIN_MENU:
				updateMenu(graphics, message, MENU_MAIN);
				break;
			case STATE_SETTINGS:
				updateMenuSetting(graphics, message);
				break;
			case STATE_HELP:
				updateHelpScreen(graphics, message);
				break;
			 case STATE_ABOUT:
				updateAboutScreen(graphics, message);
				break;
			case STATE_SELECTGIRL:
				updateSelectGirlScreen(graphics, message);
				break;
			case STATE_LOADING_GAME_PLAY:
				updateLoading(graphics, message, LOADING_TYPE_GAME_PLAY);
				break;
			case STATE_GAME_PLAY:
				updateGameplay(graphics, message);
				break;
			case STATE_INGAME_MENU:
				updateMenu(graphics, message, MENU_INGAME);
				break;
			/*case STATE_EXIT_CONFIRM:
				updateConfirmScreen(graphics, message, CONFIRM_SCREEN_EXIT_GAME);
				break;
			*/
			case STATE_GO_TO_MAIN_MENU_QUESTION:
				updateConfirmScreen(graphics, message, CONFIRM_SCREEN_EXIT_TO_MAIN_MENU);
				break;
			/* case STATE_GAME_COMPLETE:
				updateGameCompleted(graphics, message);
				break;
			case STATE_HIGHT_SCORE:
				updateHightScore(graphics, message);
				break;
			case STATE_UPLOAD_SCORE:
				updateUploadingScore(graphics, message);
				break; */
			case STATE_LOADING_GAMEPLAY_TO_MAIN_MENU:
				updateLoading(graphics, message, LOADING_TYPE_GAME_PLAY_TO_MAIN_MENU);
				break;
			/* case STATE_EXIT:
				setting.save();
				super.exit();
				break; */
		}	//switch (currentState)
	}

	#include "Game/Game_Paint.h"
	#include "Game/Game_Load.h"
	#include "Game/Game_LogoScreen.h"
	#include "Game/Game_ConfirmScreen.h"
	#include "Game/Game_Menu.h"
	#include "Game/Game_Help.h"
	#include "Game/Game_AboutScreen.h"
	#include "Game/Game_SelectGirlScreen.h"
	#include "Game/Game_GamePlay.h"
	// #include "Game/Game_Complete.h"
	// #if USE_CHEATS
		// #include "Game_Cheat.h"
	// #endif
	#include "Game/Game_Sound.h"
	#if _DEBUG
		#include "Game/Game_Debug.h"
	#endif
}
