
	///*********************************************************************
	///* Game_GamePlay.h
	///*********************************************************************

	public static int 		HEIGHT_FRAME_ITEM 	= 35;
	public static int 		BOTTOM_HEIGHT_PAINT	= 50;
	public static boolean 	isPlayingGame 		= false;
	public static int 		counterValue		= -1;
	public static int 		resultOfHero		= 0;
	public static int 		resultOfGamePlay	= -2;
	public static int 		imageIndex = 0;
	public static boolean   isSetResult = false;
	public static boolean   isWin = false;
	public static Image 	backGround = null;
	public static int 		counterFrameToPaintText = 0;
	public static int 		positionHeightPaintTex 	= 0; 
	public static boolean 	isPaintText	= false;
	public static String[] 	wrapTextToPaint;
	public static byte 		numberPlayEnable = -1;
	public static boolean	isPressKey123 = false;
	public static boolean 	isConfirmSendSMS	= true;
	public static ImagePack ip = null;
	public static int subState = SUBSTATE_INGAME;
	
	public static void updateGame()
	{
		if(numberPlayEnable > 0) {
			if(resultOfGamePlay == -1 && isPressKey123) {
				isPressKey123 = false;
				numberPlayEnable--;
				if(numberPlayEnable <= 0)
					subState = SUBSTATE_SMSMESSAGE;
				setting.writeByte(RECORD_NUMBER_PLAYER_ENABLE, (byte)(numberPlayEnable));
				setting.save();
			}
		}
		
		if(resultOfGamePlay == 1 && !isSetResult)
		{
			isSetResult = true;
			if(imageIndex < MARIA_OZAWA_DAT_MAX - 1)
			{
			imageIndex++;
			backGround = null;
			isPaintText = true;
			}
			else
				setGameState(STATE_SELECTGIRL);
		}
		
		if(isPlayingGame)
			counterValue = (counterValue + 1)%9;
			
		if (GAME_KEY_FIRE)
		{
			if(numberPlayEnable <= 0)
				setGameState(STATE_SELECTGIRL);
			else
			{
			isPlayingGame = true;
			isSetResult = false;
			counterValue = -1;
			resultOfGamePlay = -2;
			}
		}
		else if(Game.isKeyPressed(Game.DK_NUM1)
				|| Game.isKeyPressed(Game.DK_NUM2)
				|| Game.isKeyPressed(Game.DK_NUM3)
				)
		{
			if(isPlayingGame)
			{
				isPressKey123 = true;
				if(Game.isKeyPressed(Game.DK_NUM1))
					resultOfHero = 1;
				else if(Game.isKeyPressed(Game.DK_NUM2))
					resultOfHero = 2;
				else if(Game.isKeyPressed(Game.DK_NUM3))
					resultOfHero = 3;
			}
			isPlayingGame = false;			
		}
	}

	private static void paintTextInGame(Graphics g)
	{
		if(isPaintText)
		{
			isPaintText = false;
			counterFrameToPaintText = 0;
			positionHeightPaintTex = BOTTOM_HEIGHT_PAINT;
			String textToPaint = "";
			if(numberPlayEnable <= 0)
				textToPaint = Package.getText(TEXT.DIALOG_2);
			else
				textToPaint = "DAY LA CAU SO " + imageIndex;
				//textToPaint = Package.getText(TEXT.DIALOG_1);
			wrapTextToPaint = fonts[FONT_NORMAL].wrapText(textToPaint, 100);
		}		
		if(counterFrameToPaintText < 200)
		{
			counterFrameToPaintText++;
			fonts[FONT_NORMAL].setPalette(PALETTE_FONT_NORMAL_BLACK);
			if(counterFrameToPaintText < BOTTOM_HEIGHT_PAINT)
			{
				sprites[SPRITE_OBJECT].paintFrame(g, OBJECT_FRAME_DIALOG, getScreenWidth() >> 1,
					getScreenHeight() - counterFrameToPaintText, Graphics.HCENTER | Graphics.TOP);
				fonts[FONT_NORMAL].drawPage(g, wrapTextToPaint, getScreenWidth() >> 1,
					getScreenHeight() - counterFrameToPaintText, Graphics.HCENTER | Graphics.TOP);
			}
			else
			{
				sprites[SPRITE_OBJECT].paintFrame(g, OBJECT_FRAME_DIALOG, getScreenWidth() >> 1,
					getScreenHeight() - BOTTOM_HEIGHT_PAINT, Graphics.HCENTER | Graphics.TOP);
				fonts[FONT_NORMAL].drawPage(g, wrapTextToPaint, getScreenWidth() >> 1,
					getScreenHeight() - BOTTOM_HEIGHT_PAINT, Graphics.HCENTER | Graphics.TOP);
			}
		}
		else if(positionHeightPaintTex > 0)
		{			
			positionHeightPaintTex--;
			fonts[FONT_NORMAL].setPalette(PALETTE_FONT_NORMAL_BLACK);
			
			sprites[SPRITE_OBJECT].paintFrame(g, OBJECT_FRAME_DIALOG, getScreenWidth() >> 1,
					getScreenHeight() - positionHeightPaintTex, Graphics.HCENTER | Graphics.TOP);
			fonts[FONT_NORMAL].drawPage(g, wrapTextToPaint, getScreenWidth() >> 1,
					getScreenHeight() - positionHeightPaintTex, Graphics.HCENTER | Graphics.TOP);
		}
	}
	
	public static void getDataGirl(final int i){
		if(i !=  1)
		{
			if(!ImageRecordStore.checkImageRecordExist(i+"_dat"))
			{
				isDownloading = true;
				subState = SUBSTATE_DOWNLOADING;
				Thread t = new Thread() {
					public void run() {
						wc.getDataGirl(i);
					}
				};
				t.start();
			}
			else
				downloadDataGirlFinish();
		}
	}
	
	public static void downloadDataGirlFinish(){
		isDownloading = false;
		subState = SUBSTATE_INGAME;
		ip.packageOpen();
	}
	
	private static void paintGamePlayBackGround(Graphics g)
	{
		if(backGround == null)
		{
			backGround = ip.getImage(imageIndex);						
		}
		if(backGround != null )
			g.drawImage(backGround, getScreenWidth() >> 1, getScreenHeight() >> 1, Graphics.HCENTER | Graphics.VCENTER);
	}

	private static void paintGamePlay(Graphics g)
	{	
		if(isPlayingGame)
		{
			switch(counterValue/3)
			{
				case 0: 
					//paint bao			
					sprites[SPRITE_INGAMEICON1].paintFrame(g, INGAMEICON1_FRAME_BAO_PAUSE,
							getScreenWidth(), getScreenHeight() - BOTTOM_HEIGHT_PAINT, Graphics.RIGHT | Graphics.BOTTOM);
					break;
				case 1:
					//paint bua
					sprites[SPRITE_INGAMEICON1].paintFrame(g, INGAMEICON1_FRAME_BUA_PAUSE,
							getScreenWidth(), getScreenHeight() - BOTTOM_HEIGHT_PAINT, Graphics.RIGHT | Graphics.BOTTOM);
					break;
				case 2:
					//paint keo
					sprites[SPRITE_INGAMEICON1].paintFrame(g, INGAMEICON1_FRAME_KEO_PAUSE,
							getScreenWidth(), getScreenHeight() - BOTTOM_HEIGHT_PAINT, Graphics.RIGHT | Graphics.BOTTOM);
					break;					
			}
		}
		else
		{
			if(counterValue == -1)
			{
				sprites[SPRITE_INGAMEICON1].paintFrame(g, INGAMEICON1_FRAME_BAO_PAUSE,
							getScreenWidth(), getScreenHeight() - BOTTOM_HEIGHT_PAINT, Graphics.RIGHT | Graphics.BOTTOM);
			}
			else
			{
				switch(counterValue/3)
				{
					case 0: 					
						switch(resultOfHero)
						{
							case 1: 								
								resultOfGamePlay = 0;
								actors[ACTOR_FACE].setAnimation(INGAMEICON1_ANIMATION_BAO_HOA_BAO, getScreenWidth(), 
												getScreenHeight() - BOTTOM_HEIGHT_PAINT, Graphics.RIGHT | Graphics.BOTTOM);
								break;
							case 2:
								resultOfGamePlay = -1;
								actors[ACTOR_FACE].setAnimation(INGAMEICON1_ANIMATION_BAO_KHOC_BUA, getScreenWidth(), 
												getScreenHeight() - BOTTOM_HEIGHT_PAINT, Graphics.RIGHT | Graphics.BOTTOM);								
								break;
							case 3:
								resultOfGamePlay = 1;								
								actors[ACTOR_FACE].setAnimation(INGAMEICON1_ANIMATION_BAO_CUOI_KEO, getScreenWidth(), 
												getScreenHeight() - BOTTOM_HEIGHT_PAINT, Graphics.RIGHT | Graphics.BOTTOM);								
								break;
						}					
						break;
					case 1:
						switch(resultOfHero)
						{
							case 1:
								resultOfGamePlay = 1;
								actors[ACTOR_FACE].setAnimation(INGAMEICON1_ANIMATION_BUA_CUOI_BAO, getScreenWidth(), 
												getScreenHeight() - BOTTOM_HEIGHT_PAINT, Graphics.RIGHT | Graphics.BOTTOM);								
								break;
							case 2: 
								resultOfGamePlay = 0;
								actors[ACTOR_FACE].setAnimation(INGAMEICON1_ANIMATION_BUA_HOA_BUA, getScreenWidth(), 
												getScreenHeight() - BOTTOM_HEIGHT_PAINT, Graphics.RIGHT | Graphics.BOTTOM);								
								break;
							case 3: 
								resultOfGamePlay = -1;
								actors[ACTOR_FACE].setAnimation(INGAMEICON1_ANIMATION_BUA_KHOC_KEO, getScreenWidth(), 
												getScreenHeight() - BOTTOM_HEIGHT_PAINT, Graphics.RIGHT | Graphics.BOTTOM);
								break;
						}					
						break;
					case 2:
						switch(resultOfHero)
						{
							case 1: 
								resultOfGamePlay = -1;
								actors[ACTOR_FACE].setAnimation(INGAMEICON1_ANIMATION_KEO_KHOC_BAO, getScreenWidth(), 
												getScreenHeight() - BOTTOM_HEIGHT_PAINT, Graphics.RIGHT | Graphics.BOTTOM);
								break;
							case 2: 
								resultOfGamePlay = 1;
								actors[ACTOR_FACE].setAnimation(INGAMEICON1_ANIMATION_KEO_CUOI_BUA, getScreenWidth(), 
												getScreenHeight() - BOTTOM_HEIGHT_PAINT, Graphics.RIGHT | Graphics.BOTTOM);
								break;
							case 3:	
								resultOfGamePlay = 0;
								actors[ACTOR_FACE].setAnimation(INGAMEICON1_ANIMATION_KEO_HOA_KEO, getScreenWidth(), 
												getScreenHeight() - BOTTOM_HEIGHT_PAINT, Graphics.RIGHT | Graphics.BOTTOM);
								break;
						}					
						break;
				}
				actors[ACTOR_FACE].render(g);
			}
		}		
	}
	
	private static void paintTheStatus(Graphics g)
	{
		int palette = PALETTE_FONT_NORMAL_BLACK;
		fonts[FONT_NORMAL].setPalette(palette);
		fonts[FONT_NORMAL].drawString(g,numberPlayEnable + "" , getScreenWidth() - 20 , 20, Graphics.LEFT | Graphics.VCENTER);

		switch(resultOfHero)
		{
			case 0: 
				sprites[SPRITE_INGAMEICON].paintFrame(g, INGAMEICON_FRAME_NORMAL,
						0, getScreenHeight() - BOTTOM_HEIGHT_PAINT, Graphics.LEFT | Graphics.BOTTOM);
			case 1: 							
				sprites[SPRITE_INGAMEICON].paintFrame(g, INGAMEICON_FRAME_BAO,
						0, getScreenHeight() - BOTTOM_HEIGHT_PAINT, Graphics.LEFT | Graphics.BOTTOM);
				break;
			case 2: 							
				sprites[SPRITE_INGAMEICON].paintFrame(g, INGAMEICON_FRAME_BUA,
						0, getScreenHeight() - BOTTOM_HEIGHT_PAINT, Graphics.LEFT | Graphics.BOTTOM);
				break;
			case 3: 							
				sprites[SPRITE_INGAMEICON].paintFrame(g, INGAMEICON_FRAME_KEO,
						0, getScreenHeight() - BOTTOM_HEIGHT_PAINT, Graphics.LEFT | Graphics.BOTTOM);
				break;
		}
	}
	
	private static void paintSMSMessage(Graphics g)
	{
		g.setColor(0xffffff);
		fillRect(g, transparentImageMask, 0, 0, getScreenWidth(), getScreenHeight());
		g.fillRect(0, (getScreenHeight() >> 1)  - 50, getScreenWidth() , 100);
		int palette = PALETTE_FONT_NORMAL_BLACK;
		fonts[FONT_NORMAL].setPalette(palette);
		String textToPaint = Package.getText(TEXT.DIALOG_3);
		fonts[FONT_NORMAL].drawString(g,textToPaint, getScreenWidth() >> 1, (getScreenHeight() >> 1) - 20, Graphics.HCENTER | Graphics.VCENTER);
		fonts[FONT_NORMAL].drawString(g,"YES", (getScreenWidth() >> 1) - 40, (getScreenHeight() >> 1) + 20, Graphics.HCENTER | Graphics.VCENTER);
		fonts[FONT_NORMAL].drawString(g,"NO", (getScreenWidth() >> 1) + 40, (getScreenHeight() >> 1) + 20, Graphics.HCENTER | Graphics.VCENTER);
		g.setColor(0x989898);
		if(isConfirmSendSMS)
			g.drawRect((getScreenWidth() >> 1) - 40 - 20, (getScreenHeight() >> 1) + 20 - 10, 40, 20);
		else
			g.drawRect((getScreenWidth() >> 1) + 40 - 20, (getScreenHeight() >> 1) + 20 - 10, 40, 20);
	}

	private static void updateGameplay(Graphics g, int message)
	{
		switch (message) {
			case MESSAGE_CONSTRUCTOR:
				setSoftKeys(TEXT.SOFTKEY_MENU, SOFTKEY_NONE);
				playSound(SOUND_M_TITLE);
				imageIndex = 0;
				backGround = null;
				isSetResult = false;
				isPlayingGame = false;
				isPaintText = true;
				resultOfGamePlay = -2;
				resultOfHero = 0;
				counterValue = -1;
				if(setting.readByte(RECORD_NUMBER_PLAYER_ENABLE) == 0) {
					numberPlayEnable = 3;
					setting.writeByte(RECORD_NUMBER_PLAYER_ENABLE, (byte)(numberPlayEnable));
					setting.save();
				} else {
					numberPlayEnable = setting.readByte(RECORD_NUMBER_PLAYER_ENABLE);
				}
				System.out.println(">>>>>>>>>>>>>>>> numberPlayEnable = " + numberPlayEnable);
				ip = new ImagePack(indexSelect+1+"");
				getDataGirl(indexSelect+1);
				break;

			case MESSAGE_UPDATE:
				switch(subState)
				{
					case SUBSTATE_INGAME:
						if (LEFT_SOFT_KEY) {
							setGameState(STATE_SELECTGIRL);
						} else if(RIGHT_SOFT_KEY){
					
							setGameState(STATE_INGAME_MENU);
						}
						updateGame();
						break;
					case SUBSTATE_DOWNLOADING:
						break;
					case SUBSTATE_SMSMESSAGE:
						if(GAME_KEY_LEFT_PRESSED || GAME_KEY_RIGHT_PRESSED)
							isConfirmSendSMS = !isConfirmSendSMS;
						else if(GAME_KEY_SELECT){
							subState = SUBSTATE_INGAME;
							if(isConfirmSendSMS)
								sms.send(SMS_TYPE_BUY_NUMBER_PLAY);
						}
						break;
				}
				break;

			case MESSAGE_PAINT:
				setClip(0, 0, getScreenWidth(), getScreenHeight());
				switch(subState)
				{
					case SUBSTATE_INGAME:
						paintGamePlayBackGround(g);
						paintTextInGame(g);
						paintGamePlay(g);
						paintTheStatus(g);
						break;
					case SUBSTATE_DOWNLOADING:
						paintLoading(g);
						break;
					case SUBSTATE_SMSMESSAGE:
						paintGamePlayBackGround(g);
						paintTextInGame(g);
						paintGamePlay(g);
						paintTheStatus(g);
						paintSMSMessage(g);
						break;
				}
				break;

			case MESSAGE_DESTRUCTOR:
				backGround = null;
				isSetResult = false;
				isPlayingGame = false;
				resultOfGamePlay = -2;
				counterValue = -1;
				break;

			case MESSAGE_SHOWNOTIFY:
				//setGameState(STATE_INGAME_MENU);
				break;
		}
	}