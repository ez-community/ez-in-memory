
	///*********************************************************************
	///* Game_ConfirmScreen.h
	///*********************************************************************

	//
	// Confirm screen types
	//
	private static final int CONFIRM_SCREEN_SOUND 				= 0;
	private static final int CONFIRM_SCREEN_EXIT_GAME 			= 1;
	private static final int CONFIRM_SCREEN_EXIT_TO_MAIN_MENU 	= 2;

	// Confirm screen question message
	private static final int[] CONFIRM_SCREEN_TITLES = {
		TEXT.SOUND_CONFIRM,
		TEXT.EXIT_GAME_CONFIRM,
		TEXT.EXIT_TO_MAIN_MENU_CONFIRM
	};

	private static void paintConfirmScreen(Graphics g, int type)
	{
		int dy = sprites[SPRITE_MENU].getFrameHeight(MENU_FRAME_BUTTON);
		dy += (dy >> 1);
		int x = getScreenWidth() >> 1;
		int y = getScreenHeight() - (dy * ((CONFIRM_SCREEN_TITLES.length) + 1));
		y -= (y >> 2);

		fillRect(0x000000);
		if (type == CONFIRM_SCREEN_EXIT_GAME) {
			sprites[SPRITE_SPLASH].paintFrame(g, SPLASH_FRAME_240X320,
						getScreenWidth() >> 1, getScreenHeight() >> 1, Graphics.HCENTER | Graphics.VCENTER);
		} else if (type == CONFIRM_SCREEN_EXIT_TO_MAIN_MENU) {
			// paintGamePlayBackGround(g);
			sprites[SPRITE_SPLASH].paintFrame(g, SPLASH_FRAME_240X320,
						getScreenWidth() >> 1, getScreenHeight() >> 1, Graphics.HCENTER | Graphics.VCENTER);
		}
		// fillRect(g, transparentImageMask, 0, 0, getScreenWidth(), getScreenHeight());

		fonts[FONT_NORMAL].setPalette(PALETTE_FONT_NORMAL_WHITE);
		String title = Package.getText(CONFIRM_SCREEN_TITLES[type]);
		fonts[FONT_NORMAL].drawString(g, title, x, y, Graphics.HCENTER | Graphics.VCENTER);

		int palette = (selectedMenuItem == TEXT.COMMON_YES) ? PALETTE_FONT_NORMAL_BLACK : PALETTE_FONT_NORMAL_WHITE;
		int frame = (selectedMenuItem == TEXT.COMMON_YES) ? MENU_FRAME_BUTTON_HOVER : MENU_FRAME_BUTTON;
		sprites[SPRITE_MENU].paintFrame(g, frame, x, y + dy, Graphics.HCENTER | Graphics.VCENTER);
		fonts[FONT_NORMAL].setPalette(palette);
		fonts[FONT_NORMAL].drawString(g, Package.getText(TEXT.COMMON_YES),
			x, y + dy, Graphics.HCENTER | Graphics.VCENTER);

		palette = (selectedMenuItem == TEXT.COMMON_NO) ? PALETTE_FONT_NORMAL_BLACK : PALETTE_FONT_NORMAL_WHITE;
		frame = (selectedMenuItem == TEXT.COMMON_NO) ? MENU_FRAME_BUTTON_HOVER : MENU_FRAME_BUTTON;
		sprites[SPRITE_MENU].paintFrame(g, frame, x, y + (dy << 1), Graphics.HCENTER | Graphics.VCENTER);
		fonts[FONT_NORMAL].setPalette(palette);
		fonts[FONT_NORMAL].drawString(g, Package.getText(TEXT.COMMON_NO),
			x, y + (dy << 1), Graphics.HCENTER | Graphics.VCENTER);
	}

	private static void updateConfirmScreen(Graphics g, int message, int type)
	{
		switch (message) {
			case MESSAGE_CONSTRUCTOR:
				selectedMenuItem = TEXT.COMMON_NO;
				transparentImageMask = createTransparentImage(TRANSPARENT_IMAGE_MASK_SIZE,
					TRANSPARENT_IMAGE_MASK_SIZE, 0x000000, 0x80);
				setSoftKeys(TEXT.SOFTKEY_SELECT, SOFTKEY_NONE);
				break;

			case MESSAGE_UPDATE:
				if (GAME_KEY_UP_PRESSED) {
					selectedMenuItem = TEXT.COMMON_YES;
				} else if (GAME_KEY_DOWN_PRESSED) {
					selectedMenuItem = TEXT.COMMON_NO;
				} else if (GAME_KEY_SELECT) {
					switch (type) {
						case CONFIRM_SCREEN_SOUND:
							if (selectedMenuItem == TEXT.COMMON_YES) {
								setEnableSound(true);
							}
							setGameState(STATE_LOADING_TO_MAIN_MENU);
							break;

						case CONFIRM_SCREEN_EXIT_GAME:
							if (selectedMenuItem == TEXT.COMMON_YES) {
								setGameState(STATE_EXIT);
							} else {
								setGameState(lastState);
							}
							break;

						case CONFIRM_SCREEN_EXIT_TO_MAIN_MENU:
							if (selectedMenuItem == TEXT.COMMON_YES) {
								//setGameState(STATE_LOADING_TO_MAIN_MENU);
								setGameState(STATE_LOADING_GAMEPLAY_TO_MAIN_MENU);
							} else {
								setGameState(lastState);
							}
							break;
					}	// switch(type)
				}	// else if (GAME_KEY_FIRE)
				break;	// case MESSAGE_UPDATE

			case MESSAGE_PAINT:
				paintConfirmScreen(g, type);
				break;

			case MESSAGE_DESTRUCTOR:
				break;
		}	//switch (message)
	}
