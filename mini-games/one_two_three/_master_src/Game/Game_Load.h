
	///*********************************************************************
	///* Game_Load.h
	///*********************************************************************

	private static void loadResource(int step)
	{
		#if _DEBUG
		printLoadingStepName(step);
		#endif
		switch (step) {
			case LOADING_RMS:
				setting = new Setting(RECORD_NAME, RECORD_SIZE);
				break;

			case LOADING_PACK_OPEN_SPRITE:
				Package.open(PACK_SPRITE);
				break;

			/* case LOADING_PACK_OPEN_SPRITE2:
				Package.open(PACK_SPRITE2);
				break;

			case LOADING_PACK_OPEN_SPRITE3:
				Package.open(PACK_SPRITE3);
				break; */

			case LOADING_PACK_OPEN_FONT:
				Package.open(PACK_FONT);
				break;

			case LOADING_PACK_OPEN_TEXT:
				Package.open(PACK_TEXT);
				break;

			case LOADING_PACK_CLOSE:
				Package.close();
				break;

			case LOADING_SPRITE_LOGO:
				sprites[SPRITE_LOGO] = Package.loadSprite(SPRITE_LOGO, false);
				break;
				
			case LOADING_SPRITE_SELECT_GIRL:
				//sprites[SPRITE_SELECTGIRL] = Package.loadSprite(SPRITE_SELECTGIRL, false);
				break;

			case LOADING_FONT:
				fonts[FONT_NORMAL] = Package.loadFontSprite(FONT_NORMAL, FONT_NORMAL_MAPPING, true);
				// Cache all palettes
				for (int i = 0; i < fonts[FONT_NORMAL].getPaletteCount(); i++) {
					fonts[FONT_NORMAL].cache(i);
				}
				fonts[FONT_NORMAL].uncacheRGB();
				break;

			case LOADING_TEXT:
				if (setting.readByte(RECORD_LANGUAGE) == TEXT_EN) {
					Package.loadText(TEXT_EN);
				} else {
					Package.loadText(TEXT_VI);
				}
				break;

			case LOADING_SOUND:
				loadSoundPack();
				break;

			case LOADING_SPRITE_MENU:
				sprites[SPRITE_MENU] = Package.loadSprite(SPRITE_MENU, true);
				sprites[SPRITE_MENU].cache();
				sprites[SPRITE_MENU].uncacheRGB();
				break;

			case LOADING_SPRITE_SPLASH:
				sprites[SPRITE_SPLASH] = Package.loadSprite(SPRITE_SPLASH, true);
				sprites[SPRITE_SPLASH].cache();
				break;
				
			case LOADING_SPRITE_OBJECT:
				sprites[SPRITE_OBJECT] = Package.loadSprite(SPRITE_OBJECT, true);
				sprites[SPRITE_OBJECT].cache();
				break;
			
			case LOADING_SPRITE_INGAME_ICON:
				sprites[SPRITE_INGAMEICON] = Package.loadSprite(SPRITE_INGAMEICON, true);
				sprites[SPRITE_INGAMEICON].cache();
				sprites[SPRITE_INGAMEICON1] = Package.loadSprite(SPRITE_INGAMEICON1, true);
				sprites[SPRITE_INGAMEICON1].cache();
				break;

			case LOADING_INIT_ACTOR:
				actors = new Actor[ACTOR_MAX];
				actors[ACTOR_FACE] = new Actor(ACTOR_FACE, sprites[SPRITE_INGAMEICON1]);
				break;

			case LOADING_FREE_GAME_PLAY:
				break;
		}
	}

	private static int[] loadingSteps;
	private static int loadStep;

	private static void updateLoading(Graphics g, int message, int type)
	{
		switch (message) {
			case MESSAGE_CONSTRUCTOR:
				if (sprites == null && fonts == null) {
					sprites = new GameSprite[SPRITE_MAX];
					fonts = new FontSprite[FONT_MAX];
				}

				switch (type) {
					case LOADING_TYPE_INIT_GAME:
						loadingSteps = LOADING_STEP_INIT_GAME;
						currentTime = System.currentTimeMillis();
						break;
					
					case LOADING_TYPE_MAIN_MENU:
						loadingSteps = LOADING_STEP_MAIN_MENU;
						break;
					
					case LOADING_TYPE_GAME_PLAY:
						loadingSteps = LOADING_STEP_GAMEPLAY;
						break;
					
					case LOADING_TYPE_GAME_PLAY_TO_MAIN_MENU:
						loadingSteps = LOADING_STEP_GAMEPLAY_TO_MAIN_MENU;
						break;
				}
				
				loadStep = 0;

				stopAllSound();
				setSoftKeys(SOFTKEY_NONE, SOFTKEY_NONE);
				break;

			case MESSAGE_UPDATE:
				if (loadStep < loadingSteps.length) {
					loadResource(loadingSteps[loadStep]);
				} else {
					switch (type) {
						case LOADING_TYPE_INIT_GAME:							
							setGameState(STATE_LOGO_SCREEN);
							break;
						
						case LOADING_TYPE_MAIN_MENU:
							setGameState(STATE_SPLASH_SCREEN);
						break;
					
						case LOADING_TYPE_GAME_PLAY:
							setGameState(STATE_GAME_PLAY);
							break;
						
						case LOADING_TYPE_GAME_PLAY_TO_MAIN_MENU:
							setGameState(STATE_MAIN_MENU);
							break;
					}
				}

				loadStep++;
				break;

			case MESSAGE_PAINT:
				if (type == LOADING_TYPE_INIT_GAME && sprites[SPRITE_LOGO] != null) {
					fillRect(0xffffff);
					sprites[SPRITE_LOGO].paintFrame(g, LOGO_FRAME_LOGO,
						getScreenWidth() >> 1, getScreenHeight() >> 1, Graphics.HCENTER | Graphics.VCENTER);
				}
				break;

			case MESSAGE_DESTRUCTOR:
				SYSTEM_GC;
				break;
		}
	}
