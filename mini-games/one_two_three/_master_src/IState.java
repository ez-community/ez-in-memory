
public interface IState
{
	//
	// Handle state messages
	//
	public static final int MESSAGE_CONSTRUCTOR 	= 0;
	public static final int MESSAGE_UPDATE 			= 1;
	public static final int MESSAGE_PAINT 			= 2;
	public static final int MESSAGE_DESTRUCTOR 		= 3;
	public static final int MESSAGE_SHOWNOTIFY 		= 4;
	public static final int MESSAGE_HIDENOTIFY 		= 5;

	//
	// States
	//
	public static final int STATE_EXIT							= -1;
	public static final int STATE_LOADING_INIT_GAME				= 0;
	public static final int STATE_LOGO_SCREEN					= STATE_LOADING_INIT_GAME + 1;
	public static final int STATE_LOADING_TO_MAIN_MENU			= STATE_LOGO_SCREEN + 1;
	public static final int STATE_SPLASH_SCREEN					= STATE_LOADING_TO_MAIN_MENU + 1;
	public static final int STATE_MAIN_MENU 					= STATE_SPLASH_SCREEN + 1;
	public static final int STATE_SETTINGS 						= STATE_MAIN_MENU + 1;
	public static final int STATE_HELP 							= STATE_SETTINGS + 1;
	public static final int STATE_ABOUT 						= STATE_HELP + 1;
	public static final int STATE_SELECTGIRL					= STATE_ABOUT + 1;
	public static final int STATE_LOADING_GAME_PLAY				= STATE_SELECTGIRL + 1;
	public static final int STATE_GAME_PLAY 					= STATE_LOADING_GAME_PLAY + 1;
	public static final int STATE_GAME_DIALOG 					= STATE_GAME_PLAY + 1;
	public static final int STATE_GAME_COMPLETE					= STATE_GAME_DIALOG + 1;
	public static final int STATE_INGAME_MENU 					= STATE_GAME_COMPLETE + 1;
	public static final int STATE_EXIT_GAME_QUESTION			= STATE_INGAME_MENU + 1;
	public static final int STATE_GO_TO_MAIN_MENU_QUESTION		= STATE_EXIT_GAME_QUESTION + 1;
	public static final int STATE_LOADING_SERVER_LIST			= STATE_GO_TO_MAIN_MENU_QUESTION + 1;
	public static final int STATE_SERVER_LIST					= STATE_LOADING_SERVER_LIST + 1;
	public static final int STATE_LOADING_GAMEPLAY_TO_MAIN_MENU	= STATE_LOADING_SERVER_LIST + 1;
	
	public static final int SUBSTATE_SELECTGIRL_SCREEN			= 0;
	public static final int SUBSTATE_INGAME						= 0;
	public static final int SUBSTATE_DOWNLOADING				= SUBSTATE_INGAME + 1;
	public static final int SUBSTATE_SMSMESSAGE					= SUBSTATE_DOWNLOADING + 1;
	
	public static final int SMS_TYPE_DOWNLOADGIRL				= 0;
	public static final int SMS_TYPE_BUY_NUMBER_PLAY			= SMS_TYPE_DOWNLOADGIRL + 1;
}
