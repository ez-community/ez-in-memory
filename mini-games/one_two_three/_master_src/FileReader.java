/*
 *
 * Copyright (c) 2007, Sun Microsystems, Inc.
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of Sun Microsystems nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
import java.io.*;

import java.util.*;

import javax.microedition.io.*;
import javax.microedition.io.file.*;
import javax.microedition.lcdui.*;
import javax.microedition.midlet.*;


/**
 * Demonstration MIDlet for File Connection API. This MIDlet implements simple
 * file browser for the filesystem available to the J2ME applications.
 *
 */
public class FileReader {
	String folderPath = "";
	
	public FileReader()
	{
		//folderPath = "file:///"+getRoots();
		folderPath = System.getProperty("fileconn.dir.photos");;
	}
	private String getRoots() {
	  String root="root";
      Enumeration drives = FileSystemRegistry.listRoots();
      System.out.println("The valid roots found are: ");
      while(drives.hasMoreElements()) {
         root = (String) drives.nextElement();
         System.out.println("\t"+root);
      }
      return root;
   }
	public boolean checkFileExists(String fileName)
	{
		try
		{
			#if USE_JSR75
			FileConnection fc = (FileConnection)Connector.open(folderPath + fileName.trim());			
			if (!fc.exists())
				return false;
			else
				return true;
			#else	//USE_JSR75
			return false;
			#endif	//USE_JSR75
		}
		catch(Exception e) {System.out.println(e); return false;}
	}
	
	
	/**
   * Count files in a directory (including files in all subdirectories)
   * @param directory the directory to start in
   * @return the total number of files
   */
    public int countFilesInDirectory(String pathDirectory)
	{
		int count = 0;
		try	{
			#if USE_JSR75
			FileConnection fc = (FileConnection)Connector.open(folderPath,Connector.READ);
			if(fc == null) System.out.println("fc bang null"); 
			Enumeration listOfFiles = fc.list("*", false);
			if(listOfFiles == null) System.out.println("listOfFiles = null");
			while(listOfFiles.hasMoreElements())
			{
			System.out.println("voday");
				listOfFiles.nextElement();
				count++;
			}
			#endif	//USE_JSR75
		}
		catch(Exception e) {
			e.printStackTrace();
		}
        
		return count;
    }

	
	/*
	* fileName 			: Name of file
	* startOffset		: Offset start to read byte, defautl should be 0 
	* lengthArray		: Number bytes of your image to read, if you don't know number bytes of your image => lenght = 0
	* example 			: ReadFile("file://localhost/root/test.txt", 0, 0);
	*/
    byte[] readFile(String fileName, int startOffset, int lengthArray)
	{
        try {
			#if USE_JSR75
			System.out.println(folderPath + fileName.trim());
            FileConnection fc = (FileConnection)Connector.open(folderPath + fileName.trim());			
            if (!fc.exists()) {
                throw new IOException("File does not exists");
            }
			
            InputStream fis = fc.openInputStream();
			if(lengthArray == 0) lengthArray = 100000;
            byte[] b = new byte[lengthArray];
            int length = fis.read(b, startOffset, lengthArray);	
            fis.close();
            fc.close();
			
			if(length > 0) {
				if(length < b.length) {
					byte[] result = new byte[length];
					for(int i = 0; i < length; i++)
						result[i] = b[i];
					return result;
				}	
				return b;
			}
			else
				return null;
			#else	//USE_JSR75
			return null;
			#endif	//USE_JSR75

        } catch (Exception e) {
			e.printStackTrace();
			return null;
        }
    }
	
	/*
	* fileName 			: Name of file
	* array				: Byte array which contain value of file.	
	*/
	void writeFile(String fileName, byte[] array)
	{
		try	{
			#if USE_JSR75
			FileConnection fc = (FileConnection)Connector.open(folderPath + fileName.trim());			
			if (!fc.exists())
			{
				fc.create();  // create the file if it doesn't exist
			}
			//DataOutputStreamEx output = new DataOutputStreamEx(fc.openOutputStream());
			//output.write(array, 0, array.length);
			//output.close();
			OutputStream outStream = fc.openOutputStream(); 
			outStream.write(array);
			outStream.close();
			fc.close();
			#endif	//USE_JSR75
		} catch (Exception e) {
            System.out.println(e.toString());
        }
	}
}
