
public interface IRecordStore
{
	public static final int RECORD_LANGUAGE					= 0;
	public static final int RECORD_SOUND_LEVEL				= RECORD_LANGUAGE + 1;
	public static final int RECORD_VIBRATION				= RECORD_SOUND_LEVEL + 1;
	public static final int RECORD_ACCELEROMETER			= RECORD_VIBRATION + 1;
	public static final int RECORD_NUMBER_PLAYER_ENABLE		= RECORD_ACCELEROMETER + 1;
	public static final int RECORD_HIGHT_SCORE				= RECORD_NUMBER_PLAYER_ENABLE + 1;

	public static final int RECORD_HIGHT_SCORE_SIZE			= 2;	// short
	public static final int RECORD_HIGHT_SCORE_LENGTH		= 10;	// total record slots

	public static final String RECORD_NAME					= "OneTwoThree";
	public static final int RECORD_SIZE						= RECORD_HIGHT_SCORE +
		(RECORD_HIGHT_SCORE_SIZE * RECORD_HIGHT_SCORE_LENGTH);
}
