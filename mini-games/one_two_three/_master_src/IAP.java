
import javax.microedition.io.Connector;
import javax.wireless.messaging.MessageConnection;
import javax.wireless.messaging.TextMessage;

public class IAP
{
	public static final String IAP_SMS_CENTER		 			= "8022";
	public static final String IAP_SMS_CENTER_PROPERTY			= "IAP-SMSCenter";
	public static final int IAP_ERROR_CONNECTION 				= 0;
	public static final int IAP_SMS_TIMED_OUT 					= 1;
	
	private static MessageConnection connection;
	private static String smsContent;
	private static boolean isSMSSent;

	public void smsDownloadGirl()
	{
		Game.sendSMSDownloadGirlSuccessful();
	}
	public void smsBuyNumberPlay()
	{
		Game.numberPlayEnable += 3;
	}
	public void send(final int smsType)
	{
		new Thread()
		{
			public void run()
			{
				DBG("SMS: send-> start new thread");
				isSMSSent = false;
				TextMessage msg;
				
				// Send the SMS
				try {
					String smsAdress = System.getProperty(IAP_SMS_CENTER_PROPERTY);
					if (smsAdress == null) {
						smsAdress = IAP_SMS_CENTER;
					}
					
					smsAdress = "sms://" + smsAdress;
					connection = (MessageConnection) Connector.open(smsAdress);
					msg = (TextMessage) connection.newMessage(MessageConnection.TEXT_MESSAGE);
					msg.setPayloadText(smsContent);
					
					try {
						Thread.sleep(200);
					} catch (Exception e) {
						DBG("PaySMS::buy: Exception trying to sleep: " + e.toString());
					}

					DBG("SMS: address-> " + smsAdress);
					DBG("SMS: connection-> " + connection);
					DBG("SMS: msg-> " + msg);
					
					connection.send(msg);
					isSMSSent = true;
					DBG("PaySMS::buy: Message sent!");
					switch(smsType)
					{
						case 0:
							smsDownloadGirl();
							break;
						case 1:
							smsBuyNumberPlay();
							break;
					}
					try {
						Thread.sleep(100);
					} catch(Exception e) {}
				} catch(Exception e) {
					isSMSSent = false;
					e.printStackTrace();
				}

				try {
					if (connection != null)
						connection.close();
					DBG("PaySMS::buy: Connection closed!");
				} catch(Exception e) {
					DBG("PaySMS::buy: Failed to close connection! Exception: " + e.toString());
				}

				DBG("PaySMS::buy: SMS Thread ended!");
			}
		}.start();
	}
}
