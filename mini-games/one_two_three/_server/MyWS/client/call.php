<?php
// Pull in the NuSOAP code
require_once('../lib/nusoap.php');
// Create the client instance
$client = new nusoap_client('http://localhost/MyWS/server/helloworld.php');
// Call the SOAP method
$result = $client->call('hello', array('name' => 'Scott'));
// Display the result
print_r($result);
?>