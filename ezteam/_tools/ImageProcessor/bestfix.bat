
@echo off

set INPUT_PATH=Z:\game-data
set OUTPUT_PATH=Z:\game-data\output

if not exist %OUTPUT_PATH% (
	mkdir %OUTPUT_PATH%
)

ImageProcessor.exe -bestfix "%INPUT_PATH%" "%OUTPUT_PATH%"
