
@echo off

set INPUT_PATH=Z:\game-data\output
set OUTPUT_PATH=Z:\game-data\output\output
set PATTERN_SIZE=80
set MAX_COLUMN=10

if not exist %OUTPUT_PATH% (
	mkdir %OUTPUT_PATH%
)

ImageProcessor.exe -merge "%INPUT_PATH%" "%OUTPUT_PATH%" %PATTERN_SIZE% %MAX_COLUMN%
