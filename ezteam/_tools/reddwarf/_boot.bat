@echo off

if "%1" == "" (
	echo Usage: boot [BootFile]
	echo Launch default application...
)

java -jar bin/sgs-boot.jar %1
