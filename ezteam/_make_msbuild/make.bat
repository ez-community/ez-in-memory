
REM MsBuild to compile project
REM Using: make.bat project-file configuration
REM Example: make.bat Windows debug

@echo off

echo ##
echo # Setting some common variables...
echo ##
call ..\config.bat

REM Relative paths
set MASTER_DATA_PATH=%PROJECT_DIR%\_master_data
set MASTER_SRC_PATH=%PROJECT_DIR%\_master_src
set CONTENT_PATH=%MASTER_SRC_PATH%\Content
set BUILD_PATH=%PROJECT_DIR%\.version
set DATA_OUT_PATH=%BUILD_PATH%\0-data
set DATA_TO_PACK_PATH=%BUILD_PATH%\1-dataToPack
set DATA_PACKAGE_PATH=%BUILD_PATH%\2-dataPackage
set DATA_SRC_PATH=%BUILD_PATH%\3-dataSrc
set RELEASE_PATH=%PROJECT_DIR%\.release
set DOCS_PATH=%PROJECT_DIR%\.docs

REM Tools
set TOOLS_PATH=%PROJECT_DIR%\_tools
set CPP_TOOL=%TOOLS_PATH%\cpp\cpp.exe
set FONT_EXPORTER=%TOOLS_PATH%\font-exporter\font-exporter.jar
set TEXT_EXPORTER=%TOOLS_PATH%\text-exporter\text-exporter.jar
set SPRITE_EDITOR=%TOOLS_PATH%\SpriteEditor\SpriteEditor.exe
set TILED_MAP_EXPORTER=%TOOLS_PATH%\tiled-map-exporter\tiled-map-exporter.jar
set DATA_PACKAGE=%TOOLS_PATH%\data-package\data-package.jar
set CLASS_CREATER=%TOOLS_PATH%\Scripts\ClassCreater.bat

REM Xna Framework
set XNA_FRAMEWORK_WINDOWS_x86_PATH=%XNA_FRAMEWORK_PATH%\Windows\x86
REM External lib
set GAME_LIB_x86=%TOOLS_PATH%\GameLib\Windows\GameLib.dll
set GAME_LIB_WP=%TOOLS_PATH%\GameLib\WP\GameLib.dll

REM Windows studio project file extension
set PROJECT_FILE_EXT=csproj

set CONFIGURATION=%1%
set PROJECT_FILE=%2%

if "%CONFIGURATION%"=="" (
	set CONFIGURATION=debug
)
if "%PROJECT_FILE%"=="" (
	set PROJECT_FILE=Windows
)
set PROJECT_FILE=%PROJECT_FILE%.%PROJECT_FILE_EXT%

echo ##
echo # Building %PROJECT_NAME%...
echo ##
echo.

:init
cls
pushd %PROJECT_DIR%

if "%CONFIGURATION%"=="clean" (
	goto clean
) else if "%CONFIGURATION%"=="data" (
	goto data
) else if "%CONFIGURATION%"=="run" (
	goto run
) else (
	goto compile
)

:data
echo ##
echo # Make data....
echo ##

echo Create directories...
if not exist %BUILD_PATH% (
	mkdir %BUILD_PATH%
	mkdir %DATA_OUT_PATH%
	mkdir %DATA_TO_PACK_PATH%
	mkdir %DATA_PACKAGE_PATH%
	mkdir %DATA_SRC_PATH%
	mkdir %RAW_SRC_PATH%
	mkdir %PREPROCESS_PATH%
	mkdir %RELEASE_PATH%
)

echo Copy master data...
xcopy %MASTER_DATA_PATH% /s /e /y %DATA_OUT_PATH% >> nul

echo Processing data...
echo.
REM cd %DATA_OUT_PATH%
REM for /f %%i in ('dir /s /b make.bat') do (
	REM call %%i
REM )
cd %DATA_OUT_PATH%\gfx\fonts
call make.bat %DATA_TO_PACK_PATH% %DATA_SRC_PATH%

cd %DATA_OUT_PATH%\gfx\sprites
call make.bat %DATA_TO_PACK_PATH% %DATA_SRC_PATH%

cd %DATA_OUT_PATH%\text
call make.bat %DATA_TO_PACK_PATH% %DATA_SRC_PATH%

if exist %DATA_OUT_PATH%\gfx\tiles (
	cd %DATA_OUT_PATH%\gfx\tiles
	call make.bat %DATA_TO_PACK_PATH% %DATA_SRC_PATH%
)

if exist %DATA_OUT_PATH%\gfx\maps (
	cd %DATA_OUT_PATH%\gfx\maps
	call make.bat %DATA_TO_PACK_PATH%
)

REM Copy font mapping files
copy %DATA_OUT_PATH%\gfx\fonts\*.bin %DATA_TO_PACK_PATH% >> nul
REM Copy sound files
copy %DATA_OUT_PATH%\sound\*.* %DATA_TO_PACK_PATH% >> nul

REM Package data
cd %DATA_OUT_PATH%
call make.bat %DATA_TO_PACK_PATH% %DATA_PACKAGE_PATH% %DATA_SRC_PATH%

REM Create source data DATA.cs
cd %DATA_SRC_PATH%
call %CLASS_CREATER% %DATA_SRC_PATH%\DATA.cs DATA %DATA_SRC_PATH%

echo Copy data package and data source to Content directory...
if not exist %CONTENT_PATH% (
	mkdir %CONTENT_PATH%
)
copy %DATA_SRC_PATH%\*.cs %CONTENT_PATH% >> nul
copy %DATA_PACKAGE_PATH%\*.* %CONTENT_PATH% >> nul

goto finish


:compile
echo.
echo ##
echo # Compile source...
echo ##
echo Project file: %PROJECT_FILE%
echo Configuration: %CONFIGURATION%

call MsBuild.exe %MASTER_SRC_PATH%\%PROJECT_FILE% /t:Rebuild /p:Configuration=%CONFIGURATION%
if "%CONFIGURATION%"=="release" (
	echo Using Eazfuscator.NET to obfuscate...
	cd %RELEASE_PATH%
	if exist %PROJECT_NAME%.exe (
		call %EAZFUSCATOR_EXEC% %PROJECT_NAME%.exe
	)
	if exist %PROJECT_NAME%.xap (
		call %EAZFUSCATOR_EXEC% %PROJECT_NAME%.xap
	)
)
goto finish


:run
echo.
echo ##
echo # Launch %PROJECT_NAME%.exe...
echo ##
call %RELEASE_PATH%\%PROJECT_NAME%.exe
goto finish

:clean
echo.
echo ##
echo # Cleanup...
echo ##
rmdir %BUILD_PATH% /s /q
rmdir %RELEASE_PATH% /s /q

goto finish


:error


:finish
popd
echo.
echo ##
echo # Compeleted!
echo ##
