#######################################
# data-package properties file
#######################################

# The name of package
PACKAGE_NAME=CARD_SPRITE_PACKAGE
# File name of package
PACKAGE_OUT_FILE=3
# File sets: the files will pack to ${PACKAGE_NAME} file
FILE_SETS=card_sprite.png,rank_suit_sprite.png
# Source file
SOURCE_OUT_FILE=card_sprite.h
