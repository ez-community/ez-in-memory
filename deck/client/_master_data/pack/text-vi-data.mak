#######################################
# data-package properties file
#######################################

# The name of package
PACKAGE_NAME=TEXT_VI_PACKAGE
# File name of package
PACKAGE_OUT_FILE=VI
# File sets: the files will pack to ${PACKAGE_NAME} file
FILE_SETS=COMMON_VI.bin
# Source file
SOURCE_OUT_FILE=text_vi.h