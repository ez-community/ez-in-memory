﻿#######################################
# font-exporter properties file
#######################################

# Font name: define the name for your font
FONT_NAME=FONT_MONEY
# System font name: Arial, Times New Roman, Calibri...
# BASE_FONT_NAME=Arial Black
BASE_FONT_NAME=DejaVu Sans
# Font style: PLAIN, BOLD, ITALIC
STYLE=PLAIN
# Font size
SIZE=8
# Color in hexa value
COLOR=514255
# Transparent or not
TRANSPARENT=1
# Overwrite an existing image file if it exists
OVERWRITE=1
# Source file
SOURCE_OUT_FILE=font_money.h
# All characters which will be create
INPUT_STRING=0123456789$
