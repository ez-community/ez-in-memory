
	///*********************************************************************
	///* Game_TableList.h
	///*********************************************************************

	private static CSprite itemSprite;

	private static int itemSelection = 0;
	private static final byte MAX_TABLE_IN_ROW = 4;
	private static final byte MAX_ROW_IN_SCREEN = 4;
	private static final byte MAX_TABLE_IN_ROOM = 20;
	private static final byte HEADER_HEIGHT = 20;
	private static final String TABLE_LIST_HEADER = "ROOM ";
	private static final int COLOR_PLAYER = 0x34eeff;
	private static byte scollY = 0;
	private static final byte TABLE_ROW_HEIGH = 65;
	private static int positionChange = 0;
	private static byte pageScoll = 0;
	private static final byte MAX_PAGE_SCOLL = 2;
	private static final byte NUMBER_FRAME_SCOLL_TABLELIST = 9;
	private static byte arrayDown[] ={0,-12,-24,-36,-48,-58,-62,-64,-65};
	String item[][];


#ifdef ADD_WAIT_PLAYER_STATE
	public static int getNumberUserinRoom() {

		int numberUser = 0;
		for(int i = 0; i < 4; i++){
			if(!InfoPlaying.arrayNamePlayer[i].equals("None")) numberUser++;
		}
		return numberUser;
	}

	public static int getPositionInTable() {

		int position = -1;
		for(int i = 0; i < 4; i++){
			if(InfoPlaying.arrayNamePlayer[i].equals(userNameMainPlayer)) position = i;
		}
		return position;
	}

	public static int getPositionInTable(String username) {

		int position = -1;
		for(int i = 0; i < 4; i++){
			if(InfoPlaying.arrayNamePlayer[i].equals(username)) position = i;
		}
		return position;
	}
#endif

	public void updateTableListScreen(byte message, Graphics g) {

		switch (message) {
		case MESSAGE_CONSTRUCTOR:
			createInfoTable();
			subState = -1;
			// init for Menu Popup
			this.menuPopupItems = new byte[] {
					MENU_POPUP_ITEM_REFRESH,
					MENU_POPUP_ITEM_MESSAGE,
					MENU_POPUP_ITEM_GOTABLE,
				};
			
			break;

		case MESSAGE_UPDATE:
			// update for Menu Popup
			if (subState == SUBSTATE_TABLE_LIST_MENU) {
				this.menuPopup.updateMenu(keyCode);
				int selectedMenuPopupItem = this.menuPopup.getSelectedMenuItemId();
				switch (selectedMenuPopupItem) {
						
					case MENU_POPUP_ITEM_MESSAGE:
						setGameState(STATE_CHAT_SCREEN);
						break;

					case MENU_POPUP_ITEM_REFRESH:
						createInfoTable();
						subState = -1;
						break;

					case MENU_POPUP_ITEM_GOTABLE:
						break;
				}	// switch (selectedMenuPopupItem)
				if (RIGHT_SOFT_KEY)
					subState = -1;
			} else {
				#ifdef USE_REDWAF_SERVER
					if(GameClient.getTableCount() > 0){

						for(int i = 0; i < GameClient.getTableCount(); i++) {
							item[i][0] = GameClient.getTableNameAt((short)i);
							item[i][1] = "0";//GameClient.getTableStatusAt((short)i);
							item[i][2] = "0";//GameClient.getTableStatusAt((short)i);
						}
					}
				#endif
				updateTableListScreen();
			}
			break;

		case MESSAGE_PAINT:
			this.paintTableListScreen(fonts[FONT_BOLD], fonts[FONT_NORMAL], g);
			// paint for MenuPopup
			if (subState == SUBSTATE_TABLE_LIST_MENU) 
				this.menuPopup.paintMenu(g);
			break;

		case MESSAGE_DESTRUCTOR:
			itemSprite = null;
			this.menuPopup = null;
			subState = -1;
			break;
		}
	}

	public String[][] parseTableInfo(String tableInfo) {

		String row[] = GameLib.splitString(tableInfo, ";");
		String item[][] = new String[MAX_TABLE_IN_ROOM][3];

		for(int i=0; i< row.length; i++) {
			String column[] = GameLib.splitString(row[i] , ",");
			item[i][0] = column[0];
			item[i][1] = column[1];
			item[i][2] = column[2];
			DBG(""+item[i][0]+" "+column[1]+"/4");
		}
		return item;
	}

	public void refreshTableInfo() {
		
		room = "";
		room = connection.getTableInfo(InfoPlaying.idRoom);
		DBG("room = " + room);
		item = parseTableInfo(room);
	}

	public void createInfoTable() {

		positionChange = NUMBER_FRAME_SCOLL_TABLELIST - 1;
		pageScoll = 0;
		scollY = -TABLE_ROW_HEIGH;
		// Common item sprite
		Package.open(IData.COMMON_SPRITE_PACKAGE);
		itemSprite = Package.loadSprite(TABLE_IMG_ID);
		itemSprite.setFrameXOffsets(TABLE_LIST_SPRITE_FRAMES_OFFSETS_X);
		itemSprite.setFramesHeight(TABLE_LIST_SPRITE_FRAMES_HEIGHT);
		itemSprite.buildCache();
		Package.close();

		#ifndef USE_REDWAF_SERVER
		room = "";
		room = connection.getTableInfo(InfoPlaying.idRoom);
		item = parseTableInfo(room);
		#else
		item = new String[MAX_TABLE_IN_ROOM][3];
		if(commmandClient != null){
		try{
			commmandClient.joinRoom(InfoPlaying.idRoom);
		}catch(Exception e) {DBG(e);}
		}
		else {DBG("commmandClient null");}
		#endif

		setSoftKeys(SOFTKEY_MENU, SOFTKEY_OK, SOFTKEY_BACK);
	}

	public void updateTableListScreen() {

		if (LEFT_SOFT_KEY) {
			// change to substate MenuPopup
			subState = SUBSTATE_TABLE_LIST_MENU;
			if (this.menuPopup != null)
				this.menuPopup = null;
			this.menuPopup = new MenuPopup(this.menuPopupItems, fonts[FONT_NORMAL], getScreenHeight() - POPUP_MENU_OFFSET_Y,
					getScreenWidth(), getScreenHeight());
			setSoftKeys(SOFTKEY_SELECT, SOFTKEY_NONE, SOFTKEY_CLOSE);
		} else if (RIGHT_SOFT_KEY) {
				setGameState(STATE_JOIN_ROOM);
		} else if (GAME_KEY_UP) {
			if(itemSelection >= MAX_TABLE_IN_ROW) itemSelection -=MAX_TABLE_IN_ROW;
		} else if (GAME_KEY_DOWN) {
			if(itemSelection < MAX_TABLE_IN_ROOM - MAX_TABLE_IN_ROW) itemSelection +=MAX_TABLE_IN_ROW;
		} else if (GAME_KEY_RIGHT) {
			if(itemSelection < MAX_TABLE_IN_ROOM - 1) itemSelection++;
		} else if (GAME_KEY_LEFT) {
			if(itemSelection > 0) itemSelection--;
		} else if (GAME_KEY_SELECT) {
			if(itemSelection != -1) {
				InfoPlaying.idTable = InfoPlaying.idRoom*MAX_TABLE_IN_ROOM + itemSelection;
				InfoPlaying.gold = item[itemSelection][2];
				#ifdef USE_REDWAF_SERVER
				if(commmandClient != null){
				try{
					commmandClient.joinTable(InfoPlaying.idRoom,itemSelection);
				}catch(Exception e) {e.printStackTrace();}
				}
				else {DBG("commmandClient null");}
				#else
				String result = connection.joinTable(InfoPlaying.idTable);
				if(!result.equals("Fail")) {
				DBG("result = " + result);
					#ifdef ADD_WAIT_PLAYER_STATE
					InfoPlaying.arrayNamePlayer = GameLib.splitString(result, ";");
					#endif
				}
				#endif
				
				setGameState(STATE_TABLE_WATTING_PLAYERS);
			}
		}

		if (((itemSelection - pageScoll * MAX_TABLE_IN_ROW) < MAX_TABLE_IN_ROW) && (pageScoll > 0)) {
			pageScoll--;
			positionChange = 0;
			scollY = -TABLE_ROW_HEIGH;
		}

		if (((itemSelection - pageScoll * MAX_TABLE_IN_ROW) >= MAX_TABLE_IN_ROW * 3) && (pageScoll < MAX_PAGE_SCOLL - 1)) {
			pageScoll++;
			positionChange = 0;
			scollY = 0;
		}
	}

	public void paintTableListScreen(FontSprite boldFont, FontSprite normalFont, Graphics g) {

		g.drawImage(backgroundImage, 0, 0, 0);

		int width = itemSprite.getFrameWidth(0);
		int height = itemSprite.getFrameHeight(0);
		int spaceX = (getScreenWidth() - MAX_TABLE_IN_ROW * width) / (MAX_TABLE_IN_ROW+1);
		int spaceY = (getScreenHeight() - HEADER_HEIGHT - 15  - MAX_ROW_IN_SCREEN * height) / (MAX_ROW_IN_SCREEN + 1);
		int tam = 0;

		if (positionChange < NUMBER_FRAME_SCOLL_TABLELIST - 1){
			positionChange++;
		}

		if(scollY == 0) tam = scollY + arrayDown[positionChange];
		else tam = scollY - arrayDown[positionChange];

		for (int i = 0; i < MAX_TABLE_IN_ROOM; i++) {
			int x = spaceX + (i % MAX_TABLE_IN_ROW) * (width + spaceX);
			int y = tam + spaceY + HEADER_HEIGHT + (i / MAX_TABLE_IN_ROW) * (height + spaceY);
			itemSprite.paintFrame(0, x, y, Graphics.LEFT | Graphics.TOP, g) ;
			int numberUser = 0;

		#ifdef USE_TOUCH_SCREEN
			if (GameLib.wasPointerPressedInRect(x, y, width, height)) {
				if (itemSelection != i) {
					itemSelection = i;
				} else {
					InfoPlaying.idTable = InfoPlaying.idRoom*MAX_TABLE_IN_ROOM + itemSelection;
					InfoPlaying.gold = item[itemSelection][2];
					String result = connection.joinTable(InfoPlaying.idTable);

					if(!result.equals("Fail")) {
						DBG("result = " + result);
						#ifdef ADD_WAIT_PLAYER_STATE
						InfoPlaying.arrayNamePlayer = GameLib.splitString(result, ";");
						#endif
					}
					setGameState(STATE_TABLE_WATTING_PLAYERS);
				}
			}
		#endif

			try {
				if (item[i][1] != null)
					numberUser = Integer.parseInt(item[i][1]);
			} catch(Exception e) {
				e.printStackTrace();
			}
			//numberUser = 4;

			fonts[FONT_MONEY].drawString(item[i][0].substring(6), x + 23, y + 23, Graphics.HCENTER | Graphics.VCENTER, g);
			if(numberUser > 0) {
				fonts[FONT_MONEY].drawString(item[i][2] + "$", x + 35, y + 40, Graphics.HCENTER | Graphics.VCENTER, g);
			}

			//draw number Player in room
			if(numberUser > 0) {GameLib.fillRect(COLOR_PLAYER, x + 19, y + 3, 7, 7, g); numberUser--;
			DBG("draw ne x="+(x + 19)+" y="+(y + 3));}
			if(numberUser > 0) {GameLib.fillRect(COLOR_PLAYER, x + 19, y + 35, 7, 7, g); numberUser--;}
			if(numberUser > 0) {GameLib.fillRect(COLOR_PLAYER, x + 3, y + 19, 7, 7, g); numberUser--;}
			if(numberUser > 0) {GameLib.fillRect(COLOR_PLAYER, x + 35, y + 19, 7, 7, g); numberUser--;}

		}

		GameLib.fillRect(BACKGROUND_GRADIENT_COLOR1, 0, 0, getScreenWidth() , HEADER_HEIGHT, g);
		g.setColor(0x34eeff);
		fonts[FONT_BOLD].drawString(TABLE_LIST_HEADER + InfoPlaying.idRoom, getScreenWidth() >> 1, HEADER_HEIGHT >> 1, Graphics.HCENTER | Graphics.VCENTER, g);

		g.setColor(0xff0000);
		g.setStrokeStyle(Graphics.SOLID);
        g.drawRect(spaceX + (itemSelection % MAX_TABLE_IN_ROW) * (width + spaceX), tam + spaceY + HEADER_HEIGHT + (itemSelection / MAX_TABLE_IN_ROW) * (height + spaceY), width, height);
	}
