
import javax.microedition.lcdui.Graphics;

public class Menu
{
	public static final int MENU_STYLE_VERTICAL	= 0;	// Normal top-down list
	public static final int MENU_STYLE_HORIZON		= 1;	// Left-right list

	private final int MENU_OFFSET_Y 		= 70;
	private final int MENU_ITEM_SPACE 		= 40;
	private final int UNSELECTED_ID 		= -1;

	private int style;
	private FontSprite font;
	private CSprite menuSprite;
	private int[] menuItems;
	private int left, top, width, height, numItemInRow, effectFrames;
	private int backgroundColor 			= 0x3366ff;
	private int selectedItemColor 			= 0xff470a;

	private int currentItemId, selectedItemId;
	private int ITEM_HEIGHT = IData.MAIN_MENU_ICONS_FRAMES_WIDTH + 25;
	private int ITEM_WIDTH = ITEM_HEIGHT;
	private int ITEM_SPACE_X;
	private int ITEM_SPACE_Y = 10;

	public Menu(int[] menuItems, FontSprite font, int left, int top, int width, int height,
		int itemInRow, int menuStyle)
	{

		this.menuItems = menuItems;
		this.font = font;
		this.left = left;
		this.top = top;
		this.width = width;
		this.height = height;
		this.numItemInRow = itemInRow;
		this.style = menuStyle;
		this.currentItemId = 0;
		this.selectedItemId = UNSELECTED_ID;
		this.effectFrames = 0;
	}

	public void setMenuStyle(int style)
	{
		this.style = style;
	}

	public void setMenuItems(int[] menuItems)
	{
		this.menuItems = menuItems;
	}

	public void setColors(int backgroundColor, int selectedItemColor)
	{
		this.backgroundColor = backgroundColor;
		this.selectedItemColor = selectedItemColor;
	}

	public void setSelectedItemColor(int selectedItemColor)
	{
		this.setColors( -1, selectedItemColor);
	}

	/**
	 * MENU_STYLE_HORIZON menu: frame 0 is normal item, frame 1 is selected item.
	 * MENU_STYLE_VERTICAL menu: normal items list, and selected items list.
	 */
	public void setMenuSprite(CSprite menuSprite)
	{
		this.menuSprite = menuSprite;
	}

	public void paintMenu(Graphics g)
	{
		switch(this.style) {
			case MENU_STYLE_VERTICAL:
				g.setClip(this.left, this.top, this.width - this.left, this.height - this.top);
				// Paint background
				if (this.backgroundColor > - 1) {
					g.setColor(this.backgroundColor);
					g.fillRect(this.left, this.top, this.width, this.height);
				}
				// Paint selected option, if don't use menu sprite
				if (this.menuSprite == null) {
					g.setColor(this.selectedItemColor);
					g.fillRect(this.left, MENU_OFFSET_Y + this.currentItemId * MENU_ITEM_SPACE -
						(MENU_ITEM_SPACE >> 1),	this.width, MENU_ITEM_SPACE);
				}
				// Paint menu items
				for (int i = 0; i < this.menuItems.length; i++) {
					// Paint menu button if use menu sprite
					if (this.menuSprite != null) {
						int frameId = (i == this.currentItemId) ? 1 : 0;
						this.menuSprite.paintFrame(frameId, this.width >> 1, MENU_OFFSET_Y + MENU_ITEM_SPACE * i,
								Graphics.HCENTER | Graphics.VCENTER, g);
					}
					this.font.drawString(Package.getText(this.menuItems[i]), this.width >> 1,
							MENU_OFFSET_Y + MENU_ITEM_SPACE * i, Graphics.HCENTER | Graphics.VCENTER, g);
				}
				break;	// MENU_STYLE_VERTICAL

			case MENU_STYLE_HORIZON:
				g.setClip(this.left, this.top, this.width - this.left, this.height - this.top);
				int posX, posY;
				posX = ITEM_SPACE_X;
				int x, y, w_h;

				// Paint menu items
				for (int i = 0; i < this.menuItems.length; i++) {
					if (i != this.currentItemId)
						w_h = IData.MAIN_MENU_ICONS_FRAMES_WIDTH + 4;
					else
						w_h = effectFrames + IData.MAIN_MENU_ICONS_FRAMES_WIDTH + 18;
					x = ITEM_SPACE_X;
					y = this.top;
					x += (i % this.numItemInRow) * (ITEM_WIDTH+ITEM_SPACE_X) + (ITEM_WIDTH >> 1) ;
					y += (i / this.numItemInRow) * (ITEM_HEIGHT+ITEM_SPACE_Y) + (ITEM_HEIGHT >> 1);

					// Use fillRoundRect() instead fillRect, vinh
					GameLib.fillRoundRect(this.selectedItemColor, this.backgroundColor, x - (w_h >> 1), y - (w_h >> 1), w_h, w_h,
							IGameConfig.MENU_ROUND_RECT_ARC_WIDTH, g);

					if (i != this.currentItemId)
						this.menuSprite.paintFrame(i + (IData.MAIN_MENU_ICONS_FRAMES_COUNT >> 1), x, y,
								Graphics.HCENTER | Graphics.VCENTER, g);
					else
						this.menuSprite.paintFrame(i, x, y, Graphics.HCENTER | Graphics.VCENTER, g);

					this.font.drawString(Package.getText(this.menuItems[i]), x, y + (w_h >> 1) + 10, Graphics.HCENTER | Graphics.VCENTER, g);
				#ifdef USE_TOUCH_SCREEN
					// Move update from Menu.updateMenu() to Menu.paint() to get button positions easily
					if (GameLib.wasPointerPressedInRect(x - (w_h >> 1), y - (w_h >> 1), w_h, w_h)) {
						if (this.currentItemId != i) {
							this.currentItemId = i;
						} else {
							this.selectedItemId = this.currentItemId;
						}
					}
				#endif	// USE_TOUCH_SCREEN
				}
				break;	// MENU_STYLE_HORIZON
		}	// switch(this.style)

		if (effectFrames > 0)
			effectFrames--;
	}

	public void updateMenu(int keyCode)
	{
		int length = this.menuItems.length;
		switch(this.style) {
			case MENU_STYLE_VERTICAL:
				if (GAME_KEY_UP)
					this.currentItemId--;
				else if (GAME_KEY_DOWN)
					this.currentItemId++;

				if (this.currentItemId < 0)
					this.currentItemId = length - 1;
				if (this.currentItemId >= length)
					this.currentItemId = 0;
				break;

			case MENU_STYLE_HORIZON:
				if (GAME_KEY_LEFT) {
					this.currentItemId--;
					this.effectFrames = 5;
				}
				else if (GAME_KEY_RIGHT) {
					this.currentItemId++;
					this.effectFrames = 5;
				}

				if (this.currentItemId < 0)
					this.currentItemId = length - 1;
				if (this.currentItemId >= length)
					this.currentItemId = 0;

				if (GAME_KEY_DOWN) {
					this.currentItemId += numItemInRow;
					this.effectFrames = 5;
				}
				else if (GAME_KEY_UP) {
					this.currentItemId -= numItemInRow;
					this.effectFrames = 5;
				}

				if (this.currentItemId < 0)
					this.currentItemId = (length - length%4) + this.currentItemId;
				if (this.currentItemId >= length)
					this.currentItemId = this.currentItemId - (length - length%4);
				break;
		}

		if (GAME_KEY_SELECT)
			this.selectedItemId = this.currentItemId;
	}

	/**
	 * Get the selected menu item index.
	 * The selected index will reset to 0 after call this method.
	 */
	public int getSelectedMenuItem()
	{
		int index = UNSELECTED_ID;
		if (this.selectedItemId != UNSELECTED_ID) {
			index = this.menuItems[this.selectedItemId];
			this.selectedItemId = UNSELECTED_ID;		// Reset select item index after get
		}
		return index;
	}
}
