
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.microedition.lcdui.Image;
import javax.microedition.midlet.MIDlet;

public class Package
{
	public static final String TEXT_ENCODING 	= "UTF-8";

	public static MIDlet midlet;
	// For text package
	public static String[] texts;
	// For data package
	public static String packageFile;
	public static int size;				// Package size
	public static int filesCount;		// Total files in this package
	public static int[] offsets;		// File offsets
	public static int[] lengths;		// File length

	public static void setMIDlet(MIDlet _midlet)
	{
		midlet = _midlet;
	}

	public static void open(String fileName)
	{
		packageFile = fileName;

		try {
			DataInputStream input = new DataInputStream(midlet.getClass().getResourceAsStream(packageFile));
			size = input.readInt();			// 4 bytes at begin of package
			filesCount = input.readInt();	// next 4 bytes

			// Load file offsets
			offsets = new int[filesCount];
			for (int i = 0; i < filesCount; i++) {
				offsets[i] = input.readInt();
			}

			// Calculate file lengths
			lengths = new int[filesCount];
			for (int i = 1; i < filesCount; i++) {						// From begin to end, except the last file
				lengths[i - 1] = offsets[i] - offsets[i - 1];
			}
			lengths[filesCount - 1] = size - offsets[filesCount - 1];	// The last file

			input.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void close()
	{
		// Reset package properties
		packageFile = null;
		size = 0;
		offsets = null;
		lengths = null;
	}

	/**
	 * Get data from a package which packed by data-package tool.
	 */
	public static byte[] getBytes(int fileIndex)
	{
		int offset = offsets[fileIndex];
		int length = lengths[fileIndex];
		byte[] data = new byte[length];

		try {

			DataInputStream input = new DataInputStream(midlet.getClass().getResourceAsStream(packageFile));
			input.skipBytes(offset);
			input.read(data, 0, length);

			input.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

		return data;
	}

	/**
	 * Load a text package into a String array.
	 * @param fileName	Package file name which is defined properties file.
	 */
	public static void loadTextPackage(int index)
	{
		try {
			byte[] buf = getBytes(index);
			ByteArrayInputStream stream = new ByteArrayInputStream(buf);
			DataInputStream input = new DataInputStream(stream);

			int size = input.readInt();
			texts = new String[size];

			for (int i = 0; i < size; i++) {
				int length = input.readInt();
				byte[] data = new byte[length];
				input.read(data, 0, length);
				String value = new String(data, TEXT_ENCODING);
				texts[i] = value;
			}

			input.close();
			close();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Get a row from text package.
	 * @param textId	The index of this row which defined in Text class.
	 * @return			Value of this row in selected package.
	 */
	public static String getText(int index)
	{
		if (index > -1)
			return texts[index];
		else
			return null;
	}

	public static Image getImage(int fileIndex)
	{
		Image image = null;
		byte[] imageData = getBytes(fileIndex);

		try {
			image = Image.createImage(imageData, 0, imageData.length);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return image;
	}

	public static FontSprite loadFontSprite(int imageFileIndex, String charactersList,
		int[] charXOffsets, int spacing, boolean cache)
	{
		Image image = getImage(imageFileIndex);
		return new FontSprite(image, charactersList, charXOffsets, spacing, cache);
	}

	public static CSprite loadSprite(int imageFileIndex)
	{
		Image image = getImage(imageFileIndex);
		return new CSprite(image);
	}

	public static CSprite loadSprite(int imageFileIndex, int[] frameXOffsets)
	{
		Image image = getImage(imageFileIndex);
		return new CSprite(image, frameXOffsets);
	}
}
