
import java.util.Hashtable;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;

/**
 * Draw string use custom image font.
 * @author Thanh Vinh
 * Last updated: May 07, 2011
 */
public class FontSprite extends CSprite {

	private static final int INVALID_INDEX 		= -1; 		// A character is not exist in this font
	private static final char SPACE_CHAR 		= 0x20; 	// Space character
	// private static final char NEW_LINE_CHAR 	= '\n'; 	// New line character

	private Hashtable characterTable;				// Hashtable to look up a character in the font image

	private int charIndex;
	private int charWidth;
	private int spacing;
	private int expandedSpacing;
	private int lineSpacing;

	/**
	 * Initialize FontSprite object
	 * @param image
	 * 		Image path which have the array of character in image.
	 * @param charactersList
	 *		A string containt all characters which are mapped in FontSprite.
	 * @param charXOffsets
	 * 		The offset of each character in the image.
	 */
	public FontSprite(Image image, String charactersList, int[] charXOffsets, int spacing, boolean cache) {

		super(image, charXOffsets);
		if (cache) {
			super.buildCache();
		}

		// Initialize the Hashtable characterTable
		this.characterTable = new Hashtable(charactersList.length());
		for (int i = 0; i < charactersList.length(); i++) {
			this.characterTable.put(String.valueOf(charactersList.charAt(i)), new Integer(i));
		}

		this.spacing = spacing;
	}

	public void setExpandedSpacing(int value) {

		this.expandedSpacing = value;
	}

	public void setLineSpacing(int value) {

		this.lineSpacing = value;
	}

	public int getLineSpacing() {

		return this.lineSpacing;
	}

	public int getCharIndex(char c) {

		Integer value = (Integer) this.characterTable.get(String.valueOf(c));	// Find value by key - character c

		if (value != null) {
			return value.intValue();
		} else {
			DBG("[FontSprite] The \"" + c + "\" character is not exist in this sprite!");
			return INVALID_INDEX;
		}
	}

	public int getWidth(String text) {

		int width = 0;

		if (text != null) {
			for (int i = 0; i < text.length(); i++) {
				char currentChar = text.charAt(i);

				if (currentChar != SPACE_CHAR) {
					this.charIndex = this.getCharIndex(currentChar);

					if (this.charIndex != INVALID_INDEX) {
						this.charWidth = super.getFrameWidth(charIndex);
						width += this.charWidth + this.expandedSpacing;
					} else {
						width += this.spacing;
					}
				} else {
					width += this.spacing;
				}
			}
		}

		return width;
	}


	public int getTextHeight(String[] lines) {

		int height = 0;
		for (int i = 0; i < lines.length; i++) {
			height += super.getHeight() + this.lineSpacing;		// Image height is equal line height
		}

		return height;
	}

	public void drawCharAt(char c, int x, int y, int anchor, Graphics g) {

		if (c != SPACE_CHAR) {
			this.charIndex = this.getCharIndex(c);

			if (this.charIndex != INVALID_INDEX) {
				this.charWidth = super.getFrameWidth(this.charIndex);
				super.paintFrame(this.charIndex, x, y, anchor, g);
			}
			else {
				this.charWidth = this.spacing;
			}
		}
		else {
			this.charWidth = this.spacing;
		}
	}

	public void drawString(String text, int x, int y, int anchor, Graphics g) {

		//
		// TODO Implement for all anchors
		//

		if (text == null) return;

		// Don't need set anchor for super.paintFrame() method
		// Calculate at here to increase speed

		/*if ((anchor & Graphics.LEFT) != 0) {				// LEFT
			// Ignore, default is TOP | LEFT
		} else */
		if ((anchor & Graphics.HCENTER) != 0) {				// HCENTER
			x -= this.getWidth(text) >> 1;
		} else if ((anchor & Graphics.RIGHT) != 0) {		// RIGHT
			x -= this.getWidth(text);
		}

		/*if ((anchor & Graphics.TOP) != 0) {				// TOP
			// Ignore, default is TOP | LEFT
		} else */
		if ((anchor & Graphics.VCENTER) != 0) {				// VCENTER
			y -= super.getHeight() >> 1;
		} else if ((anchor & Graphics.BOTTOM) != 0) {
			y -= super.getHeight();
		}

		for (int i = 0; i < text.length(); i++) {
			this.drawCharAt(text.charAt(i), x, y, 0, g);
			x += this.charWidth + this.expandedSpacing;
		}
	}

	public void drawText(String[] lines, int x, int y, int anchor, Graphics g) {

		if (lines == null) return;
		for (int i = 0; i < lines.length; i++) {
			this.drawString(lines[i], x, y, anchor, g);
			y += super.getHeight() + this.lineSpacing;		// Image height is equal line height
		}
	}

	public void drawPage(String text, int x, int y, int anchor, int pageWidth, int pageHeight, Graphics g) {

		int width = 0, x_temp = x, wordWidth;
		String word;
		int i = 0, j = 0, w;
		while(text.charAt(i) == ' ') i++;
		for (i=i; i < text.length(); i++) {
			if((text.charAt(i) == ' ') || (i == text.length()-1)) {
				if (j>0) {
					word = text.substring(i-j, i + 1);
					w = getWidth(word);
					if (width + w < pageWidth) {
						drawString(word, x, y, anchor, g);
					} else {
						y += super.getHeight() + this.lineSpacing;
						x = x_temp;
						width = 0;
						drawString(word, x, y, anchor, g);
					}
					x += w + this.expandedSpacing;
					width += w + this.expandedSpacing;
					word = null;
					j = 0;
				}
				// else {
					// this.drawCharAt(text.charAt(i), x, y, anchor, g);
					// x += this.charWidth + this.expandedSpacing;
					// width += this.charWidth + this.expandedSpacing;
				// }
			} else {
				j++;
			}
		}
	}
}
