

public class GameListener implements RedDwarfListener
{
	//private static final Logger logger = Logger.getLogger(GameListener.class.getName());

	private Protocol protocol;

	public GameListener()
	{
		this.protocol = new Protocol();
	}

	//@Override
	public void loggedIn()
	{
		//logger.info("Client has logged in!");
		DBG("Client has logged in!");
	}

	//@Override
	public void loginFailed(String reason)
	{
		//logger.log(Level.INFO, "Login falure, reason: {0}", reason);
	}

	//@Override
	public void receivedSessionMessage(byte[] message)
	{
		//logger.log(Level.INFO, "Client received a message: {0}", message);
		DBG("Client received a message " + message);
		
		this.protocol.setMessage(message);
		this.protocol.handleSessionMessage();
	}

	//@Override
	public void reconnecting()
	{
		// TODO Auto-generated method stub

	}

	//@Override
	public void reconnected()
	{
		// TODO Auto-generated method stub

	}

	//@Override
	public void disconnected(boolean graceful, String reason)
	{
		//logger.log(Level.INFO, "Client disconnected! Param: {0}, reson: {1}",
				//new Object[] {graceful, reason});
	}

	//@Override
	public void joinedChannel(String clientChannelName)
	{
		// TODO Auto-generated method stub

	}

	//@Override
	public void leftChannel(String clientChannelName)
	{
		//logger.log(Level.INFO, "Client left channel {0}", clientChannelName);
	}

	//@Override
	public void receivedChannelMessage(String clientChannelName, byte[] message)
	{
		DBG("da nhan duoc du lieu tu channel "+message);
		this.protocol.setMessage(message);
		this.protocol.handleChannelMessage();
		//logger.log(Level.INFO, "Client received a channel [{0}] message [{1}]",
			//	new Object[] {clientChannelName, message});
	}
}
