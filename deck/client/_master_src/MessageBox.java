
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;

/**
 * MessageBox object used to draw a message box.
 * Example: information message, question message.
 * @version 1.0.0
 * @date May 15, 2011.
 */

public class MessageBox
{
	public static final byte STYLE_INFORMATION			= 0;	// No button
	public static final byte STYLE_OK					= 1;	// Once button
	public static final byte STYLE_CONFIRM				= 2;	// Two buttons

	public static final int BUTTON_NONE	 				= -1;
	public static final int BUTTON_1 					= 0;
	public static final int BUTTON_2 					= 1;

	private static final int NULL_TEXT_ID				= -1;
	private static final int SPACE_LENGTH				= 4;
	private static final int BORDER_WIDTH				= 2;

	private FontSprite titleFont, font;
	private int title, message, button1, button2;	// Text ID instead string
	private int left, top, width;
	private int backgroundColor, buttonColor, selectedColor, borderColor;
	private int alpha;
	private int selectedIndex;
	private byte style;

	// Compute size and positions
	private int height;
	private int buttonWidth, buttonHeight;
	private int messagePosY;
	private int button1PosX, button2PosX, buttonPosY;
	// Use these images to draw MessageBox with alpha effect
	private Image backgroundImage, buttonImage, selectedButtonImage;

	/**
	 * MessageBox with two button with style, base for other contructors.
	 */
	public MessageBox(int title, int message, int button1, int button2, int defaultButton,
		int left, int top, int width, byte style)
	{
		this.title = title;
		this.message = message;
		this.button1 = button1;
		this.button2 = button2;
		this.selectedIndex = defaultButton;
		this.left = left;
		this.top = top;
		this.width = width;
		this.style = style;

		this.alpha = 0xff;	// No alpha is default
	}

	/**
	 * MessageBox with two button.
	 */
	public MessageBox(int title, int message, int button1, int button2, int defaultButton,
		int left, int top, int width)
	{
		this(title, message, button1, button2, defaultButton, left, top, width, STYLE_CONFIRM);
	}

	/**
	 * MessageBox with one buttons.
	 */
	public MessageBox(int title, int message, int button, int left, int top, int width)
	{
		this(title, message, button, NULL_TEXT_ID, BUTTON_1, left, top, width, STYLE_OK);
	}

	/**
	 * MessageBox with message only.
	 */
	public MessageBox(int title, int message, int left, int top, int width) {

		this(title, message, NULL_TEXT_ID, NULL_TEXT_ID, BUTTON_NONE, left, top, width, STYLE_INFORMATION);
	}

	private void computePositonAndSize()
	{
		// Y axis
		this.messagePosY = (this.title > NULL_TEXT_ID) ?
				this.top + this.titleFont.getHeight() + (SPACE_LENGTH << 1) : this.top;

		this.buttonPosY = (this.button1 > NULL_TEXT_ID) ?
				this.messagePosY + (this.font.getHeight() << 1) : this.messagePosY;

		this.height = this.buttonPosY - this.top + this.font.getHeight() + this.font.getHeight();
		// Height
		this.buttonHeight = this.buttonWidth = 0;
		if (this.title > NULL_TEXT_ID) {
			this.buttonHeight = this.font.getHeight() + (SPACE_LENGTH << 1);
		}
		// Width
		this.buttonWidth = (this.width - (SPACE_LENGTH * 6)) >> 1;
		// X axis
		this.button1PosX = this.left + (SPACE_LENGTH << 1);
		this.button2PosX = this.button1PosX + this.buttonWidth + (SPACE_LENGTH << 1);
	}

	/**
	 * Call this method before call getHeight() method.
	 */
	public void setFonts(FontSprite titleFont, FontSprite font)
	{
		this.titleFont = titleFont;
		this.font = font;
		// Compute item positions and message box height
		this.computePositonAndSize();
	}

	public int getHeight()
	{
		return this.height;
	}

	public void setColors(int backgroundColor, int buttonColor, int selectedColor, int borderColor)
	{
		this.backgroundColor = backgroundColor;
		this.buttonColor = buttonColor;
		this.selectedColor = selectedColor;
		this.borderColor = borderColor;
	}

	/**
	 * Use drawImage() method instead fillRect() method.
	 * Call this method after setColors() method.
	 */
	public void setAlpha(int alpha)
	{
		if (alpha != 0xff) {
			this.alpha = alpha;
			//
			// Create all background images
			//
			int imageWidth = this.width >> 2;
			int imageHeight = this.height >> 2;
			// MessageBox background image
			this.backgroundImage = GameLib.createTransparentImage(imageWidth, imageHeight,
					this.backgroundColor, this.alpha);
			// Button background image
			this.buttonImage = GameLib.createTransparentImage(imageWidth, imageHeight,
					this.buttonColor, this.alpha);
			// Selected button background image
			this.selectedButtonImage = GameLib.createTransparentImage(imageWidth, imageHeight,
					this.selectedColor, this.alpha);
		}
	}

	public void nextButton()
	{
		if (this.style == STYLE_CONFIRM) {
			this.selectedIndex++;
		}
		this.selectedIndex %= 2;
	}

	public void previousButton()
	{
		if (this.style == STYLE_CONFIRM) {
			this.selectedIndex--;
		}
		this.selectedIndex %= 2;
	}

	public int getSelectedButton()
	{
		int selectedButton = BUTTON_NONE;
		if (this.selectedIndex == BUTTON_1)
			selectedButton = button1;
		else if (this.selectedIndex == BUTTON_2)
			selectedButton = button2;

		return selectedButton;
	}

	public void paint(Graphics g)
	{
		if (this.alpha == 0xff) {
			g.setClip(this.left, this.top, this.width, this.height);
			// Background
			GameLib.fillRect(this.backgroundColor, this.left, this.top, this.width, this.height, g);
			// Buttons
			if (this.style != STYLE_INFORMATION) {
				if (this.selectedIndex == BUTTON_1) {
					GameLib.fillRect(this.selectedColor, this.button1PosX, this.buttonPosY,
							this.buttonWidth, this.buttonHeight, g);
					GameLib.fillRect(this.buttonColor, this.button2PosX, this.buttonPosY,
							this.buttonWidth, this.buttonHeight, g);
				} else {
					GameLib.fillRect(this.buttonColor, this.button1PosX, this.buttonPosY,
							this.buttonWidth, this.buttonHeight, g);
					GameLib.fillRect(this.selectedColor, this.button2PosX, this.buttonPosY,
							this.buttonWidth, this.buttonHeight, g);
				}
			}
		} else {
			// Background
			GameLib.fillRect(this.backgroundImage, this.left, this.top, this.width, this.height, g);
			this.titleFont.drawString(Package.getText(this.title), this.left + SPACE_LENGTH,
					this.top + SPACE_LENGTH, Graphics.LEFT | Graphics.TOP, g);
			// Buttons
			if (this.style != STYLE_INFORMATION) {
				if (this.selectedIndex == BUTTON_1) {
					GameLib.fillRect(this.selectedButtonImage, this.button1PosX, this.buttonPosY,
							this.buttonWidth, this.buttonHeight, g);
					GameLib.fillRect(this.buttonImage, this.button2PosX, this.buttonPosY,
							this.buttonWidth, this.buttonHeight, g);
				} else {
					GameLib.fillRect(this.buttonImage, this.button1PosX, this.buttonPosY,
							this.buttonWidth, this.buttonHeight, g);
					GameLib.fillRect(this.selectedButtonImage, this.button2PosX, this.buttonPosY,
							this.buttonWidth, this.buttonHeight, g);
				}
			}
		}

		g.setClip(this.left - BORDER_WIDTH, this.top - BORDER_WIDTH,
				this.width + (BORDER_WIDTH << 1), this.height + (BORDER_WIDTH << 1));
		// Borders
		GameLib.drawRect(this.borderColor, this.left, this.top, this.width, this.height, BORDER_WIDTH, g);
		GameLib.drawLine(this.borderColor, this.left, this.messagePosY, this.left + this.width, this.messagePosY, g);
		if (this.style != STYLE_INFORMATION) {
			GameLib.drawRect(this.borderColor, this.button1PosX, this.buttonPosY,
						this.buttonWidth, this.buttonHeight, g);
			GameLib.drawRect(this.borderColor, this.button2PosX, this.buttonPosY,
					this.buttonWidth, this.buttonHeight, g);
		}
		// Text
		this.titleFont.drawString(Package.getText(this.title), this.left + SPACE_LENGTH,
				this.top + SPACE_LENGTH, Graphics.LEFT | Graphics.TOP, g);
		this.font.drawString(Package.getText(this.message), (this.width >> 1) + this.left,
				this.messagePosY + SPACE_LENGTH, Graphics.HCENTER | Graphics.TOP, g);
		this.font.drawString(Package.getText(this.button1), this.button1PosX + (this.buttonWidth >> 1),
				this.buttonPosY + SPACE_LENGTH, Graphics.HCENTER | Graphics.TOP, g);
		this.font.drawString(Package.getText(this.button2), this.button2PosX + (this.buttonWidth >> 1),
				this.buttonPosY + SPACE_LENGTH, Graphics.HCENTER | Graphics.TOP, g);
	}
}
