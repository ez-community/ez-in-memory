
public interface IStates
{
	//
	// Handle state messages
	//
	public static final byte MESSAGE_CONSTRUCTOR 	= 0;
	public static final byte MESSAGE_UPDATE 		= 1;
	public static final byte MESSAGE_PAINT 			= 2;
	public static final byte MESSAGE_DESTRUCTOR 	= 3;
	public final static byte MESSAGE_SHOWNOTIFY 	= 4;
	public final static byte MESSAGE_HIDENOTIFY 	= 5;

	//
	// States
	//

	public static final byte STATE_LOGO_SCREEN						= 0;
	public static final byte STATE_LOADING							= 1;
	public static final byte STATE_SOUND_CONFIRM 					= 2;
	public static final byte STATE_SPLASH_SCREEN					= 3;
	public static final byte STATE_MAIN_MENU 						= 4;
	public static final byte STATE_HELP 							= 5;
	public static final byte STATE_ABOUT 							= 6;
	public static final byte STATE_OPTIONS 							= 7;
	public static final byte STATE_LOGIN_SCREEN						= 8;
	public static final byte STATE_REGISTER_SCREEN					= 9;
	public static final byte STATE_GAME_PLAY 						= 10;
	public static final byte STATE_INGAME_MENU 						= 11;
	public static final byte STATE_EXIT_CONFIRM						= 12;
	public static final byte STATE_EXIT_TO_MAIN_MENU				= 13;
	public static final byte STATE_JOIN_ROOM						= 14;
	public static final byte STATE_TABLE_LIST						= 15;
	public static final byte STATE_TABLE_WATTING_PLAYERS			= 16;	// Watting for player join in a table
	public static final byte STATE_CHAT_SCREEN						= 17;
	public static final byte STATE_FRIENDLIST_SCREEN				= 18;

	// Define state for Popup Menu
	public static final byte STATE_MAIN_MENU_POPUP_MENU				= 50;
	public static final byte STATE_JOIN_ROOM_POPUP_MENU				= 51;
	public static final byte SUBSTATE_TABLE_LIST_MENU				= 52;
	public static final byte SUBSTATE_INGAME_MENU					= 53;
	public static final byte STATE_LOGIN_POPUP_MENU					= 54;
	public static final byte STATE_REGISTER_POPUP_MENU				= 55;
	public static final byte SUBSTATE_WAITING_PLAY_PP_MENU			= 56;
	public static final byte SUBSTATE_SETTING_PP_MENU				= 57;
	public static final byte STATE_SETTING_POPUP_SETTING_GOLD		= 58;

	public static final byte STATE_LOGIN_CHECK_ACCOUNT				= 100;
	public static final byte STATE_LOGIN_INFO_MESSAGE				= 101;
	public static final byte STATE_REGISTER_CHECK_ACCOUNT			= 102;
	public static final byte STATE_REGISTER_INFO_MESSAGE			= 103;
	public static final byte STATE_INGAME_POPUP_ENTER_CHAT			= 104;
}
