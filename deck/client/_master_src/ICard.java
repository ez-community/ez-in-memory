
public interface ICard {

	//
	// Some constants to compare Card
	//
	public static int LOWER		= -1;
	public static int EQUAL		= 0;
	public static int GREATER	= 1;

	//
	// Card suits
	//
	public static int SUIT_SPADE	= 0x0;
	public static int SUIT_CLUB		= 0x1;
	public static int SUIT_DIAMON	= 0x2;
	public static int SUIT_HEART	= 0x3;

	public static String[] SUITS = {
		"SPADE", "CLUB", "DIAMON", "HEART"
	};

	//
	// Card ranks
	//
	public static int RANK_3		= 0x3;
	public static int RANK_4		= 0x4;
	public static int RANK_5		= 0x5;
	public static int RANK_6		= 0x6;
	public static int RANK_7		= 0x7;
	public static int RANK_8		= 0x8;
	public static int RANK_9		= 0x9;
	public static int RANK_10		= 0xA;
	public static int RANK_JACK		= 0xB;
	public static int RANK_QUEEN	= 0xC;
	public static int RANK_KING		= 0xD;
	public static int RANK_ACE		= 0xE;
	public static int RANK_DEUCE	= 0xF;	// DEUCE

	public static String[] RANKS = {
		"N/A", "N/A", "N/A", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King", "Ace", "Deuce"
	};

	//
	// Methods
	//
	public int getSuit();
	public int getRank();
	public int getValue();
	public boolean isPairWith(ICard card);
	public boolean isSequencerWith(ICard card);
	public int compareTo(ICard card);
	public void print();
}