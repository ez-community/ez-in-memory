
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;

/**
 * ListView object for draw a table data.
 * @author Thanh Vinh <thanh.vinh@hotmail.com>
 * Last updated: February 12, 2011
 */

public class ListView {

	public static final int LEFT_ALIGN			= Graphics.LEFT;
	public static final int CENTER_ALIGN		= Graphics.HCENTER;
	public static final int RIGHT_ALIGN			= Graphics.RIGHT;

	private static final int UNSELECTED_ID 		= -1;

	private FontSprite headerFont, itemFont;
	private String[] headers;
	private String[][] items;
	private int columnsCount;
	private int[] columnsPos, aligns;		// Columns positions & aligns
	private int left, top, width, height;
	private int headerHeight, itemHeight;	// Base font height
	private int headerBackgroundColor, selectedItemBackgroundColor;
	private Image[] icons;

	private int currentItemId, selectedItemId;

	public ListView(String[] headers, String[][] items, int left, int top, int width, int height) {

		this.headers = headers;
		this.items = items;
		this.selectedItemId = UNSELECTED_ID;

		this.left = left;
		this.top = top;
		this.width = width;
		this.height = height;

		this.columnsCount = this.headers.length;
	}

	private int getColumnWidth(int column) {

		if (column == this.columnsCount - 1) {
			return (this.width - this.columnsPos[column]);
		} else {
			return (this.columnsPos[column + 1] - this.columnsPos[column]);
		}
	}

	private int getStartOffsetX(int column, String text, boolean isHeader) {

		int offsetX = this.columnsPos[column];	// Column position
		int iconWidth = (isHeader ? 0 : this.icons[column].getWidth());
		int textWidth = (isHeader ? this.headerFont.getWidth(text) : this.itemFont.getWidth(text));

		if (this.aligns[column] == RIGHT_ALIGN) {
			offsetX += (this.getColumnWidth(column) - iconWidth - textWidth);
		} else if (this.aligns[column] == CENTER_ALIGN) {
			offsetX += ((this.getColumnWidth(column) - iconWidth - textWidth) >> 1);
		}

		return offsetX;
	}

	/**
	 * Render new items without re-create ListView object.
	 */
	public void setItems(String[][] items) {

		this.items = null;
		this.items = items;
	}

	public void setColumnsPos(int[] columnsPos, int[] aligns) {

		this.columnsPos = columnsPos;
		this.aligns = aligns;
	}

	public void setFonts(FontSprite headerFont, FontSprite itemFont) {

		this.headerFont = headerFont;
		this.itemFont = itemFont;

		// Calculate header height & item height
		this.headerHeight = headerFont.getHeight() + 8;
		this.itemHeight = itemFont.getHeight() + 8;
	}

	public void setColors(int headerBackgroundColor, int selectedItemBackgroundColor) {

		this.headerBackgroundColor = headerBackgroundColor;
		this.selectedItemBackgroundColor = selectedItemBackgroundColor;
	}

	public void setIcons(Image[] icons) {

		this.icons = icons;
	}

	public void paintListView(Graphics g) {

		String text;
		int offsetX, offsetY;

		// Paint header
		g.setColor(this.headerBackgroundColor);
		g.fillRect(this.left, this.top, this.width, this.headerHeight);
		offsetY = this.top + (this.headerHeight >> 1);
		for (int column = 0; column < this.columnsCount; column++) {
			text = this.headers[column];
			offsetX = this.getStartOffsetX(column, text, true);
			this.headerFont.drawString(text, offsetX, offsetY, Graphics.LEFT | Graphics.VCENTER, g);
		}

		// Paint items
		offsetY = (this.top + this.headerHeight) + (this.itemHeight >> 1) + 30;
		for (int row = 0; row < this.items.length; row++) {
			// Selected item
			if (row == this.currentItemId) {
				g.setColor(this.selectedItemBackgroundColor);
				g.fillRect(0, offsetY - (this.itemHeight >> 1), this.width, this.itemHeight);
			}

		#ifdef USE_TOUCH_SCREEN
			if (GameLib.wasPointerPressedInRect(0, offsetY - (this.itemHeight >> 1), this.width, this.itemHeight)) {
				if (this.currentItemId != row) {
					this.currentItemId = row;
				} else {
					this.selectedItemId = this.currentItemId;
				}
			}
		#endif

			// Items
			for (int column = 0; column < this.columnsCount; column++) {
				text = this.items[row][column];
				offsetX = this.getStartOffsetX(column, text, false);
				g.drawImage(this.icons[column], offsetX, offsetY, Graphics.LEFT | Graphics.VCENTER);
				this.itemFont.drawString(this.items[row][column], offsetX + this.icons[column].getWidth(),
						offsetY, Graphics.LEFT | Graphics.VCENTER, g);
			}
			offsetY += this.itemHeight;
		}
	}

	public void updateListView(int keyCode) {

		if (GAME_KEY_UP)
			this.currentItemId--;
		else if (GAME_KEY_DOWN)
			this.currentItemId++;

		if (this.currentItemId < 0)
			this.currentItemId = this.items.length - 1;
		if (this.currentItemId >= this.items.length)
			this.currentItemId = 0;

		if (GAME_KEY_SELECT)
			this.selectedItemId = this.currentItemId;
	}

	/**
	 * Get the selected item index.
	 * The selected index will reset to 0 after call this method.
	 */
	public int getSelectedItemId() {

		// Reset select item index after get
		int index = this.selectedItemId;
		this.selectedItemId = UNSELECTED_ID;
		return index;
	}
}
