
	///*********************************************************************
	///* Game_AboutScreen.h
	///*********************************************************************

	private int scrollTextOffsetY;
	private int textHeight;

	public void updateAboutScreen(byte message, Graphics g)
	{
		switch (message) {
		case MESSAGE_CONSTRUCTOR:
			messageText = GameLib.splitString(Package.getText(TEXT.ABOUT), "\\n");
			scrollTextOffsetY = getScreenHeight() - ABOUT_TEXT_OFFSET_Y;	// Start at bottom screen
			textHeight = super.fonts[0].getTextHeight(messageText);
			setTransition(TRANSITION_NEXT);
			setSoftKeys(SOFTKEY_NONE, SOFTKEY_BACK);
			break;

		case MESSAGE_UPDATE:
			if (RIGHT_SOFT_KEY) {
				setGameState(STATE_MAIN_MENU);
			}
			break;

		case MESSAGE_PAINT:
			g.drawImage(backgroundImage, 0, 0, 0);
			fonts[FONT_BOLD].drawString(Package.getText(TEXT.ABOUT_TITLE), getScreenWidth() >> 1,
					ABOUT_TITLE_TEXT_OFFSET_Y, Graphics.HCENTER | Graphics.TOP, g);

			setClip(0, ABOUT_TEXT_OFFSET_Y, getScreenWidth(), getScreenHeight() - (ABOUT_TEXT_OFFSET_Y << 1));
			drawText(messageText, getScreenWidth() >> 1,
					scrollTextOffsetY + fonts[FONT_BOLD].getHeight(), Graphics.HCENTER | Graphics.TOP, g);
			scrollTextOffsetY -= ABOUT_SCROLL_SPEED;

			if (scrollTextOffsetY + (textHeight - ABOUT_TEXT_OFFSET_Y) < 0) {
				scrollTextOffsetY = getScreenHeight() - ABOUT_TEXT_OFFSET_Y;	// Start at bottom screen
			}
			paintSoftKeys(g);
			break;

		case MESSAGE_DESTRUCTOR:
			messageText = null;
			break;
		}
	}

	public void updateHelpScreen(byte message, Graphics g)
	{
		switch (message) {
		case MESSAGE_CONSTRUCTOR:
			messageText = GameLib.splitString(Package.getText(TEXT.HELP), "\\n");

			setSoftKeys(SOFTKEY_NONE, SOFTKEY_BACK);
			setTransition(TRANSITION_NEXT);
			break;

		case MESSAGE_UPDATE:
			if (RIGHT_SOFT_KEY) {
				setGameState(STATE_MAIN_MENU);
			}
			break;

		case MESSAGE_PAINT:
			g.drawImage(backgroundImage, 0, 0, 0);
			fonts[FONT_BOLD].drawString(Package.getText(TEXT.HELP_TITLE), getScreenWidth() >> 1, 50, Graphics.HCENTER | Graphics.TOP, g);
			fonts[FONT_NORMAL].drawText(messageText, getScreenWidth() >> 1, 70, Graphics.HCENTER | Graphics.TOP, g);
			paintSoftKeys(g);
			break;

		case MESSAGE_DESTRUCTOR:
			messageText = null;
			setGameState(nextState);
			break;
		}	//switch (message)
	}
