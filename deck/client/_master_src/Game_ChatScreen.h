
	///*********************************************************************
	///* Game_ChatScreen.h
	///*********************************************************************

#ifdef DEBUG_CHATTING
	private int chatFrameCount = 0;
	private long timeDebug = 0;
#endif

	public void updateChatScreen(byte menuId, byte message, Graphics g)
	{
		switch (message) {
			case MESSAGE_CONSTRUCTOR:
			#ifdef DEBUG_CHATTING
				timeDebug = System.currentTimeMillis();
			#endif
				// Load text
				if (Settings.getSelectedLanguage() == 0) {
					Package.open(IData.TEXT_EN_PACKAGE);
					Package.loadTextPackage(IData.COMMON_EN_ID);
				} else {
					Package.open(IData.TEXT_VI_PACKAGE);
					Package.loadTextPackage(IData.COMMON_VI_ID);
				}
				Package.close();

				if (multiChatBox == null)
					multiChatBox = new MultiChatBox(fonts[FONT_NORMAL], getScreenWidth(), getScreenHeight() - 22, 0);

				drawGradientBox(BACKGROUND_GRADIENT_COLOR1, BACKGROUND_GRADIENT_COLOR2, 0, 0,
						getScreenWidth(), getScreenHeight(), GRADIENT_VERTICAL, g);
				setSoftKeys(SOFTKEY_BACK, SOFTKEY_OK, SOFTKEY_DELETE);
				break;

			case MESSAGE_UPDATE:
			#ifdef DEBUG_CHATTING
				if (System.currentTimeMillis() - timeDebug > 2000) {
					timeDebug = System.currentTimeMillis();
					chatFrameCount ++;
					multiChatBox.addMessage("hoang", " message  " + chatFrameCount, true);
				}
			#endif
				if (LEFT_SOFT_KEY) {
					setGameState(lastState);
				}
				break;

			case MESSAGE_PAINT:
				multiChatBox.update(keyCode);
				multiChatBox.paint(g);
				break;

			case MESSAGE_DESTRUCTOR:
				break;
		}
	}
