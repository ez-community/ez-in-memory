
import javax.microedition.lcdui.Canvas;

public interface IKeyboard {

	public static final int NONE_VALUE 	= 0;
	public static final int KEY_NUM0 	= Canvas.KEY_NUM0;
	public static final int KEY_NUM1 	= Canvas.KEY_NUM1;
	public static final int KEY_NUM2 	= Canvas.KEY_NUM2;
	public static final int KEY_NUM3 	= Canvas.KEY_NUM3;
	public static final int KEY_NUM4 	= Canvas.KEY_NUM4;
	public static final int KEY_NUM5 	= Canvas.KEY_NUM5;
	public static final int KEY_NUM6 	= Canvas.KEY_NUM6;
	public static final int KEY_NUM7 	= Canvas.KEY_NUM7;
	public static final int KEY_NUM8 	= Canvas.KEY_NUM8;
	public static final int KEY_NUM9 	= Canvas.KEY_NUM9;
	public static final int KEY_POUND 	= Canvas.KEY_POUND;
	public static final int KEY_STAR 	= Canvas.KEY_STAR;
	public static final int UP 		= -1;
	public static final int DOWN 	= -2;
	public static final int LEFT 	= -3;
	public static final int RIGHT 	= -4;
	public static final int FIRE 	= -5;
	public static final int MENU	= -6;
	public static final int BACK 	= -7;
}
