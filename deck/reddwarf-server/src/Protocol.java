
import java.nio.ByteBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Vector;

import com.sun.sgs.app.AppContext;
import com.sun.sgs.app.Channel;
import com.sun.sgs.app.ChannelListener;
import com.sun.sgs.app.ChannelManager;
import com.sun.sgs.app.ClientSession;
import com.sun.sgs.app.AppContext;
import com.sun.sgs.app.Channel;
import com.sun.sgs.app.ChannelManager;
import com.sun.sgs.impl.sharedutil.MessageBuffer;

public class Protocol implements IProtocol
{
	private static final Logger logger = Logger.getLogger(Protocol.class.getName());

	private ClientSession session;
	private Channel channel;
	private MessageBuffer input;
	private MessageBuffer output;

	public Protocol(ClientSession session)
	{
		this.channel = null;
		this.session = session;
	}
	public Protocol(Channel channel, ClientSession session)
	{
		this.channel = channel;
		this.session = session;
	}
	public static ByteBuffer getCardListMessage(CardList cardList)
	{
		int length = cardList.length();
		int capacity = BYTE_SIZE + (SHORT_SIZE * length);
		MessageBuffer output = new MessageBuffer(capacity);

		output.putByte(length);
		for (byte i = 0; i < length; i++) {
			ICard card = cardList.getCardAt(i);
			output.putShort(card.getValue());
		}

		ByteBuffer message = ByteBuffer.wrap(output.getBuffer());
		return message;
	}
	
	
	public static ByteBuffer getCardListMessageWithOpcode(byte opcode, CardList cardList)
	{
		int length = cardList.length();
		int capacity = BYTE_SIZE + BYTE_SIZE + (SHORT_SIZE * length);
		MessageBuffer output = new MessageBuffer(capacity);

		output.putByte(opcode);
		output.putByte(length);
		for (byte i = 0; i < length; i++) {
			ICard card = cardList.getCardAt(i);
			output.putShort(card.getValue());
		}

		ByteBuffer message = ByteBuffer.wrap(output.getBuffer());
		return message;
	}

	/**
	 * Set the received message.
	 */
	public void setMessage(ByteBuffer message)
	{
		logger.log(Level.INFO, "Set message {0} ", message);

		try {
			byte[] buf = new byte[message.remaining()];
			message.get(buf);
			this.input = new MessageBuffer(buf);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Get the response message.
	 */
	public ByteBuffer getMessage(boolean isChannel)
	{
		this.output = null;
		ByteBuffer message = null;

		// Handle the received message
		if(!isChannel)
			this.handleSessionMessage(); // only send for user request
		else
			this.handleChannelMessage(); // send all user in table

		// Return ByteBuffer from MessageBuffer
		logger.log(Level.INFO, "Response message {0}", this.output);
		if (this.output != null) {
			message = ByteBuffer.wrap(this.output.getBuffer());
		}
		return message;
	}

	public void handleSessionMessage()
	{
		logger.log(Level.INFO, "Input message {0}", this.input);

		//
		// Declare at here to avoid duplicate var names
		//
		Room room;
		Table table;
		short roomId, tableId;

		// Response message length
		int capacity = BYTE_SIZE;	// Response message has at least a byte (is command)

		try {
			// Get the client's command
			byte command = this.input.getByte();
			System.out.println("!!!!!!!!!! COMMAND: " + command);

			switch (command) {
				case LOGIN_REQUEST:
					this.output = new MessageBuffer(capacity);

					byte version = this.input.getByte();
					if (version == VERSION) {		// Check for protocol version
						// TODO Check for login information
						String userInfo = this.input.getString();
						Player player = new Player(userInfo);
						if (player.login()) {
							GameServer.addPlayer(player);				// Add this player into Hashtable
							this.output.putByte(LOGIN_SUCCESS);
						} else {
							this.output.putByte(LOGIN_FAILURE);
						}
					} else {
						this.output.putByte(INVALID_VERSION);
					}
					break;

				case ROOM_LIST_REQUEST:
					short roomCount = GameServer.getRoomCount();

					// Get the message length first
					capacity += SHORT_SIZE;								// Room count
					for (int i = 0; i < roomCount; i++) {
						String roomName = GameServer.getRoomNameAt(i);
						capacity += BYTE_SIZE;							// Room status
						capacity += MessageBuffer.getSize(roomName);	// Room name
					}

					this.output = new MessageBuffer(capacity);

					// Write out
					output.putByte(ROOM_LIST);
					output.putShort(roomCount);							// Room count
					for (byte i = 0; i < roomCount; i++) {
						output.putByte(GameServer.getRoomStatusAt(i));	// Room status
						output.putString(GameServer.getRoomNameAt(i));	// Room name
					}
					break;

				case TABLE_LIST_REQUEST:
					roomId = this.input.getShort();						// Room index
					short tableCount = GameServer.TABLES_PER_ROOM;		// Table count
					room = GameServer.getRoomAt(roomId);

					// Get the message length first
					capacity += SHORT_SIZE * 2;							// Room index & table count
					for (short i = 0; i < tableCount; i++) {
						capacity += BYTE_SIZE;							// Table status
						String name = room.getTableNameAt(i);
						capacity += MessageBuffer.getSize(name);		// Table name
					}

					this.output = new MessageBuffer(capacity);

					// Write out
					this.output.putByte(TABLE_LIST);
					this.output.putShort(roomId);						// Room index
					this.output.putShort(tableCount);					// Table count
					for (short i = 0; i < tableCount; i++) {
						table = GameServer.getTableAt(roomId, i);
						this.output.putByte(table.getStatus());			// Table status
						this.output.putString(room.getTableNameAt(i));	// Table name
					}
					break;

				case TABLE_JOIN_REQUEST:
					roomId = this.input.getShort();
					tableId = this.input.getShort();

					table = GameServer.getTableAt(roomId, tableId);
					if (!table.isFull()) {
						table.join(this.session);
						System.out.println("number="+table.getStatus());
						for(int i=0; i< Table.MAX_PLAYERS; i++){
							Player player = table.getPlayer(i);
							if(player != null) capacity += MessageBuffer.getSize(player.getUserName());
							else capacity += MessageBuffer.getSize("None");
						}
					}

					// NOTE Do not need to send the channel name to client
					// The server automatically append the channel name to the response message
					this.output = new MessageBuffer(capacity);
					if (!table.isFull()) {
						this.output.putByte(TABLE_JOIN);
						for(int i=0; i< Table.MAX_PLAYERS; i++){
							Player player = table.getPlayer(i);
							if(player != null) this.output.putString(player.getUserName());
							else this.output.putString("None");

						}
						 ChannelManager channelManager = AppContext.getChannelManager();
						 Channel channel = channelManager.getChannel(table.getName());
						 ByteBuffer message = ByteBuffer.wrap(output.getBuffer());
						 channel.send(message);
					} else {
						this.output = new MessageBuffer(capacity);
						this.output.putByte(TABLE_FULL);
					}
					break;

				case TABLE_LEAVE_REQUEST:
					roomId = this.input.getShort();
					tableId = this.input.getShort();
					room = GameServer.getRoomAt(roomId);

					table = GameServer.getTableAt(roomId, tableId);
					table.leave(this.session);
					break;

				case START_GAME_REQUEST:
					Deck deck = new Deck();
					deck.shuffle();
					String name = this.session.getName();
					Player player = GameServer.getPlayer(name);
					Table middleTable = player.getUserTable();
					ChannelManager channelManager = AppContext.getChannelManager();
					Channel channel = channelManager.getChannel(middleTable.getName());					
					
					//for(int i = 0; i < 4; i++)
					{
						Vector cards = deck.getCards(1);
						CardList listCardDealToOnePlayer = new CardList(cards);
						ByteBuffer bufferCard = this.getCardListMessageWithOpcode(CARD_LIST, listCardDealToOnePlayer);
						channel.send(bufferCard);						
					}
					break;

				default:
					throw new Exception("Unknown session opcode: " + command);
			}	//switch (command)
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void handleChannelMessage()
	{
		logger.log(Level.INFO, "Input message {0}", this.input);

		//
		// Declare at here to avoid duplicate var names
		//
		Room room;
		Table table;
		short roomId, tableId;

		// Response message length
		int capacity = BYTE_SIZE;	// Response message has at least a byte (is command)

		try {
			// Get the client's command
			byte command = this.input.getByte();
			System.out.println("!!!!!!!!!! COMMAND: " + command);

			switch (command) {
			

				case TABLE_JOIN_REQUEST:
					roomId = this.input.getShort();
					tableId = this.input.getShort();

					table = GameServer.getTableAt(roomId, tableId);
					if (!table.isFull()) {
						table.join(this.session);
						System.out.println("number="+table.getStatus());
						for(int i=0; i< Table.MAX_PLAYERS; i++){
							Player player = table.getPlayer(i);
							if(player != null) capacity += MessageBuffer.getSize(player.getUserName());
							else capacity += MessageBuffer.getSize("None");
						}
					}

					// NOTE Do not need to send the channel name to client
					// The server automatically append the channel name to the response message
					this.output = new MessageBuffer(capacity);
					if (!table.isFull()) {
						this.output.putByte(TABLE_JOIN);
						for(int i=0; i< Table.MAX_PLAYERS; i++){
							Player player = table.getPlayer(i);
							if(player != null) this.output.putString(player.getUserName());
							else this.output.putString("None");

						}
						// ChannelManager channelManager = AppContext.getChannelManager();
						// Channel channel = channelManager.getChannel(table.getName());
						// channel.send(output);
					} else {
						this.output = new MessageBuffer(capacity);
						this.output.putByte(TABLE_FULL);
					}
					break;

				case TABLE_LEAVE_REQUEST:
					roomId = this.input.getShort();
					tableId = this.input.getShort();
					room = GameServer.getRoomAt(roomId);

					table = GameServer.getTableAt(roomId, tableId);
					short positionLeave = table.leave(this.session);
					capacity+= SHORT_SIZE;
					this.output = new MessageBuffer(capacity);
					this.output.putByte(TABLE_LEAVE);
					this.output.putShort(positionLeave);
					break;

				case START_GAME_REQUEST:
					this.output = new MessageBuffer(capacity);
					this.output.putByte(CARD_LIST);
					break;

				default:
					throw new Exception("Unknown session opcode: " + command);
			}	//switch (command)
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
