
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.sun.sgs.app.AppContext;
import com.sun.sgs.app.ClientSession;
import com.sun.sgs.app.ClientSessionListener;
import com.sun.sgs.app.DataManager;
import com.sun.sgs.app.ManagedReference;

class PlayerSessionListener implements Serializable, ClientSessionListener
{
	/** The version of the serialized form of this class. */
	private static final long serialVersionUID = 1L;

	/** The {@link Logger} for this class. */
	private static final Logger logger = Logger.getLogger(PlayerSessionListener.class.getName());

	/** The session this {@code ClientSessionListener} is listening to. */
	private final ManagedReference<ClientSession> sessionRef;

	/** The name of the {@code ClientSession} for this listener. */
	private final String sessionName;

	/**
	 * Creates a new {@code PlayerSessionListener} for the session.
	 *
	 * @param session the session this listener is associated with
	 */
	public PlayerSessionListener(ClientSession session)
	{
		if (session == null) {
			throw new NullPointerException("Null session");
		}

		DataManager dataMgr = AppContext.getDataManager();
		sessionRef = dataMgr.createReference(session);
		this.sessionName = session.getName();
		// this.enterRoom(0);
	}

	/**
	 * Returns the session for this listener.
	 */
	protected ClientSession getSession()
	{
		// We created the ref with a non-null session, so no need to check it.
		return sessionRef.get();
	}

	/**
	 * Enter a room by the room index.
	 */
//	public void enterTable(int index)
//	{
//		logger.log(Level.INFO, "User \"{0}\" entered ROOM {1}", new Object[]{ this.sessionName, index });
//
//		ChannelManager channelManager = AppContext.getChannelManager();
//		String name = GameServer.getTableName(index);
//		Channel channel = channelManager.getChannel(name);
//		channel.join(this.getSession());
//	}

	/**
	 * {@inheritDoc}
	 * <p>
	 * Logs when data arrives from the client, and echoes the message back.
	 */
	public void receivedMessage(ByteBuffer message)
	{
		logger.log(Level.INFO, "Message from {0}", this.sessionName);
		Protocol protocol = new Protocol(this.getSession());
		protocol.setMessage(message);

		ByteBuffer responseMessage = protocol.getMessage(false);
		logger.log(Level.INFO, "Response message: {0}", responseMessage);

		ClientSession session = getSession();
		if (responseMessage != null) {
			session.send(responseMessage);
		}
	}

	/**
	 * {@inheritDoc}
	 * <p>
	 * Logs when the client disconnects.
	 */
	public void disconnected(boolean graceful)
	{
		logger.log(Level.INFO, "User {0} has logged out, param: {1}", new Object[] { sessionName, graceful });
		GameServer.removePlayer(sessionName);
	}
}
