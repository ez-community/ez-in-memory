
import java.io.Serializable;
import java.nio.ByteBuffer;

import com.sun.sgs.app.AppContext;
import com.sun.sgs.app.Channel;
import com.sun.sgs.app.ChannelManager;
import com.sun.sgs.app.ClientSession;
import com.sun.sgs.impl.sharedutil.MessageBuffer;

public class Table implements Serializable
{
	public static final byte MAX_PLAYERS		= 4;

	/** The version of the serialized form of this class. */
	private static final long serialVersionUID 		= 1L;

	private String name;
	private Player[] players; // A list of players who join in this table, not use ArrayList or Vector
	private byte playerCounter;

	public Table(String name)
	{
		this.name = name;
		this.players = new Player[MAX_PLAYERS];
	}

	public String getName()
	{
		return name;
	}

	public byte getStatus()
	{
		return this.playerCounter;
	}
	
	public Player getPlayer(int i)
	{
		return this.players[i];
	}

	public boolean isFull()
	{
		if (this.playerCounter != MAX_PLAYERS) {
			return false;
		} else {
			return true;
		}
	}

	public void join(ClientSession session)
	{
		ChannelManager channelManager = AppContext.getChannelManager();
        Channel channel = channelManager.getChannel(this.name);
		channel.join(session);		// Join channel

		// Add player into player list
		String name = session.getName();
		Player player = GameServer.getPlayer(name);
		for (int i = 0; i < this.players.length; i++) {
			if (this.players[i] == null) {
				this.players[i] = player;
				player.setTable(this);
				this.playerCounter++;
				break;
			}
		}

		// Notify to all client in this channel
		// int userId = player.getUserId();
		// int capacity = IProtocol.BYTE_SIZE + IProtocol.INT_SIZE + MessageBuffer.getSize(name);

		// MessageBuffer buf = new MessageBuffer(capacity);
		// buf.putByte(IProtocol.TABLE_JOIN);
		// buf.putInt(userId);
		// buf.putString(name);

		// ByteBuffer message = ByteBuffer.wrap(buf.getBuffer());
		// channel.send(message);
	}

	short leave(ClientSession session)
	{
		ChannelManager channelManager = AppContext.getChannelManager();
        Channel channel = channelManager.getChannel(this.name);
		channel.leave(session);
		
		short positionLeave = -1;
		String name = session.getName();
		int userId = 0;
		// Remove player from player list
		for (int i = 0; i < this.players.length; i++) {
			if (this.players[i] != null) {
				if (this.players[i].getUserName().equals(name)) {
					this.players[i].setTable(null);
					userId = this.players[i].getUserId();
					GameServer.removePlayer(name);
					this.players[i] = null;
					positionLeave = (short)i;
					this.playerCounter--;
					break;
				}
			}
		}
		
		return positionLeave;

		// Notify to all client in this channel
		// int capacity = IProtocol.BYTE_SIZE + IProtocol.INT_SIZE;

		// MessageBuffer buf = new MessageBuffer(capacity);
		// buf.putByte(IProtocol.TABLE_LEAVE);
		// buf.putInt(userId);

		// ByteBuffer message = ByteBuffer.wrap(buf.getBuffer());
		// channel.send(message);
	}

	public void send(ByteBuffer message)
	{
		ChannelManager channelManager = AppContext.getChannelManager();
        Channel channel = channelManager.getChannel(this.name);
		channel.send(message);
	}
}
