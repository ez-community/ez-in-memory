
import java.util.Random;
import java.util.Vector;
import org.apache.log4j.Logger;


public class Deck {

	public static final int PLAYER_CARDS_COUNT = 13;
	private static Logger log = Logger.getLogger(Deck.class);

	private Random random;
	private Vector cards;

	public Deck() {

		long seed = System.currentTimeMillis();
		this.random = new Random(seed);

		//
		// Initial cards
		//
		this.cards = new Vector();
		for (int rank = ICard.RANK_3; rank <= ICard.RANK_DEUCE; rank++) {
			for (int suit = ICard.SUIT_SPADE; suit <= ICard.SUIT_HEART; suit++) {
				ICard card = new Card(rank, suit);
				this.cards.addElement(card);
			}
		}
	}

	public void shuffle() {

		int size = this.cards.size();
		for (int i = 0; i < size; i++) {
			int j = Math.abs(this.random.nextInt() % size);
			Card.swap(this.cards, i, j);
		}
	}

	public Vector getCards(int playerId) {

		Vector playerCards = new Vector();

		for (int i = 0; i < PLAYER_CARDS_COUNT; i++) {
			ICard card = (ICard) this.cards.elementAt(i + (playerId * PLAYER_CARDS_COUNT));
			playerCards.addElement(card);
		}

		return playerCards;
	}
	
	public static Vector getCards(byte[] listCardFromServer) {
	
		Vector playerCards = new Vector();
		int rankCardFromList; 
		int suitCardFromList;
		if(listCardFromServer != null)
			for (int i = 0; i < listCardFromServer.length; i++) {
				rankCardFromList = (int)(listCardFromServer[i] / 4);
				suitCardFromList = (int)(listCardFromServer[i] % 4);	
				if(suitCardFromList > 3)
				{
					DBG("suitCardFromList = " + suitCardFromList );
					suitCardFromList = 3;
				}
				ICard card = new Card(rankCardFromList, suitCardFromList);
				playerCards.addElement(card);
			}
		else
			DBG(">>>>>>>>>>>>>>>>>>>> getCards = null");
		return playerCards;
	}

	public void print() {

		for (int i = 0; i < this.cards.size(); i++) {
			ICard card = (ICard) this.cards.elementAt(i);
			card.print();
		}
	}
	
}
