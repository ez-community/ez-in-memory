@echo off

cd ..
set PROJ_DIR=%cd%
cd make

set JAVA_HOME=Y:\jdk1.6.0
set ANT_HOME=Y:\apache-ant-1.8.2
set PATH=%PATH%;%JAVA_HOME%\bin;%ANT_HOME%\bin
set CLASSPATH=%CLASSPATH%;%PROJ_DIR%\_tools\log4j\log4j.jar;%PROJ_DIR%\_tools\log4j\log4j.properties

