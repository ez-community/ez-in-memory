
public class GameClient extends RedDwarfSocketClient
{
	public static final String HOST				= "127.0.0.1";
	public static final int PORT				= 3330;

	private static String[] roomNames;
	private static byte[] roomStatuses;

	private static String[] tableNames;
	private static byte[] tableStatuses;

	@SuppressWarnings("unused")
	private static short currentRoomIndex 		= -1;
	@SuppressWarnings("unused")
	private static short currentTableIndex 		= -1;

	public GameClient(String host, int port, RedDwarfListener responseHandler)
	{
		super(host, port, responseHandler);
	}

	public static void initializeRoomList(int length)
	{
		roomNames = new String[length];
		roomStatuses = new byte[length];
	}

	public static void initializeTableList(int length)
	{
		tableNames = new String[length];
		tableStatuses = new byte[length];
	}

	public static int getRoomCount()
	{
		if (roomNames != null) {
			return roomNames.length;
		} else {
			return 0;
		}
	}

	public static int getTableCount()
	{
		if (tableNames != null) {
			return tableNames.length;
		} else {
			return 0;
		}
	}

	public static void putRoom(short index, byte status, String name)
	{
		roomNames[index] = name;
		roomStatuses[index] = status;
	}

	public static void putTable(short index, byte status, String name)
	{
		tableNames[index] = name;
		tableStatuses[index] = status;
	}

	public static String getRoomNameAt(short index)
	{
		return roomNames[index];
	}

	public static byte getRoomStatusAt(short index)
	{
		return roomStatuses[index];
	}

	public static String getTableNameAt(short index)
	{
		return tableNames[index];
	}

	public static byte getTableStatusAt(short index)
	{
		return tableStatuses[index];
	}

	public static void setCurrentRoomIndex(short index)
	{
		currentRoomIndex = index;
	}

	public static void setCurrentTableIndex(short index)
	{
		currentTableIndex = index;
	}
}
