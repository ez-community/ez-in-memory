
/**
 * Base on SimpleSgsProtocol.
 */

public interface IProtocol extends SimpleSgsProtocol
{
	//
	// The size of some Java types
	//
	public static final int BYTE_SIZE						= 1;
	public static final int SHORT_SIZE						= 2;
	public static final int INT_SIZE						= 4;

	//
	// Note: All custom protocol start at 0x60
	//

  	/** The protocol version number is invalid. Server response to a client. */
	public static final byte INVALID_VERSION 				= 0x60;

	/** Room list request. Client requesting the room list of game.*/
	public static final byte ROOM_LIST_REQUEST 				= 0x61;

	/**
	 * Room list information. Server response to a client's
	 * {@link #ROOM_LIST_REQUEST}.
	 * <br>
	 * Payload:
	 * <ul>
	 * <li> (short) room count
     * <li> (ByteArray) The index, name and status of all rooms.
     * </ul>
     * <br>
     * Each room is formated as below:
     * <ul>
     * <li> (byte) status (good, normal, bad)
     * <li> (String) room name
     * </ul>
	 */
	public static final byte ROOM_LIST 						= 0x62;

	/**
	 * Table list request. Client requesting the table list in a room.
	 * <br>
	 * Payload:
	 * <ul>
	 * <li> (byte) room index
	 * </ul>
	 */
	public static final byte TABLE_LIST_REQUEST				= 0x63;

	/**
	 * Table list in a room. Server response to a client's
	 * {@link #TABLE_LIST_REQUEST}.
	 * <br>
	 * Payload:
	 * <ul>
	 * <li> (short) room index
	 * <li> (short) table count
     * <li> (ByteArray) The id, name and status of all tables in a room
     * </ul>
     * <br>
     * Each table is formated as below:
     * <ul>
     * <li> (byte) status (how many player entered in this room?)
     * <li> (String) table name
     * </ul>
	 */
	public static final byte TABLE_LIST						= 0x64;

	/**
	 * Joining in table request. Client request to join a table in a room.
	 * <br>
	 * Payload:
	 * <ul>
	 * <li> (short) room index
	 * <li> (short) table index
	 * </ul>
	 */
	public static final byte TABLE_JOIN_REQUEST 			= 0x65;

	/**
	 * Server notifying new client that it has joined a table.
	 * This notifying message send from a channel, command same as {@code CHANNEL_JOIN}
	 * but after CHANNEL_MESSAGE message.
	 * @see SimpleSgsProtocol#CHANNEL_JOIN
	 * Payload:
	 * <ul>
	 * <li> (int) user ID
	 * <li> (String) user name
	 * </ul>
	 */
	public static final byte TABLE_JOIN 					= CHANNEL_JOIN;

	/** Table leave request. Client request to leave a table. */
	public static final byte TABLE_LEAVE_REQUEST			= 0x66;

	/**
	 * Server notifying new client that it has leave a table.
	 * This notifying message send from a channel, command same as {@code CHANNEL_LEAVE}
	 * but after CHANNEL_MESSAGE message.
	 * @see SimpleSgsProtocol#CHANNEL_LEAVE
	 * Payload:
	 * <ul>
	 * <li> (int) user ID
	 * </ul>
	 */
	public static final byte TABLE_LEAVE					= CHANNEL_LEAVE;

	/**
	 * Table full. Server notifying a client that the table which client request join is full.
	 */
	public static final byte TABLE_FULL						= 0x67;

	/**
	 * Client request to start game. Game start if that table has enough users, dependency game type.
	 */
	public static final byte START_GAME_REQUEST				= 0x68;

	/**
	 * Card list value. Server send card list value to client. <br>
	 * Payload:
	 * <ul>
	 * <li> (byte) length (card count)
	 * <li> (ByteArray) All cards value in short type.
	 * </ul>
	 */
	public static final byte CARD_LIST						= 0x69;
}

